<?php

class mailer_emails extends project_mailer_emails {

	function mailer_emails() {
		parent::project_mailer_emails();

		$this->public_acts = array("contact");

		$this->contacts_form = array(
			"layout" => "default",
			"method" => "post",
			"fields" => array(
				"email" => array(
					"type" => "email",
					"title" => "Email address",
					"placeholder" => "Enter email address",
					"prepend" => '<span class="glyphicon glyphicon-envelope"></span>',
					"required" => true,
				),
				"subject" => array(
					"type" => "select",
					"options" => "subjects",
					"title" => "Subject",
				),
				"message" => array(
					"type" => "textarea",
					"title" => "Message",
					"placeholder" => "Enter message",
					"required" => true,
				),
				"captcha" => array(
					"type" => "coolcaptcha",
					"title" => "What are the symbols?",
					"width" => 200,
					"height" => 50,
				),
				"submit_button" => array(
					"type" => "submit_button",
					"title" => "Send Message!",
					"align" => "right",
					"ladda" => 1,
				),
			),
			"redirect" => "contact_us?sent=1",
		);

	}
	
	function add($form_data, $clean = true) {
		global $coreSQL, $smarty;
		
		$user_id = (int)$smarty->get_template_vars('user_id');
		
		if ($user_id) {
			$coreSQL->query("UPDATE `users` SET `last_email_send`='".date('Y-m-d H:i:s')."' WHERE `id`=".(int)$user_id);
		}
		
		parent::add($form_data, $clean);
	}
	
	function setClick($hash) {
		global $coreSQL, $users;
		
		if (getIp() == '78.60.137.210' || getIp() == '88.119.206.62' || getIp() == '78.61.246.2' || getIp() == '90.140.246.185') {
			// Do nothing
		}
		else {
		
			if ($users->id) {
				$coreSQL->query("UPDATE `users` SET `last_email_click`='".date('Y-m-d H:i:s')."' WHERE `id`=".(int)$users->id);
			}

			parent::setClick($hash);
			
		}
	}
	
	function addConfirmationEmail($form_data) {
		global $config;
		
		/*if (file_exists($config['main_dir'].'views/images/logo.png')) {
			$form_data['images'] = serialize(array(
				0 => array(
					'cid' => 'logo',
					'filename' => $config['main_dir'].'views/images/logo.png',
					'name' => 'Logo',
					'encoding' => 'base64',
					'type' => 'image/png',
				),
			));
		}*/
		
		$data = project_mailer_emails::addConfirmationEmail($form_data);
		
		// debug
		//$data['to'] = 'reports@seopoz.com';
		//$this->add($data, false);
	}
	
	function addWelcomeEmail($form_data) {
		global $config;
		
		if (file_exists($config['main_dir'].'views/images/logo.png')) {
			$form_data['images'] = serialize(array(
				0 => array(
					'cid' => 'logo',
					'filename' => $config['main_dir'].'views/images/logo.png',
					'name' => 'Logo',
					'encoding' => 'base64',
					'type' => 'image/png',
				),
			));
		}
		
		$data = project_mailer_emails::addWelcomeEmail($form_data);
		
		// debug
		//$data['to'] = 'reports@seopoz.com';
		//$this->add($data, false);
	}
	
	function addResetPasswordEmail($form_data) {
		global $config;
		
		if (file_exists($config['main_dir'].'views/images/logo.png')) {
			$form_data['images'] = serialize(array(
				0 => array(
					'cid' => 'logo',
					'filename' => $config['main_dir'].'views/images/logo.png',
					'name' => 'Logo',
					'encoding' => 'base64',
					'type' => 'image/png',
				),
			));
		}
		
		$data = project_mailer_emails::addResetPasswordEmail($form_data);
		
		// debug
		$data['to'] = 'reports@seopoz.com';
		$this->add($data, false);
	}
	
	function contact($form_data) {
		global $config, $smarty;

		if (!isValidEmail($form_data['email'])) {
			$smarty->assign('error_email_invalid', 'email');
			$error = true;
		}

		if (empty($form_data['message'])) {
			$smarty->assign('error_message_empty', 'message');
			$error = true;
		}

		if (empty($_SESSION['captcha']) || strtolower(trim($_REQUEST['captcha'])) != $_SESSION['captcha']) {
			$smarty->assign('error_captcha', 'captcha');
			$error = true;
		}

		if (!$error) {
			$config['contacts_email'] = 'tomas@seopoz.com';
			$this->addContactEmail($form_data);
			redirect(clean($form_data['act_redirect']));
		}
	}
	
	// Self reporting
	
	function addLoginEmail($form_data) {
		global $config, $smarty;
		
		$smarty->assign('config', $config);
		$smarty->assign('user_id', $form_data['id']);
		$smarty->assign('lang', $form_data['default_lang']);
		$smarty->assign('code', $form_data['confirm_code']);
		$smarty->assign('hash', $form_data['password']);
		$smarty->assign('email', $form_data['email']);
		$smarty->assign('first_name', $form_data['first_name']);
		$smarty->assign('last_online', $form_data['last_online']);
		$smarty->assign('login_after', secondsToTime(time() - strtotime($form_data['last_online'])));
		$smarty->assign('login_count', $form_data['login_count']);
		$smarty->assign('user_ip', $form_data['user_ip']);
		
		$data = array();
		$data['type'] = 'Login';
		$data['user_from'] = $config['mailer_user_from'];
		$data['user_name'] = $config['mailer_user_name'];
		$data['to'] = $form_data['email'];
		$data['subject'] = "User #".$form_data['id'].": ".ucfirst($form_data['first_name'])." just logged ".$form_data['login_count'].'th time';
		$data['body'] = $smarty->fetch('emails/self/login.tpl');
		$data['status'] = 1;
		$data['priority'] = 3;
		
		$this->add($data, false);
	}
	
	function addClickedEmail($form_data) {
		global $config, $smarty;

		$smarty->assign('config', $config);
		$smarty->assign('user_id', $form_data['id']);
		$smarty->assign('lang', $form_data['default_lang']);
		$smarty->assign('code', $form_data['confirm_code']);
		$smarty->assign('hash', $form_data['password']);
		$smarty->assign('email', $form_data['email']);
		$smarty->assign('first_name', $form_data['first_name']);
		$smarty->assign('click', $form_data['click']);

		$data = array();
		$data['type'] = 'Clicked';
		$data['user_from'] = $config['mailer_user_from'];
		$data['user_name'] = $config['mailer_user_name'];
		$data['to'] = 'tomas@seopoz.com';
		$data['subject'] = "User #".$form_data['id'].": ".$form_data['email']." clicked on ".$form_data['click'];
		$data['body'] = $smarty->fetch('emails/self/clicked.tpl');
		$data['status'] = 0;
		$data['priority'] = 3;

		$this->add($data, false);
	}
	
	// Report to admin
	
	function addReportEmail($form_data) {
		global $config, $smarty, $companies;

		$smarty->assign('config', $config);
		$smarty->assign('user_id', $form_data['id']);
		$smarty->assign('lang', $form_data['default_lang']);
		$smarty->assign('code', $form_data['confirm_code']);
		$smarty->assign('hash', $form_data['password']);
		$smarty->assign('email', $form_data['email']);
		$smarty->assign('first_name', $form_data['first_name']);
		$smarty->assign('company', $companies->getById($form_data['company_id']));
		$smarty->assign('message', $form_data['message']);
		
		$data = array();
		$data['type'] = 'Report a company';
		$data['user_from'] = $config['mailer_user_from'];
		$data['user_name'] = $config['mailer_user_name'];
		$data['to'] = 'tomas@seopoz.com';
		$data['subject'] = "Report a company";
		$data['body'] = $smarty->fetch('emails/admin/report.tpl');
		$data['status'] = 0;
		$data['priority'] = 3;

		$this->add($data, false);
	}
	
	function addRequestForEditEmail($form_data) {
		global $config, $smarty, $companies;

		$smarty->assign('config', $config);
		$smarty->assign('user_id', $form_data['id']);
		$smarty->assign('lang', $form_data['default_lang']);
		$smarty->assign('code', $form_data['confirm_code']);
		$smarty->assign('hash', $form_data['password']);
		$smarty->assign('email', $form_data['email']);
		$smarty->assign('first_name', $form_data['first_name']);
		$smarty->assign('company', $companies->getById($form_data['company_id']));
		
		$data = array();
		$data['type'] = 'Request for edit';
		$data['user_from'] = $config['mailer_user_from'];
		$data['user_name'] = $config['mailer_user_name'];
		$data['to'] = 'tomas@seopoz.com';
		$data['subject'] = "Request for edit a company";
		$data['body'] = $smarty->fetch('emails/admin/request.tpl');
		$data['status'] = 0;
		$data['priority'] = 3;

		$this->add($data, false);
	}
	
}

?>