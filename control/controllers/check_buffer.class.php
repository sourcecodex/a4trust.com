<?php

class check_buffer extends controller {

	function check_buffer() {
		parent::controller("check_buffer");

		$this->fields = array(
			"created" => "created",
			"website_id" => "int",
			"url" => "string",
			"scan_server" => "string",
			"priority" => "int",
		);

		//$this->createTableStructure();
	}

}

?>