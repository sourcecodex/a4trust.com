<?php

class companies extends controller {

	function companies() {
		global $pages;
		
		parent::controller("companies");

		$this->fields = array(
			"created" => "created",
			"user_id" => "int",
			
			"type" => "int",
			"name" => "string",
			"address" => "string",
			"logo" => "string",
			"description" => "string",
			"description_long" => "text",
			"revenue" => "int",
			"employees" => "int",
			"rating" => "float",
			"projects" => "string",
			"architects" => "string",
			"sector" => "string_array",
			"production" => "string_array",
			"website" => "string",
			"comment" => "string",
			"producers" => "string_array",
			"related_companies" => "string_array",
		);
		
		$this->types = array(
			1 => "Dealer",
			2 => "Producer",
		);
		
		$this->products = array(
			1 => "Workstations",
			2 => "Meeting zones",
			3 => "Storages & Archives",
			4 => "Executive furniture",
			5 => "Office Chairs",
			6 => "Office Lounge",
			7 => "Occasional tables",
			8 => "Acoustic Solutions",
			9 => "PODs",
			10 => "Reception Furniture",
			11 => "Auditorium & Stadium seating",
			12 => "Office partitions",
			13 => "Laboratory furniture",
			14 => "Public/Airport furniture",
			15 => "Residential furniture",
			16 => "HoReCa",
			17 => "Flooring & Carpets",
			18 => "Lightning",
			19 => "Hospitals & Medical furniture",
		);
		
		$this->sectors = array(
			1 => "Commercial Office",
			2 => "Fashion/Retail",
			3 => "Sports/Stadiums",
			4 => "Government",
			5 => "Public areas",
			6 => "Hospitality",
			7 => "Education",
			8 => "Healthcare",
			9 => "Residential",
			10 => "Lightning",
			11 => "Flooring",
			12 => "Fit-Out",
		);
		
		$this->ratings = array(
			1 => "1 star",
			2 => "2 star",
			3 => "3 star",
			4 => "4 star",
			5 => "5 star",
		);
		
		$this->logged_acts = array("add_xajax", "edit_xajax", "uploadLogo", "removeLogo", "requestForEdit_xajax");
		
		$this->add_form = array(
			"title" => "Add company",
			"layout" => "default",
			"method" => "xajax",
			"fields" => array(
				"type" => array(
					"type" => "select",
					"options" => "company_types",
					"title" => "Type",
					"hide_default_option" => 1,
				),
				"name" => array(
					"type" => "text",
					"title" => "Name",
					"required" => 1,
				),
				"address" => array(
					"type" => "text",
					"title" => "Address",
					"required" => 1,
				),
				"addresses" => array(
					"custom" => 1,
					"type" => "addresses",
					"title" => "Other Addresses",
				),
				"description" => array(
					"type" => "textarea",
					"title" => "Description short",
					"required" => 1,
				),
				"description_long" => array(
					"type" => "textarea",
					"title" => "Description long",
				),
				"revenue" => array(
					"type" => "text",
					"title" => "Revenue",
					"required" => 1,
				),
				"website" => array(
					"type" => "text",
					"title" => "Website",
				),
				"projects" => array(
					"custom" => 1,
					"type" => "projects",
					"title" => "Projects",
				),
				"sector" => array(
					"type" => "checkboxes",
					"checkboxes" => "sectors",
					"title" => "Sectors",
					"inline" => 1,
				),
				"production" => array(
					"type" => "checkboxes",
					"checkboxes" => "products",
					"title" => "Products",
					"inline" => 1,
				),
				"producers" => array(
					"custom" => 1,
					"type" => "related_companies",
					"title" => "Producers",
					"selected" => "producers_selected",
				),
				"related_companies" => array(
					"custom" => 1,
					"type" => "related_companies",
					"title" => "Related companies",
					"selected" => "related_companies_selected",
				),
				"submit" => array(
					"type" => "submit_button",
					"title" => "Submit"
				),
			),
			"redirect" => "user/company_profile",
		);
		
		$this->edit_form = array(
			"title" => "Edit company",
			"layout" => "default",
			"method" => "xajax",
			"fields" => array(
				"type" => array(
					"type" => "select",
					"options" => "company_types",
					"title" => "Type",
					"hide_default_option" => 1,
					"disabled" => 1,
				),
				/*"rating" => array(
					"custom" => 1,
					"type" => "rating",
					"title" => "Rating",
				), */
				"name" => array(
					"type" => "text",
					"title" => "Name",
					"required" => 1,
				),
				"address" => array(
					"type" => "text",
					"title" => "Address",
					"required" => 1,
				),
				"addresses" => array(
					"custom" => 1,
					"type" => "addresses",
					"title" => "Other Addresses",
				),
				"description" => array(
					"type" => "textarea",
					"title" => "Description short",
					"required" => 1,
				),
				"description_long" => array(
					"type" => "textarea",
					"title" => "Description long",
				),
				"revenue" => array(
					"type" => "text",
					"title" => "Revenue",
					"required" => 1,
				),
				"website" => array(
					"type" => "text",
					"title" => "Website",
				),
				"projects" => array(
					"custom" => 1,
					"type" => "projects",
					"title" => "Projects",
				),
				"sector" => array(
					"type" => "checkboxes",
					"checkboxes" => "sectors",
					"title" => "Sectors",
					"inline" => 1,
				),
				"production" => array(
					"type" => "checkboxes",
					"checkboxes" => "products",
					"title" => "Products",
					"inline" => 1,
				),
				"producers" => array(
					"custom" => 1,
					"type" => "related_companies",
					"title" => "Producers",
					"selected" => "producers_selected",
				),
				"related_companies" => array(
					"custom" => 1,
					"type" => "related_companies",
					"title" => "Related companies",
					"selected" => "related_companies_selected",
				),
				"submit" => array(
					"type" => "submit_button",
					"title" => "Update"
				),
			),
			"redirect" => "user/company_profile?updated=1",
		);

		$this->company_logo_form = array(
			"layout" => "default",
			"method" => "post",
			"multipart" => 1,
			"fields" => array(
				"logo" => array(
					"type" => "image",
					"title" => "Company Logo",
					"remove_act" => "removeLogo",
					"div_style" => "width: 200px;",
				),
				"submit_button" => array(
					"type" => "submit_button",
					"title" => "Upload",
					"align" => "left",
					"ladda" => 1,
				),
			),
			"redirect" => "company_profile?updated=1",
		);
		
		$this->request_for_edit_form = array(
			"layout" => "default",
			"method" => "xajax",
			"multipart" => 1,
			"fields" => array(
				"company" => array(
					"custom" => 1,
					"type" => "search_companies",
					"title" => "Search company",
				),
				"submit_button" => array(
					"type" => "submit_button",
					"title" => "Submit",
					"align" => "left",
					"ladda" => 1,
				),
			),
			"redirect" => "companies/submit?sent=1",
		);
		
		//$this->createTableStructure();
	}

	function is($user_id, $name) {
		global $coreSQL;
		return (int)$coreSQL->queryValue("SELECT `id` FROM `".$this->table."` WHERE "
				. "`user_id`=".(int)$user_id." AND `name`='".addslashes($name)."'");
	}
	
	function getByUser($user_id) {
		global $coreSQL;
		$company_id = (int)$coreSQL->queryValue("SELECT `id` FROM `".$this->table."` WHERE "
				. "`user_id`=".(int)$user_id." LIMIT 1");
		return $this->getById($company_id);
	}
	
	function getByIds($ids) {
		global $coreSQL;
		if (!empty($ids[0])) {
			return $coreSQL->queryData("SELECT * FROM `".$this->table."` WHERE `id` IN (".addslashes(implode(',', $ids)).")");
		}
		else {
			return array();
		}
	}
	
	function add($form_data) {
		global $users, $addresses, $projects;
		
		$form_data['user_id'] = (int)$users->id;
		
		$form_data['sector'] = createArrayFromField($form_data, 'sector');
		$form_data['production'] = createArrayFromField($form_data, 'production');
		$form_data['producers'] = createArrayFromField($form_data, 'producers');
		$form_data['related_companies'] = createArrayFromField($form_data, 'related_companies');
		
		unset($form_data['rating']);
		
		$form_data['id'] = parent::add($form_data);
		
		$addresses->set($form_data);
		$projects->set($form_data);
		
		redirect('company_profile');
	}
	
	function edit($form_data) {
		global $coreSQL, $users, $addresses, $projects;
		
		$form_data['id'] = (int)$coreSQL->queryValue("SELECT `id` FROM `".$this->table."` WHERE "
				. "`user_id`=".(int)$users->id." LIMIT 1");
		
		$form_data['sector'] = createArrayFromField($form_data, 'sector');
		$form_data['production'] = createArrayFromField($form_data, 'production');
		$form_data['producers'] = createArrayFromField($form_data, 'producers');
		$form_data['related_companies'] = createArrayFromField($form_data, 'related_companies');
		
		unset($form_data['user_id']);
		unset($form_data['type']);
		unset($form_data['rating']);
		
		parent::edit($form_data);
		
		$addresses->set($form_data);
		$projects->set($form_data);
	}
	
	function uploadLogo($form_data) {
		global $config, $coreSQL, $users;
		
		$company_id = (int)$coreSQL->queryValue("SELECT `id` FROM `".$this->table."` WHERE "
				. "`user_id`=".(int)$users->id." LIMIT 1");
		
		if (isset($_FILES['logo']['error']) && $_FILES['logo']['error'] == 0) {
			$tmp_name = $_FILES['logo']['tmp_name'];

			$uploaddir = $config['main_dir'].'data/logos/'.$company_id;
			if (!file_exists($uploaddir)) {
				mkdir($uploaddir, 0777);
			}
			
			$logo_file = $uploaddir.'/logo.png';

			if (!file_exists($logo_file)) {
				create_thumbnail_png($tmp_name, $logo_file, 300, 300, true);
				
				$logo_path = 'data/logos/'.$company_id.'/logo.png';
				$coreSQL->query("UPDATE `".$this->table."` SET `logo`='".$logo_path."' WHERE `id`=".(int)$company_id);
			}
		}
	}
	
	function removeLogo($form_data) {
		global $config, $coreSQL, $users;
		
		$company_id = (int)$coreSQL->queryValue("SELECT `id` FROM `".$this->table."` WHERE "
				. "`user_id`=".(int)$users->id." LIMIT 1");
		
		$logo_file = $config['main_dir'].'data/logos/'.$company_id.'/logo.png';
		if (file_exists($logo_file)) {
			unlink($logo_file);
		}
		
		$coreSQL->query("UPDATE `".$this->table."` SET `logo`='' WHERE `id`=".(int)$company_id);
	}
	
	function requestForEdit($form_data) {
		global $smarty, $users, $requests, $mailer_emails;
		
		if (empty($form_data['company_id'])) {
			$smarty->assign('error_required', 'company');
		}
		else {
			
			$requests->add(array(
				'user_id' => $users->id,
				'company_id' => (int)$form_data['company_id'],
			));
			
			$user_info = $users->getById($users->id);
			$user_info['company_id'] = (int)$form_data['company_id'];
			$mailer_emails->addRequestForEditEmail($user_info);
		}
	}
	
	// ============================= Search ============================================

	function getCompanyName($company_id) {
		global $coreSQL;
		return $coreSQL->queryValue("SELECT `name` FROM `".$this->table."` WHERE `id`=".(int)$company_id);
	}
	
	function searchQuery($filter) {
				
		if (!empty($filter['search_query'])) {
			$search_sql = " AND (`name` LIKE '%".addslashes($filter['search_query'])."%')";
		}

		$result = parent::searchQuery($filter, array("created" => "DESC"), 20, $search_sql);
		
		if ($result) {
			foreach ($result as $key => $row) {
				foreach ($this->fields as $field => $type) {
					if ($type == "string_array") {
						$result[$key][$field] = explode(",", $row[$field]);
						
						if ($field == "producers" && !empty($result[$key][$field])) {
							$producers = array();
							
							foreach ($result[$key][$field] as $company_id) {
								$company_name = $this->getCompanyName($company_id);
								if (!empty($company_name)) {
									$producers []= $company_name;
								}
							}
							
							$result[$key][$field] = $producers;
						}
					}
					elseif ($type == "array") {
						$result[$key][$field] = unserialize($row[$field]);
					}
				}
			}
		}
		
		return $result;
	}

}

?>