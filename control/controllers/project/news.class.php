<?php

class news extends controller {

	function news() {
		global $pages;
		
		parent::controller("news");

		$this->fields = array(
			"created" => "created",
			"user_id" => "int",

			"title" => "string",
			"description" => "string",
			"body" => "text",
			"feature_img" => "string",
			"header_img" => "string",
		);
		
		//$this->createTableStructure();
	}
	
	// ============================= Search ============================================

	function searchQuery($filter) {
		global $users;
		
		//$filter['user_id'] = (int)$users->id;
		
		if (!empty($filter['search_query'])) {
			$search_sql = " AND (`title` LIKE '%".addslashes($filter['search_query'])."%')";
		}

		return parent::searchQuery($filter, array("created" => "DESC"), 50, $search_sql);
	}

}

?>