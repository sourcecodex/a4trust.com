<?php

class requests extends controller {

	function requests() {
		
		parent::controller("requests");
		
		$this->fields = array(
			"created" => "created",
			"user_id" => "int",
			"company_id" => "int",
			"status" => "string",
		);
		
		//$this->createTableStructure();
	}
	
}

?>