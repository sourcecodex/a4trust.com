<?php

class projects extends controller {
	
	function projects() {
		
		parent::controller("projects");
		
		$this->fields = array(
			"company_id" => "int",
			"name" => "string",
			"url" => "string",
		);
		
		//$this->createTableStructure();
	}
	
	function getByCompany($company_id) {
		global $coreSQL;
		return $coreSQL->queryData("SELECT * FROM `".$this->table."` WHERE `company_id`=".(int)$company_id." ORDER BY `id`");
	}

	function deleteByCompany($company_id) {
		global $coreSQL;
		$coreSQL->query("DELETE FROM `".$this->table."` WHERE `company_id`='".(int)$company_id."'");
	}
	
	function set($form_data) {
		
		$this->deleteByCompany($form_data['id']);
		
		$projects_name = createArrayFromField($form_data, 'projects_name');
		$projects_url = createArrayFromField($form_data, 'projects_url');
		
		foreach ($projects_name as $key => $name) {
			$this->add(array(
				'company_id' => (int)$form_data['id'],
				'name' => trim($name),
				'url' => trim($projects_url[$key]),
			));
		}
	}
	
}

?>