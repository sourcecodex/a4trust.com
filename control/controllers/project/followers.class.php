<?php

class followers extends controller {

	function followers() {
		
		parent::controller("followers");
		
		$this->fields = array(
			"created" => "created",
			"user_id" => "int",
			"company_id" => "int",
		);
		
		//$this->createTableStructure();
	}
	
	function is($user_id, $company_id) {
		global $coreSQL;
		return (int)$coreSQL->queryValue("SELECT `id` FROM `".$this->table."` WHERE "
				. "`user_id`=".(int)$user_id." AND `company_id`='".(int)$company_id."'");
	}
	
	function remove($user_id, $company_id) {
		global $coreSQL;
		$coreSQL->query("DELETE FROM `".$this->table."` WHERE "
				. "`user_id`=".(int)$user_id." AND `company_id`='".(int)$company_id."'");
	}
	
	function getFollowing($user_id) {
		global $coreSQL;
		return $coreSQL->queryColumn("SELECT `company_id` FROM `".$this->table."` WHERE `user_id`=".(int)$user_id, "company_id");
	}
	
}

?>