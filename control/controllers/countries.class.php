<?php

class countries extends project_countries {

	function countries() {
		global $pages;
		
		parent::project_countries();
		
		$this->fields += array(
			"capital" => "string",
			"currency" => "string",
			"population" => "string",
			"tld" => "string",
		);
		
		$this->logged_acts = array("edit_xajax");
		
		$this->edit_form = array(
			"title" => "Edit country",
			"layout" => "modal",
			"method" => "xajax",
			"ladda" => 1,
			"fields" => array(
				"id" => array(
					"type" => "hidden",
				),
				"country" => array(
					"type" => "text",
					"title" => "Country",
					"disabled" => 1,
				),
			),
			"redirect" => $pages->request_url,
		);
	}
	
	function edit($form_data) {
		parent::edit($form_data, false);
	}

}

?>