<?php

class users extends project_users {

	function users() {
		parent::project_users();
		
		$this->fields += array(
			"location" => "string",
			"login_count" => "int",
			"http_referer" => "string",
			
			"social_identifier" => "string",
			"social_provider" => "string",
			"social_profile" => "string",
			"social_photo" => "string",
		);
		
		$this->public_acts = array("signUp", "signIn", "signOut", "resetPassword", "resendConfirmation_xajax");
		$this->logged_acts = array("updateAccount_xajax", "changePassword_xajax", "changePasswordOnReset_xajax", "cancelAccount_xajax",
			"reportCompany_xajax");

		$this->sign_up_form = array(
			"layout" => "default",
			"method" => "post",
			"fields" => array(
				"email" => array(
					"type" => "email",
					"title" => "Email address",
					"placeholder" => "Enter email address",
					"prepend" => '<span class="glyphicon glyphicon-envelope"></span>',
					"required" => true,
				),
				"password" => array(
					"type" => "password",
					"title" => "Password",
					"placeholder" => "Enter password",
					"prepend" => '<span class="glyphicon glyphicon-lock"></span>',
					"required" => true,
				),
				"submit_button" => array(
					"type" => "submit_button",
					"title" => 'Sign up <i class="fa fa-sign-in"></i>',
					"align" => "right",
					"ladda" => 1,
				),
			),
			"redirect" => "",
		);

		$this->sign_in_form = array(
			"layout" => "default",
			"method" => "post",
			"fields" => array(
				"email" => array(
					"type" => "email",
					"title" => "Email address",
					"placeholder" => "Enter email address",
					"prepend" => '<span class="glyphicon glyphicon-envelope"></span>',
				),
				"password" => array(
					"type" => "password",
					"title" => "Password",
					"placeholder" => "Enter password",
					"prepend" => '<span class="glyphicon glyphicon-lock"></span>',
				),
				"submit_button" => array(
					"type" => "submit_button",
					"title" => 'Log in <i class="fa fa-sign-in"></i>',
					"align" => "right",
					"remember_me" => 1,
					"ladda" => 1,
				),
			),
			"redirect" => "",
		);

		$this->reset_pass_form = array(
			"layout" => "default",
			"method" => "post",
			"fields" => array(
				"email" => array(
					"type" => "email",
					"title" => "Email address",
					"placeholder" => "Enter email address",
					"prepend" => '<span class="glyphicon glyphicon-envelope"></span>',
					"required" => true,
				),
				"captcha" => array(
					"type" => "coolcaptcha",
					"title" => "What are the symbols?",
					"width" => 200,
					"height" => 50,
				),
				"submit_button" => array(
					"type" => "submit_button",
					"title" => "Email new password",
					"align" => "right",
					"ladda" => 1,
				),
			),
			"redirect" => "user/resetpass?confirm=1",
		);

		$this->change_pass_form = array(
			"layout" => "default",
			"method" => "xajax",
			"fields" => array(
				"current_password" => array(
					"type" => "password",
					"title" => "Current password",
					"placeholder" => "Enter current password",
					"required" => true,
				),
				"new_password" => array(
					"type" => "password",
					"title" => "New password",
					"placeholder" => "Enter new password",
					"required" => true,
				),
				"new_password2" => array(
					"type" => "password",
					"title" => "Re-type new password",
					"placeholder" => "Re-type new password",
					"required" => true,
				),
				"submit_button" => array(
					"type" => "submit_button",
					"title" => "Change password",
					"align" => "right",
					"ladda" => 1,
				),
			),
			"redirect" => "user/settings?changed=1",
		);

		$this->change_pass2_form = array(
			"layout" => "default",
			"method" => "xajax",
			"fields" => array(
				"new_password" => array(
					"type" => "password",
					"title" => "New password",
					"placeholder" => "Enter new password",
					"required" => true,
				),
				"new_password2" => array(
					"type" => "password",
					"title" => "Re-type new password",
					"placeholder" => "Re-type new password",
					"required" => true,
				),
				"submit_button" => array(
					"type" => "submit_button",
					"title" => "Change password",
					"align" => "right",
					"ladda" => 1,
				),
			),
			"redirect" => "user/settings?changed=1",
		);
		
		$this->profile_edit_form = array(
			"title" => "Profile details",
			"layout" => "default",
			"method" => "xajax",
			"fields" => array(
				"id" => array(
					"type" => "hidden",
				),
				"first_name" => array(
					"type" => "text",
					"title" => "First name",
				),
				"last_name" => array(
					"type" => "text",
					"title" => "Last name",
				),
				"submit_button" => array(
					"type" => "submit_button",
					"title" => "Save",
					"align" => "right",
					"ladda" => 1,
				),
			),
			"redirect" => "user/settings?updated=1",
		);
		
		$this->report_form = array(
			"title" => "Report a company",
			"layout" => "modal-project",
			"method" => "xajax",
			"width" => 500,
			"hide_footer" => 1,
			"fields" => array(
				"message" => array(
					"type" => "textarea",
					"title" => "Message",
				),
				"submit_button" => array(
					"type" => "submit_button",
					"title" => "Report",
					"align" => "right",
					"ladda" => 1,
				),
			),
		);

		$this->logUser();
	}
	
	function logUser() {
		global $coreSQL;
		
		project_users::logUser();
		
		if ($this->isLogged) {
			if (getIp() == '78.60.137.210' || getIp() == '88.119.206.62' || getIp() == '78.61.246.2') {
				// Do nothing
			}
			else {
				$coreSQL->query("UPDATE `".$this->table."` SET `last_online`='".date('Y-m-d H:i:s')."' WHERE `id`=".(int)$this->id);
			}
		}
	}

	function signUp($form_data) {
		global $config, $coreSQL, $coreSession, $mailer_emails, $smarty;

		// for security reason unset these vars
		unset($form_data['admin']);
		unset($form_data['email_confirmed']);
		
		$form_data = cleanArray($form_data);

		$form_data['email'] = trim($form_data['email']);
		$form_data['password'] = trim($form_data['password']);

		$error = false;

		if (strlen($form_data['password']) < 6) {
			$smarty->assign('error_password_length', 'password');
			$error = true;
		}
		
		if (strpos($form_data['password'], "'") !== false) {
			$smarty->assign('error_password_invalid', 'password');
			$error = true;
		}

		if (!isValidEmail($form_data['email'])) {
			$smarty->assign('error_email_invalid', 'email');
			$error = true;
		}

		$user_info = $this->getByEmail($form_data['email']);
		if ($user_info) {
			$smarty->assign('error_email_exists', 'email');
			$error = true;
		}

		if (!$error) {
			
			$form_data['confirm_code'] = strtoupper(substr(md5(uniqid(rand())), 0, 6));
			$form_data['password2'] = $form_data['password'];
			$form_data['active'] = 1;
			$form_data['default_lang'] = $coreSession->session['lang'];
			
			$form_data['http_referer'] = $coreSession->session['http_referer'];
			
			$parts = explode("@", $form_data['email']);
			$form_data['first_name'] = $parts[0];
			
			$user_id = $this->add($form_data);

			if ($user_id) {
				$this->updateIp($user_id);
				
				$form_data['id'] = $user_id;
				$form_data['subject'] = 'Confirm your email address to start using '.$config['project_name'];
				$mailer_emails->addConfirmationEmail($form_data);

				redirect("signup?confirm=1&email=".$form_data['email']);
			}

		}

	}
	
	function signUpSocial($form_data) {
		global $config, $smarty, $coreSQL, $coreSession, $mailer_emails;
		
		// for security reason unset these vars
		unset($form_data['admin']);
		unset($form_data['email_confirmed']);
		
		$form_data = cleanArray($form_data);

		$form_data['email'] = trim($form_data['email']);
		$form_data['password'] = trim($form_data['password']);

		$user_info = $this->getByEmail($form_data['email']);
		if ($user_info) {
			$smarty->assign('auth_error', 'This email has already been taken: '.$user_info['email']);
			return false;
		}
		
		$form_data['confirm_code'] = strtoupper(substr(md5(uniqid(rand())), 0, 6));
		$form_data['password2'] = $form_data['password'];
		$form_data['active'] = 1;
		$form_data['default_lang'] = $coreSession->session['lang'];
		
		$form_data['http_referer'] = $coreSession->session['http_referer'];
		
		$form_data['email_confirmed'] = 1;

		$user_id = $this->add($form_data);

		if ($user_id) {
			$this->updateIp($user_id);
			
			$form_data['id'] = $user_id;
			$form_data['subject'] = 'Welcome to '.$config['project_name'].'!';
			$mailer_emails->addWelcomeEmail($form_data);
			
			$this->signIn($form_data);
			
			redirect('');
		}
		
	}

	function confirmEmail($form_data) {
		global $config, $coreSQL, $mailer_emails, $smarty;

		$user_info = $this->getByUserCodeHash($form_data['u'], $form_data['c'], $form_data['h']);

		if ($user_info) {

			if (!$user_info['email_confirmed']) {
				$user_info['subject'] = 'Welcome to '.$config['project_name'].'!';
				$mailer_emails->addWelcomeEmail($user_info);
			}

			$coreSQL->query("UPDATE `".$this->table."` SET `email_confirmed`=1 WHERE `id`=".(int)$user_info['id']);

			if (!$this->isLogged) {
				$user_info['password'] = $user_info['password2'];
				$this->signIn($user_info);
			}

			redirect("");
		}
		else {
			$smarty->assign('error_incorrect_code', 'Incorrect confirmation code');
		}

	}

	function resendConfirmation($form_data) {
		global $config, $mailer_emails;

		$user_info = $this->getByEmail($form_data['email']);
		if ($user_info) {
			if (!$user_info['email_confirmed']) {
				$user_info['subject'] = 'Confirm your email address to start using '.$config['project_name'];
				$mailer_emails->addConfirmationEmail($user_info);
				return true;
			}
			else {
				return false;
			}
		}
	}

	function signIn($form_data) {
		if (getIp() == '78.60.137.210' || getIp() == '88.119.206.62' || getIp() == '78.61.246.2') {
			$form_data['disable_update_ip'] = true;
		}
		
		project_users::signIn($form_data);
	}

	function signInHook($form_data) {
		global $coreSQL, $mailer_emails;
		
		if (getIp() == '78.60.137.210' || getIp() == '88.119.206.62' || getIp() == '78.61.246.2') {
			
			$coreSQL->query("UPDATE `".$this->table."` SET `login_count`=`login_count`+1 WHERE `id`=".(int)$this->id);
			
			$user_data = $this->getById($this->id);
			$mailer_emails->addLoginEmail($user_data);
		}
	}

	function signOut($form_data) {
		project_users::signOut($form_data);
	}

	function autoLogin($form_data) {
		global $pages;

		if (getIp() == '78.60.137.210' || getIp() == '88.119.206.62' || getIp() == '78.61.246.2') {
			$form_data['disable_update_ip'] = true;
		}
		
		$form_data['act_redirect'] = $pages->url;
		project_users::autoLogin($form_data);
	}
	
	function socialAuthenticate($provider, $success_redirect) {
		project_users::socialAuthenticate($provider, $success_redirect);
	}

	function socialGet($provider, $error_redirect, $get_data) {
		return project_users::socialGet($provider, $error_redirect, $get_data);
	}
	
	function resetPassword($form_data) {
		$form_data['subject'] = 'Reset password';
		project_users::resetPassword($form_data);
	}

	function changePassword($form_data) {
		project_users::changePassword($form_data);
	}

	function changePasswordOnReset($form_data) {
		project_users::changePasswordOnReset($form_data);
	}

	function edit($form_data) {
		unset($form_data['password']);
		
		project_users::edit($form_data);
	}
	
	function updateAccount($form_data) {
		$user_data = array();
		$user_data['id'] = $this->id;
		$user_data['first_name'] = $form_data['first_name'];
		$user_data['last_name'] = $form_data['last_name'];

		$this->edit($user_data);
	}
	
	function cancelAccount($form_data) {
		global $mailer_emails;
		
		$user_info = $this->getById($this->id);
		$user_info['cancel_reason'] = $form_data['reason'];
		
		$mailer_emails->addCancelAccountEmail($user_info);
	}
	
	function reportCompany($form_data) {
		global $pages, $mailer_emails;
		
		$user_info = $this->getById($this->id);
		$user_info['company_id'] = (int)$pages->path[2];
		$user_info['message'] = $form_data['message'];
		
		$mailer_emails->addReportEmail($user_info);
	}
	
}

?>