<?php

function searchCompanies2($field, $name, $selected_companies) {
	global $coreSQL, $smarty;

	$objResponse = new xajaxResponse();
	$name = htmlspecialchars(trim($name));
	
	if ($name != "" && strlen($name) >= 1) {
		$result = $coreSQL->queryData("SELECT * FROM `companies` WHERE `name` LIKE '".clean($name)."%' ".
				($selected_companies?"AND `id` NOT IN (".implode(",", $selected_companies).")":"")
				. " ORDER BY `name` ASC LIMIT 10");

		if ($result) {
			$smarty->assign('field', $field);
			$smarty->assign('companies', $result);
			$content = $smarty->fetch("components/xajax/search_companies2.tpl");
			
			$objResponse->addAssign($field."_suggestions", "innerHTML", $content);
			$objResponse->addScriptCall("showCompanySuggestions2", $field);
		}
		else {
			$objResponse->addScriptCall("hideCompanySuggestions2", $field);
		}
		
	}
	else {
		$objResponse->addAssign($field."_suggestions", "innerHTML", "");
		$objResponse->addScriptCall("hideCompanySuggestions2", $field);
	}
	
	return $objResponse;
}

$xajax->registerFunction("searchCompanies2");

?>
