<?php

function followCompany($company_id) {
	global $users, $followers;

	$objResponse = new xajaxResponse();
	
	if ($followers->is($users->id, $company_id)) {
		$followers->remove($users->id, $company_id);
		$objResponse->addAssign("follow_icon", "className", "fal fa-bookmark");
	}
	else {
		$followers->add(array(
			'user_id' => $users->id,
			'company_id' => (int)$company_id,
		));
		$objResponse->addAssign("follow_icon", "className", "fas fa-bookmark");
	}
	
	return $objResponse;
}

$xajax->registerFunction("followCompany");

?>