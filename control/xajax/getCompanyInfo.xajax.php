<?php

function getCompanyInfo($company_id) {
	global $companies, $smarty;

	$objResponse = new xajaxResponse();
	
	$smarty->assign('company', $companies->getById($company_id));
	$content = $smarty->fetch("components/xajax/get_company_info.tpl");

	$objResponse->addAssign("company_selected", "innerHTML", $content);
	
	return $objResponse;
}

$xajax->registerFunction("getCompanyInfo");

?>
