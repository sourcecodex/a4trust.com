<?php

function addProjectRow($row_id) {
	global $smarty;

	$objResponse = new xajaxResponse();

	$row_id = (int)$row_id;
	
	$smarty->assign('row_id', $row_id);

	$content = $smarty->fetch('components/xajax/project_row.tpl');
	
	$objResponse->addInsertAfter("projects_row".($row_id-1), "div", "projects_row".$row_id);
	$objResponse->addAssign("projects_row".$row_id, "innerHTML", $content);
	
	// update add button
	$objResponse->addEvent("projects_plus", "onclick", "xajax_addProjectRow(".($row_id+1).")");
	
	return $objResponse;
}

$xajax->registerFunction("addProjectRow");

?>
