<?php

function addAddressRow($row_id) {
	global $smarty;

	$objResponse = new xajaxResponse();

	$row_id = (int)$row_id;
	
	$smarty->assign('row_id', $row_id);

	$content = $smarty->fetch('components/xajax/address_row.tpl');
	
	$objResponse->addInsertAfter("addresses_row".($row_id-1), "div", "addresses_row".$row_id);
	$objResponse->addAssign("addresses_row".$row_id, "innerHTML", $content);
	
	// update add button
	$objResponse->addEvent("addresses_plus", "onclick", "xajax_addAddressRow(".($row_id+1).")");
	
	return $objResponse;
}

$xajax->registerFunction("addAddressRow");

?>
