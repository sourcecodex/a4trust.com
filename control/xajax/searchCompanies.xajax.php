<?php

function searchCompanies($name) {
	global $coreSQL, $smarty;

	$objResponse = new xajaxResponse();
	$name = htmlspecialchars(trim($name));
	
	if ($name != "" && strlen($name) >= 1) {
		$result = $coreSQL->queryData("SELECT * FROM `companies` WHERE `user_id`=0 AND "
				. "`name` LIKE '".clean($name)."%' ORDER BY `name` ASC LIMIT 10");

		if ($result) {			
			$smarty->assign('companies', $result);
			$content = $smarty->fetch("components/xajax/search_companies.tpl");
			
			$objResponse->addAssign("company_suggestions", "innerHTML", $content);
			$objResponse->addScriptCall("showCompanySuggestions");
		}
		else {
			$objResponse->addScriptCall("hideCompanySuggestions");
		}
		
		$is_company = (int)$coreSQL->queryValue("SELECT `id` FROM `companies` WHERE `name`='".$name."' LIMIT 1");
		if ($is_company) {
			$objResponse->addScriptCall("setCompanyId", $is_company);
		}
		else {
			$objResponse->addScriptCall("unsetCompanyId");
		}
	}
	else {
		$objResponse->addAssign("company_suggestions", "innerHTML", "");
		$objResponse->addScriptCall("hideCompanySuggestions");
		$objResponse->addScriptCall("unsetCompanyId");
	}
	
	return $objResponse;
}

$xajax->registerFunction("searchCompanies");

?>
