<?php

$smarty->assign('default_libs', array("jquery", "bootstrap", "font-awesome", "ladda-bootstrap", 
									"bootstrap-rating", "tinymce"));

$smarty->assign('default_css_styles', array("style.css", "default.css"));

if (!isset($coreSession->session['http_referer'])) {
	$coreSession->session['http_referer'] = $_SERVER['HTTP_REFERER'];
}

if ($users->isLogged) {
	$smarty->assign('company_profile', $companies->getByUser($users->id));
}

?>