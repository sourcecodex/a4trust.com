<?php

if ($users->isLogged) {
	
	$smarty->assign('company_types', $companies->types);
	$smarty->assign('sectors', $companies->sectors);
	$smarty->assign('products', $companies->products);
	$smarty->assign('ratings', $companies->ratings);
	
	$_POST = $companies->getByUser($users->id);
	
	$smarty->assign('company', $_POST);
	
	$smarty->assign('addresses', $addresses->getByCompany($_POST['id']));
	$smarty->assign('projects', $projects->getByCompany($_POST['id']));
	
	$smarty->assign('producers_selected', $companies->getByIds($_POST['producers']));
	$smarty->assign('related_companies_selected', $companies->getByIds($_POST['related_companies']));
	
}
else {
	redirect('');
}

?>
