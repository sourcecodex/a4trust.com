<?php

if ($users->isLogged) {
	
	$smarty->assign('following', $companies->getByIds($followers->getFollowing($users->id)));
	
}
else {
	redirect('');
}

?>