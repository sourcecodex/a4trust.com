<?php

if ($users->isLogged) {
	
	$smarty->assign('company_types', $companies->types);
	$smarty->assign('sectors', $companies->sectors);
	$smarty->assign('products', $companies->products);
	
	$company = $companies->getById($pages->path[2]);
	
	$smarty->assign('company', $company);
	
	$smarty->assign('addresses', $addresses->getByCompany($company['id']));
	$smarty->assign('projects', $projects->getByCompany($company['id']));
	
	$smarty->assign('producers', $companies->getByIds($company['producers']));
	$smarty->assign('related_companies', $companies->getByIds($company['related_companies']));
	
	$smarty->assign('following', (int)$followers->is($users->id, $company['id']));
	
}
else {
	redirect('');
}

?>