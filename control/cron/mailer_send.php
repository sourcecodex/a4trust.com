<?php

include_once "../../config.php";

$config['disable_core_session'] = true;
include_once $config['core_dir']."load_min.php";

require_once $config['core_dir']."project/control/controllers/project_mailer_contacts.class.php";
require_once $config['control_dir']."controllers/mailer/mailer_contacts.class.php";
$mailer_contacts = new mailer_contacts();

require_once $config['core_dir']."project/control/controllers/project_mailer_emails.class.php";
require_once $config['control_dir']."controllers/mailer/mailer_emails.class.php";
$mailer_emails = new mailer_emails();

$send_emails = $coreSQL->queryData("SELECT * FROM `mailer_emails` WHERE `status`=0 ORDER BY `priority` ASC LIMIT 5");

if ($send_emails) {
	require_once $config['core_dir']."core/classes/mailer.class.php";
	
	foreach ($send_emails as $send_email) {

		$contact_info = $mailer_contacts->getByEmail($send_email['to']);
		
		if ($contact_info) {

			if (!$contact_info['ignore']) {

				$send = true;

				if ((int)$send_email['sending_options'] == 1) {
					// All except undelivered and members
					if ($contact_info['user_id'] || $contact_info['undelivered']) $send = false;
				}
				elseif ((int)$send_email['sending_options'] == 2) {
					// All Except undelivered
					if ($contact_info['undelivered']) $send = false;
				}
				elseif ((int)$send_email['sending_options'] == 3) {
					// All Except members
					if ($contact_info['user_id']) $send = false;
				}
				elseif ((int)$send_email['sending_options'] == 4) {
					// Unique contacts
					$send = false;
				}
				else {
					// All (no filter)
				}

				if ($send) {
					$mailer_user = $coreSQL->queryRow("SELECT * FROM `mailer_users` WHERE `user`='".$send_email['user_from']."'");

					$coreMailer = new coreMailer($mailer_user);
					if (!empty($send_email['user_name'])) {
						$coreMailer->changeFromName($send_email['user_name']);
					}
					$coreMailer->sendEmail($send_email['to'], $send_email['subject'], $send_email['body'], null, null, null, $send_email['body']);

					$mailer_contacts->addUniq(array('email' => $send_email['to']));

					$coreSQL->query("UPDATE `mailer_emails` SET `status`=1 WHERE `id`=".(int)$send_email['id']);
				}
				else {
					$coreSQL->query("UPDATE `mailer_emails` SET `status`=3 WHERE `id`=".(int)$send_email['id']);
				}

			}
			else $coreSQL->query("UPDATE `mailer_emails` SET `status`=2 WHERE `id`=".(int)$send_email['id']);

		}
		else {
			$mailer_user = $coreSQL->queryRow("SELECT * FROM `mailer_users` WHERE `user`='".$send_email['user_from']."'");

			$coreMailer = new coreMailer($mailer_user);
			if (!empty($send_email['user_name'])) {
				$coreMailer->changeFromName($send_email['user_name']);
			}
			$coreMailer->sendEmail($send_email['to'], $send_email['subject'], $send_email['body'], null, null, null, $send_email['body']);

			$mailer_contacts->addUniq(array('email' => $send_email['to']));

			$coreSQL->query("UPDATE `mailer_emails` SET `status`=1 WHERE `id`=".(int)$send_email['id']);
		}

	}
}

$coreSQL->disconect();

?>