<?php

// blocked IPs
if (getIp() == "185.130.184.203" || getIp() == "185.17.149.160" || getIp() == "85.206.165.5") {
	die();
}

if ($_GET['act'] == "confirm_email") {
	$users->confirmEmail($_GET);
}
else if (isset($_GET['u']) && $_GET['c'] != "" && $_GET['h'] != "") {
	$mailer_emails->setClick($_GET['id']);
	
	$users->autoLogin($_GET);
}

if ($_GET['act'] == "mailer_click") {
	$mailer_emails->setClick($_GET['id']);
	redirect("");
}

// ============================= Social Authentication =========================================

if ($_GET['act'] == "signup_facebook") {
	$users->socialAuthenticate("Facebook", "/signup?provider=");
}

if ($_GET['act'] == "signup_google") {
	$users->socialAuthenticate("Google", "/signup?provider=");
}

if ($_GET['act'] == "signup_twitter") {
	$users->socialAuthenticate("Twitter", "/signup?provider=");
}

if ($_GET['act'] == "signup_linkedin") {
	$users->socialAuthenticate("LinkedIn", "/signup?provider=");
}

if ($_GET['act'] == "login_facebook") {
	$users->socialAuthenticate("Facebook", "/login?provider=");
}

if ($_GET['act'] == "login_google") {
	$users->socialAuthenticate("Google", "/login?provider=");
}

if ($_GET['act'] == "login_twitter") {
	$users->socialAuthenticate("Twitter", "/login?provider=");
}

?>