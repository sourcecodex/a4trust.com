<?php

class users extends admin_users {

	function users() {
		parent::admin_users();

		$this->fields += array(
			
			"login_count" => "int",
			
		);

		$extra_cols_list = array(
			"info" => array(
				"type" => "info_button",
				"title" => "Info",
				"popup" => "users/info",
			),
			"edit" => array(
				"type" => "edit_button",
				"title" => "Edit",
				"popup" => "users/edit",
			),
		);

		array_insert($this->list_table['columns'], 7, $extra_cols_list);
		
		unset($this->list_table['columns']['default_lang']);
		unset($this->list_table['columns']['user_ip']);
		
		$this->list_table['columns']['created'] = array(
			"custom" => 1,
			"type" => "time_past",
			"title" => "Created__",
			"sorting" => 1,
		);
		
		$this->list_table['columns']['last_online'] = array(
			"custom" => 1,
			"type" => "time_past",
			"title" => "Logged___",
			"sorting" => 1,
		);
		
		$this->info_modal = array(
			"title" => "User Info",
			"layout" => "default",
			"width" => 800,
		);
		
		$this->list_modal = array(
			"title" => "Users List",
			"layout" => "default",
			"width" => 800,
		);
		
		unset($this->add_form['fields']['admin']);
		unset($this->add_form['fields']['default_lang']);
		unset($this->add_form['fields']['active']);
		unset($this->add_form['fields']['email_confirmed']);
		
		unset($this->edit_form['fields']['admin']);
		unset($this->edit_form['fields']['default_lang']);
		unset($this->edit_form['fields']['password']);
		

		$extra_cols_search = array(
			"id" => array(
				"type" => "text",
				"title" => "User Id",
			),
		);
		
		array_insert($this->search_form['fields'], 1, $extra_cols_search);
		
		$this->search_query_fields = array("user_ip", "first_name", "email");
		
		$this->sign_in_form['redirect'] = "users";

		$this->logUser();
	}
	
	function delete($form_data) {
		
		$user_info = $this->getById($form_data['id']);

		if ($user_info && (int)$user_info['admin'] == 0) {
			
			parent::delete($form_data);
			
		}
	}
	
}

?>