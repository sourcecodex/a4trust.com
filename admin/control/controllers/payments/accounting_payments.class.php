<?php

class accounting_payments extends admin_accounting_payments {

    function accounting_payments() {
		parent::admin_accounting_payments();
	}

	function addManual($form_data) {
		global $coreSQL;
		
		$payment_id = parent::addManual($form_data);
		$payment = $this->getById($payment_id);

		if ($payment['product_id'] == 1 || $payment['product_id'] == 2 || $payment['product_id'] == 8 ||
			$payment['product_id'] == 9 || $payment['product_id'] == 10 || $payment['product_id'] == 22 || 
			$payment['product_id'] == 23) {
			$plan = 'bronze';
		}
		elseif ($payment['product_id'] == 3 || $payment['product_id'] == 4 ||
				$payment['product_id'] == 11 || $payment['product_id'] == 12 || 
				$payment['product_id'] == 24 || $payment['product_id'] == 25) {
			$plan = 'silver';
		}
		elseif ($payment['product_id'] == 5 || $payment['product_id'] == 6 ||
				$payment['product_id'] == 13 || $payment['product_id'] == 14 || 
				$payment['product_id'] == 26 || $payment['product_id'] == 27) {
			$plan = 'gold';
		}
		elseif ($payment['product_id'] == 18 || $payment['product_id'] == 19) {
			$plan = 'platinum';
		}
		elseif ($payment['product_id'] == 20 || $payment['product_id'] == 21) {
			$plan = 'enterprise';
		}
		else {
			$plan = false;
		}
		
		if ($plan) {
			$user_info = $coreSQL->queryRow("SELECT * FROM `users` WHERE `id`=".(int)$payment['user_id']);
			if ($user_info) {
				$data = array(
					"plan" => $plan,
					"plan_start" => $payment['date_start'],
					"plan_end" => $payment['date_end'],
					"payments" => $user_info['payments']+1,
					"trial_expired" => "0",
				);

				$coreSQL->updateArray($data, "users", "`id`=".(int)$user_info['id']);
			}
		}

	}

}

?>