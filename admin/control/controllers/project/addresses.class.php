<?php

class addresses extends controller {

	function addresses() {
		
		parent::controller("addresses");
		
		$this->fields = array(
			"created" => "created",
			"company_id" => "int",
			"name" => "string",
			"address" => "string",
		);
		
		//$this->createTableStructure();
	}
	
	function getByCompany($company_id) {
		global $coreSQL;
		return $coreSQL->queryData("SELECT * FROM `".$this->table."` WHERE `company_id`=".(int)$company_id." ORDER BY `id`");
	}

	function deleteByCompany($company_id) {
		global $coreSQL;
		$coreSQL->query("DELETE FROM `".$this->table."` WHERE `company_id`='".(int)$company_id."'");
	}
	
	function set($form_data) {
		
		$this->deleteByCompany($form_data['id']);
		
		$addresses_name = createArrayFromField($form_data, 'addresses_name');
		$addresses_address = createArrayFromField($form_data, 'addresses_address');
		
		foreach ($addresses_name as $key => $name) {
			$this->add(array(
				'company_id' => (int)$form_data['id'],
				'name' => trim($name),
				'address' => trim($addresses_address[$key]),
			));
		}
	}
	
}

?>