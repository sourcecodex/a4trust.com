<?php

class news extends controller {

	function news() {
		global $pages;
		
		parent::controller("news");

		$this->fields = array(
			"created" => "created",
			"user_id" => "int",
			"title" => "string",
			"description" => "string",
			"body" => "text",
			"feature_img" => "string",
			"header_img" => "string",
		);

		$this->logged_acts = array("add_xajax", "edit_xajax", "delete_xajax", 
			"uploadFeatureImage", "removeFeatureImage", "uploadHeaderImage", "removeHeaderImage");
		
		$this->list_table = array(
			"layout" => "default",
			"options" => array("striped", "hover", "condensed"),
			"header" => 1,
			"columns" => array(
				"ids" => array(
					"type" => "id_checkbox",
				),
				"feature_img" => array(
					"type" => "img_button",
					"title" => "Feature",
					"popup" => "news/feature_img",
				),
				"header_img" => array(
					"type" => "img_button",
					"title" => "Header",
					"popup" => "news/header_img",
				),
				"title" => array(
					"type" => "text",
					"title" => "Title",
					"sorting" => 1,
				),
				"edit" => array(
					"type" => "edit_button",
					"title" => "Edit",
					"popup" => "news/edit",
				),
				"remove" => array(
					"type" => "remove_button",
					"title" => "Remove",
					"confirmation" => "Are you sure you want to remove this company?",
				),
			),
			"rows" => array(),
		);
		
		$this->add_form = array(
			"title" => "Add new",
			"layout" => "modal",
			"method" => "xajax",
			"fields" => array(
				"title" => array(
					"type" => "text",
					"title" => "Title",
				),
				"body" => array(
					"type" => "editor",
					"title" => "Body",
				),
			),
			"redirect" => $pages->request_url,
		);

		// fix redirect if results filtered
		if (strpos($pages->request_url, "page=") !== false) {
			$edit_redirect_path = $pages->request_url;
		}
		else {
			$edit_redirect_path = $pages->request_url."?page=1";
		}
		
		$this->edit_form = array(
			"title" => "Edit new",
			"layout" => "modal",
			"method" => "xajax",
			"fields" => array(
				"id" => array(
					"type" => "hidden",
				),
				"title" => array(
					"type" => "text",
					"title" => "Title",
				),
				"body" => array(
					"type" => "editor",
					"title" => "Body",
				),
			),
			"redirect" => $edit_redirect_path,
		);
		
		$this->search_form = array(
			"title" => "Search news",
			"layout" => "modal",
			"method" => "post",
			"action" => "news",
			"submit_title" => "Search",
			"fields" => array(
				"title" => array(
					"type" => "text",
					"title" => "Title",
				),
				"body" => array(
					"type" => "text",
					"title" => "Body",
				),
			),
		);
		
		$this->feature_img_form = array(
			"title" => "Feature image",
			"layout" => "modal",
			"method" => "post",
			"multipart" => 1,
			"hide_footer" => 1,
			"fields" => array(
				"id" => array(
					"type" => "hidden",
				),
				"feature_img" => array(
					"type" => "image",
					"title" => "Feature image",
					"remove_act" => "removeFeatureImage",
					"div_style" => "width: 200px;",
				),
				"submit_button" => array(
					"type" => "submit_button",
					"title" => "Upload",
					"align" => "left",
					"ladda" => 1,
				),
			),
		);
		
		$this->header_img_form = array(
			"title" => "Header image",
			"layout" => "modal",
			"method" => "post",
			"multipart" => 1,
			"hide_footer" => 1,
			"fields" => array(
				"id" => array(
					"type" => "hidden",
				),
				"header_img" => array(
					"type" => "image",
					"title" => "Header image",
					"remove_act" => "removeHeaderImage",
					"div_style" => "width: 200px;",
				),
				"submit_button" => array(
					"type" => "submit_button",
					"title" => "Upload",
					"align" => "left",
					"ladda" => 1,
				),
			),
		);
		
		//$this->createTableStructure();
	}
	
	function add($form_data) {
		global $users;
		
		$form_data['user_id'] = (int)$users->id;
		
		parent::add($form_data, false);
	}
	
	function edit($form_data) {
		parent::edit($form_data, false);
	}
	
	function uploadFeatureImage($form_data) {
		global $config, $coreSQL;
		
		$new_id = (int)$form_data['id'];
		
		if (isset($_FILES['feature_img']['error']) && $_FILES['feature_img']['error'] == 0) {
			$tmp_name = $_FILES['feature_img']['tmp_name'];

			$uploaddir = dirname($config['main_dir']).'/data/news/'.$new_id;
			if (!file_exists($uploaddir)) {
				mkdir($uploaddir, 0777);
			}
			
			$new_file = $uploaddir.'/feature_img.png';

			if (!file_exists($new_file)) {
				create_thumbnail_png($tmp_name, $new_file, 430, 430, true);
				
				$img_path = 'data/news/'.$new_id.'/feature_img.png';
				$coreSQL->query("UPDATE `".$this->table."` SET `feature_img`='".$img_path."' WHERE `id`=".(int)$new_id);
			}
		}
		
		redirect('news');
	}
	
	function removeFeatureImage($form_data) {
		global $config, $coreSQL;
		
		$new_id = (int)$form_data['id'];
		
		$new_file = dirname($config['main_dir']).'/data/news/'.$new_id.'/feature_img.png';
		if (file_exists($new_file)) {
			unlink($new_file);
		}
		
		$coreSQL->query("UPDATE `".$this->table."` SET `feature_img`='' WHERE `id`=".(int)$new_id);
		
		redirect('news');
	}
	
	function uploadHeaderImage($form_data) {
		global $config, $coreSQL;
		
		$new_id = (int)$form_data['id'];
		
		if (isset($_FILES['header_img']['error']) && $_FILES['header_img']['error'] == 0) {
			$tmp_name = $_FILES['header_img']['tmp_name'];

			$uploaddir = dirname($config['main_dir']).'/data/news/'.$new_id;
			if (!file_exists($uploaddir)) {
				mkdir($uploaddir, 0777);
			}
			
			$new_file = $uploaddir.'/header_img.png';

			if (!file_exists($new_file)) {
				create_thumbnail_png($tmp_name, $new_file, 1200, 1200, true);
				
				$img_path = 'data/news/'.$new_id.'/header_img.png';
				$coreSQL->query("UPDATE `".$this->table."` SET `header_img`='".$img_path."' WHERE `id`=".(int)$new_id);
			}
		}
		
		redirect('news');
	}
	
	function removeHeaderImage($form_data) {
		global $config, $coreSQL;
		
		$new_id = (int)$form_data['id'];
		
		$new_file = dirname($config['main_dir']).'/data/news/'.$new_id.'/header_img.png';
		if (file_exists($new_file)) {
			unlink($new_file);
		}
		
		$coreSQL->query("UPDATE `".$this->table."` SET `header_img`='' WHERE `id`=".(int)$new_id);
		
		redirect('news');
	}
	
	// ============================= Search ============================================

	function searchQuery($filter) {
		global $users;
		
		//$filter['user_id'] = (int)$users->id;
		
		if (!empty($filter['search_query'])) {
			$search_sql = " AND (`title` LIKE '%".addslashes($filter['search_query'])."%')";
		}

		return parent::searchQuery($filter, array("created" => "DESC"), 50, $search_sql);
	}

}

?>