<?php

class requests extends controller {

	function requests() {
		
		parent::controller("requests");
		
		$this->fields = array(
			"created" => "created",
			"user_id" => "int",
			"company_id" => "int",
			"status" => "string",
		);
		
		$this->logged_acts = array("approve_xajax", "delete_xajax");
		
		$this->list_table = array(
			"layout" => "default",
			"options" => array("striped", "hover", "condensed"),
			"header" => 1,
			"columns" => array(
				"ids" => array(
					"type" => "id_checkbox",
				),
				"user_id" => array(
					"type" => "text",
					"title" => "User",
				),
				"company_id" => array(
					"type" => "text",
					"title" => "Company",
				),
				"status" => array(
					"type" => "text",
					"title" => "Status",
				),
				"approve" => array(
					"type" => "call_button",
					"title" => "Approve",
					"function" => "approve",
					"icon" => "ok",
				),
				"remove" => array(
					"type" => "remove_button",
					"title" => "Remove",
				),
			),
			"rows" => array(),
		);
		
		$this->search_form = array(
			"title" => "Search requests",
			"layout" => "modal",
			"method" => "post",
			"action" => "requests",
			"submit_title" => "Search",
			"fields" => array(
				"user_id" => array(
					"type" => "text",
					"title" => "User",
				),
			),
		);
		
		//$this->createTableStructure();
	}
	
	function approve($form_data) {
		global $coreSQL, $companies;
		
		$request = $this->getById($form_data['id']);
		$company = $companies->getById($request['company_id']);
		
		if ($company){
			// check is already not assigned
			if ((int)$company['user_id'] == 0) {
				$coreSQL->query("UPDATE `companies` SET `user_id`=".(int)$request['user_id']." WHERE `id`=".(int)$request['company_id']);
				$coreSQL->query("DELETE FROM `".$this->table."` WHERE `id`=".(int)$form_data['id']);
				
				// send email to user about approval
			}
		}
		
	}
	
	function searchQuery($filter) {
		global $users;
		
		//$filter['user_id'] = (int)$users->id;
		
		if (!empty($filter['search_query'])) {
			$search_sql = " AND (`name` LIKE '%".addslashes($filter['search_query'])."%')";
		}

		$result = parent::searchQuery($filter, array("created" => "DESC"), 50, $search_sql);
		
		return $result;
	}
	
}

?>