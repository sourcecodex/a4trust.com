<?php

class creatives extends controller {

	function creatives() {
		parent::controller("creatives");

		$this->fields = array(
			"campaign" => "string",
			"creative" => "string",
		);

		//$this->createTableStructure();
	}
	
	function getCreatives() {
		$data = $this->getAll();
		$result = array();
		foreach ($data as $row) {
			$result[$row['creative']] = $row['campaign'];
		}
		return $result;
	}
	
	function getCreativesByCampaign() {
		$data = $this->getAll();
		$result = array();
		foreach ($data as $row) {
			$result[$row['campaign']][] = $row['creative'];
		}
		return $result;
	}

}

?>