<?php

function createArrayFromField($form_data, $field) {
	$result = array();
	foreach ($form_data as $key => $value) {
		if (substr($key, 0, strlen($field)) == $field) {
			if (!empty($value)) {
				$result[] = $value;
			}
		}
	}
	return $result;
}

?>