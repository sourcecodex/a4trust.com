<?php

if ($users->isLogged) {
	
	$smarty->assign('user_ips', $coreSQL->queryColumn("SELECT `user_ip` FROM `users` WHERE 1", "user_ip"));
	
	$grouped_ips = $coreSQL->queryData("SELECT `user_ip`, COUNT(`user_ip`) AS `cc` FROM `refclicks` "
			. "WHERE 1 GROUP BY `user_ip` ORDER BY COUNT(`user_ip`) DESC");
	
	$ip_counts = array();
	foreach ($grouped_ips as $ip) {
		$ip_counts[$ip['user_ip']] = $ip['cc'];
	}
	
	$smarty->assign('ip_counts', $ip_counts);
	
	if ($pages->path[1] == "seo") {
		$search_sql = " AND (`http_referer` LIKE '%www.google.%' OR `http_referer` LIKE '%www.bing.com%' OR `http_referer` LIKE '%search.yahoo.com%')";
	}
	elseif ($pages->path[1] == "social") {
		$search_sql = " AND NOT (`http_referer` LIKE '%www.google.%' OR `http_referer` LIKE '%www.bing.com%' OR `http_referer` LIKE '%search.yahoo.com%' OR "
				. "`http_referer` LIKE '%www.baidu.com%' OR `http_referer` LIKE '%yandex.com%' OR `http_referer` LIKE '%yandex.ru%' OR "
				. "`http_referer` LIKE '%ads.google.com%' OR `http_referer` LIKE '%mail.google.com%' OR `http_referer` LIKE '%com.google.android.gm%')";
	}
	
	$refclicks->list_table['rows'] = $refclicks->searchQuery($_POST, array("created" => "DESC"), 50, $search_sql);
}
else {
	redirect('');
}

?>