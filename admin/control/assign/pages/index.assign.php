<?php

if ($users->isLogged) {
	
	$dashboard = array();
	$dashboard['users_count'] = $coreSQL->queryValue("SELECT COUNT(*) FROM `users`");
	$dashboard['subscribed_count'] = $coreSQL->queryValue("SELECT COUNT(*) FROM `users` WHERE `plan`!=''");
	$dashboard['standard_count'] = $coreSQL->queryValue("SELECT COUNT(*) FROM `users` WHERE `plan`='standard'");
	$dashboard['premium_count'] = $coreSQL->queryValue("SELECT COUNT(*) FROM `users` WHERE `plan`='premium'");
	$dashboard['plan_ended_count'] = $coreSQL->queryValue("SELECT COUNT(*) FROM `users` WHERE `plan`='' AND `payments`>0");
	$dashboard['trial_reactivated_count'] = $coreSQL->queryValue("SELECT COUNT(*) FROM `users` WHERE `trial_reactivated`=1");
	$dashboard['trial_expired_count'] = $coreSQL->queryValue("SELECT COUNT(*) FROM `users` WHERE `trial_expired`=1");
	
	$dashboard['keywords0_count'] = $coreSQL->queryValue("SELECT COUNT(*) FROM `users` WHERE `keywords_count`=0");
	$dashboard['keywords1_count'] = $coreSQL->queryValue("SELECT COUNT(*) FROM `users` WHERE `keywords_count`>=1 AND `keywords_count`<1000");
	$dashboard['keywords2_count'] = $coreSQL->queryValue("SELECT COUNT(*) FROM `users` WHERE `keywords_count`>=1000 AND `keywords_count`<3000");
	$dashboard['keywords3_count'] = $coreSQL->queryValue("SELECT COUNT(*) FROM `users` WHERE `keywords_count`>=3000 AND `keywords_count`<=5000");
	$dashboard['keywords4_count'] = $coreSQL->queryValue("SELECT COUNT(*) FROM `users` WHERE `keywords_count`>5000");
	
	$dashboard['no_gwt_count'] = $coreSQL->queryValue("SELECT COUNT(*) FROM `users` WHERE `gwt_token`=''");
	$dashboard['level0_count'] = $coreSQL->queryValue("SELECT COUNT(*) FROM `users` WHERE `level`=0");
	$dashboard['level1_count'] = $coreSQL->queryValue("SELECT COUNT(*) FROM `users` WHERE `level`=1");
	$dashboard['level2_count'] = $coreSQL->queryValue("SELECT COUNT(*) FROM `users` WHERE `level`=2");
	$dashboard['level3_count'] = $coreSQL->queryValue("SELECT COUNT(*) FROM `users` WHERE `level`=3");
	$dashboard['level4_count'] = $coreSQL->queryValue("SELECT COUNT(*) FROM `users` WHERE `level`=4");
	$dashboard['level5_count'] = $coreSQL->queryValue("SELECT COUNT(*) FROM `users` WHERE `level`=5");
	
	$smarty->assign('dashboard', $dashboard);
	
	function assignChartData($title, $ahref, $chart_id, $table, $period, $query, $time_column = "created", $value_selector = "COUNT(*)") {
		global $coreSQL, $smarty;
		
		if ($period == "weekly") {
			$group_by = "YEARWEEK(`".$time_column."`)";
			$time_query = "`".$time_column."`>'".date("Y-m-d H:i:s", strtotime("-12 month"))."'";
		}
		else {
			$group_by = "DATE_FORMAT(`".$time_column."`, '%m-%d')";
			$time_query = "`".$time_column."`>'".date("Y-m-d H:i:s", strtotime("-3 month"))."'";
		}

		$chart_data = $coreSQL->queryData("SELECT ".$group_by." AS `date`, ".$value_selector." as `value` FROM `".$table."` "
				. "WHERE $time_query $query "
				. "GROUP BY ".$group_by." ORDER BY `".$time_column."` ASC");

		$total = 0; $counter = 0;
		foreach ($chart_data as $item) { $total += (int)$item['value']; $counter++; }
		
		$smarty->assign('chart'.$chart_id.'_data', json_encode($chart_data));
		$smarty->assign('chart'.$chart_id.'_total', $total);
		if ($total > 0) $smarty->assign('chart'.$chart_id.'_average', round($total/$counter, 1));
		if ($total > 0) $smarty->assign('chart'.$chart_id.'_monthly', ($period == "weekly"?round($total/12, 1):round($total/3, 1)) );
		$smarty->assign('chart'.$chart_id.'_title', $title);
		$smarty->assign('chart'.$chart_id.'_ahref', $ahref);
		
		return $chart_data;
	}
	
	function assignChartData2($title, $ahref, $chart_id, $table, $period, $query, $query2) {
		global $coreSQL, $smarty;
		
		$chart_data = assignChartData($title, $ahref, $chart_id, $table, $period, $query);
		
		if ($period == "weekly") {
			$group_by = "YEARWEEK(`created`)";
			$time_query = "`created`>'".date("Y-m-d H:i:s", strtotime("-12 month"))."'";
		}
		else {
			$group_by = "DATE_FORMAT(`created`, '%m-%d')";
			$time_query = "`created`>'".date("Y-m-d H:i:s", strtotime("-3 month"))."'";
		}

		$chart_data2 = $coreSQL->queryData("SELECT ".$group_by." AS `date`, COUNT(*) as `value` FROM `".$table."` "
				. "WHERE $time_query $query $query2 "
				. "GROUP BY ".$group_by." ORDER BY `created` ASC");

		$total = 0; $counter = 0;
		foreach ($chart_data2 as $item) { $total += (int)$item['value']; $counter++; }
		
		$chart_data2_full = array();
		$counter1 = 0;
		foreach ($chart_data as $item) {
			$counter1++;
			$value = array('date' => $item['date'], 'value' => 0);
			foreach ($chart_data2 as $item2) {
				if ($item['date'] == $item2['date']) {
					$value = $item2;
					break;
				}
			}
			$chart_data2_full []= $value;
		}
		
		$smarty->assign('chart'.$chart_id.'_data2', json_encode($chart_data2_full));
		$smarty->assign('chart'.$chart_id.'_total2', $total);
		if ($total > 0) $smarty->assign('chart'.$chart_id.'_average2', round($total/$counter1, 1));
		if ($total > 0) $smarty->assign('chart'.$chart_id.'_monthly2', ($period == "weekly"?round($total/12, 1):round($total/3, 1)) );
	}
	
	if ($pages->path[1] == "weekly") {
		$period = "weekly";
	}
	else {
		$period = "daily";
	}
	
	assignChartData("New users", "/stats/monthly_users", 1, "users", $period, "");
	assignChartData("Online", "/stats/monthly_users", 2, "daily_stats", $period, "", "date", "SUM(`login_counts`)");
	
	assignChartData("Active users (level 5)", "/stats/monthly_users", 3, "users", $period, " AND `level`=5");
	assignChartData("Users +5000 keywords", "/stats/monthly_users", 4, "users", $period, " AND `keywords_count`>5000");
	
	assignChartData("Keywords", "", 5, "keywords", $period, "");
	assignChartData2("Adclicks", "/adclicks", 6, "adclicks", $period, " AND `creative`!='' AND `location`!='' AND `http_referer`!=''", " AND `user_ip` IN (SELECT DISTINCT `user_ip` FROM `users`)");
	
	assignChartData2("SEO clicks", "/refclicks/seo", 7, "refclicks", $period, " AND (`http_referer` LIKE '%www.google.%' OR `http_referer` LIKE '%www.bing.com%' OR `http_referer` LIKE '%search.yahoo.com%')", " AND `user_ip` IN (SELECT DISTINCT `user_ip` FROM `users`)");
	
	assignChartData2("Ref & Social clicks", "/refclicks/social", 8, "refclicks", $period, " AND NOT (`http_referer` LIKE '%www.google.%' OR `http_referer` LIKE '%www.bing.com%' OR `http_referer` LIKE '%search.yahoo.com%' OR "
			. "`http_referer` LIKE '%www.baidu.com%' OR `http_referer` LIKE '%yandex.com%' OR `http_referer` LIKE '%yandex.ru%' OR "
			. "`http_referer` LIKE '%ads.google.com%' OR `http_referer` LIKE '%mail.google.com%' OR `http_referer` LIKE '%com.google.android.gm%')", " AND `user_ip` IN (SELECT DISTINCT `user_ip` FROM `users`)");
	
	assignChartData("Domains with keywords", "", 9, "domains", $period, " AND `keywords_count`>0");
	assignChartData("Domains linked GA", "", 10, "domains", $period, " AND `analytics_linked`=1");
	
	assignChartData("Payments", "/stats/coming_payments", 11, "accounting_payments", $period, "", "billing_date");
	assignChartData("Payments amount", "/stats/monthly_income", 12, "accounting_payments", $period, "", "billing_date", "SUM(`billing_amount`)");
	
	$smarty->assign('period', $period);
	$smarty->assign('period_metric', ($period == "weekly"?"week":"day"));
	
	// todo active users chart

}

?>