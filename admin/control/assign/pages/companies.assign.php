<?php

if ($users->isLogged) {
	
	$smarty->assign('company_types', $companies->types);
	$smarty->assign('sectors', $companies->sectors);
	$smarty->assign('products', $companies->products);
	
	$companies->list_table['rows'] = $companies->searchQuery($_POST);
	
}
else {
	redirect('');
}

?>