<?php

if ($users->isLogged) {
	
	function assignChartData($title, $type, $chart_id, $table, $period, $query) {
		global $coreSQL, $smarty;
		
		if ($period == "weekly") {
			$group_by = "YEARWEEK(`created`)";
			$time_query = "`created`>'".date("Y-m-d H:i:s", strtotime("-12 month"))."'";
		}
		else {
			$group_by = "DATE_FORMAT(`created`, '%m-%d')";
			$time_query = "`created`>'".date("Y-m-d H:i:s", strtotime("-3 month"))."'";
		}

		$chart_data = $coreSQL->queryData("SELECT ".$group_by." AS `date`, COUNT(*) as `value` FROM `".$table."` "
				. "WHERE $time_query $query "
				. "GROUP BY ".$group_by." ORDER BY `created` ASC");

		$total = 0; $counter = 0;
		foreach ($chart_data as $item) { $total += (int)$item['value']; $counter++; }
		
		$smarty->assign('chart'.$chart_id.'_data', json_encode($chart_data));
		$smarty->assign('chart'.$chart_id.'_total', $total);
		if ($total > 0) $smarty->assign('chart'.$chart_id.'_average', round($total/$counter, 1));
		if ($total > 0) $smarty->assign('chart'.$chart_id.'_monthly', ($period == "weekly"?round($total/12, 1):round($total/3, 1)) );
		$smarty->assign('chart'.$chart_id.'_title', $title);
		$smarty->assign('chart'.$chart_id.'_type', $type);
		
		return $chart_data;
	}
	
	function assignChartData2($title, $type, $chart_id, $table, $period, $query, $query2) {
		global $coreSQL, $smarty;
		
		$chart_data = assignChartData($title, $type, $chart_id, $table, $period, $query);
		
		if ($period == "weekly") {
			$group_by = "YEARWEEK(`created`)";
			$time_query = "`created`>'".date("Y-m-d H:i:s", strtotime("-12 month"))."'";
		}
		else {
			$group_by = "DATE_FORMAT(`created`, '%m-%d')";
			$time_query = "`created`>'".date("Y-m-d H:i:s", strtotime("-3 month"))."'";
		}

		$chart_data2 = $coreSQL->queryData("SELECT ".$group_by." AS `date`, COUNT(*) as `value` FROM `".$table."` "
				. "WHERE $time_query $query $query2 "
				. "GROUP BY ".$group_by." ORDER BY `created` ASC");

		$total = 0; $counter = 0;
		foreach ($chart_data2 as $item) { $total += (int)$item['value']; $counter++; }
		
		$chart_data2_full = array();
		$counter1 = 0;
		foreach ($chart_data as $item) {
			$counter1++;
			$value = array('date' => $item['date'], 'value' => 0);
			foreach ($chart_data2 as $item2) {
				if ($item['date'] == $item2['date']) {
					$value = $item2;
					break;
				}
			}
			$chart_data2_full []= $value;
		}
		
		$smarty->assign('chart'.$chart_id.'_data2', json_encode($chart_data2_full));
		$smarty->assign('chart'.$chart_id.'_total2', $total);
		if ($total > 0) $smarty->assign('chart'.$chart_id.'_average2', round($total/$counter1, 1));
		if ($total > 0) $smarty->assign('chart'.$chart_id.'_monthly2', ($period == "weekly"?round($total/12, 1):round($total/3, 1)) );
	}
	
	if ($pages->path[1] == "emails") {
		
		if ($pages->path[2] == "weekly") {
			$period = "weekly";
		}
		else {
			$period = "daily";
		}
		
		assignChartData2("Emails", "", 1, "mailer_emails", $period, " AND `to`!='reports@seopoz.com'", " AND `opened`=1");
		
		assignChartData("Email clicks", "", 2, "mailer_emails", $period, " AND `clicked`=1 AND `to`!='reports@seopoz.com'");
		
		assignChartData2("Ranking report emails", "Keyword Ranking Changes", 3, "mailer_emails", $period, " AND `to`!='reports@seopoz.com' AND (`type`='Keywords Ranking' OR `type`='Keywords Ranking Report' OR `type`='Keyword Ranking Changes')", " AND `opened`=1");
		
		assignChartData2("SEO traffic emails", "Weekly Traffic Report", 4, "mailer_emails", $period, " AND `to`!='reports@seopoz.com' AND (`type`='Notifications' OR `type`='Weekly Traffic Report')", " AND `opened`=1");
		
		assignChartData2("No domains emails", "No Domains", 5, "mailer_emails", $period, " AND `to`!='reports@seopoz.com' AND `type`='No Domains'", " AND `opened`=1");
		
		assignChartData2("Connect GWT emails", "Connect GWT", 6, "mailer_emails", $period, " AND `to`!='reports@seopoz.com' AND `type`='Connect GWT'", " AND `opened`=1");
		
		assignChartData2("One click login emails", "One Click Login", 7, "mailer_emails", $period, " AND `to`!='reports@seopoz.com' AND `type`='One Click Login'", " AND `opened`=1");
		
		assignChartData2("Trial expired (reached limit) emails", "Reached Limit", 8, "mailer_emails", $period, " AND `to`!='reports@seopoz.com' AND (`type`='Trial Expired' OR `type`='Reached Limit')", " AND `opened`=1");
		
		$smarty->assign('period', $period);
		$smarty->assign('period_metric', ($period == "weekly"?"week":"day"));
	}
	
	$paused_keywords = array(
		"seo", "search engine optimization", "seo serp", "seo analyzer",
		"serp", "serp google", "serps",
		"rank", "website rank tool",
		"seo tool", "seo tools",
		"seo checker", "seo rank checker free",
		"keyword tool", "google keyword tool", "seo keywords tool", "keyword suggestion tool", "keyword search volume tool",
		"keyword position",
		"google ranking", "google rankings", "google rank website", "google search rank",
		"website ranking",  "website ranking tools", "web site ranking tool", 
		"search rankings", "search engine ranking",
		"overture keyword tool", "keyword density tracker", 
		
		"key word rank", "free seo software", "free rank checker tool", "seo checker tool",
		"keyword tracker tool google", "free seo keyword tools", "seo checker free", "free seo rank checker",
		"free seo keyword tool", "search engine keyword tool", "rank keywords", "seo reporting",
		"google keyword tracker", "keyword google ranking", "serp site", "seo ranking tool", "seo keyword tool",
		"free ranking checker", "keywords rank checker tool", "keyword checker",
		"keyword rank checker", "website search engine ranking", "serp tool", "keyword research tool online",
		"keyword tool online", "seo checker online", "website ranking checker free", "website rank checker free",
		"online keyword research tool", "search engine rankings", "seo ranking tools", "check seo rank",
		"seo keyword ranking tool", "seo optimization tools free", "serp ranking", "keyword tracking tool",
		"google rank position", "rank checker seo", "webceo", "search ranking tool",
		"check website ranking", "seo for website free", "free rank checker online", "ranking keyword",
		"site rank checker", "site ranking checker", "serp online", "keyword tracker google",
		"keyword spy", "free online rank checker",
		"seo rank", "seo tools keywords", "keywords checker", "keywords rank", "seo optimization software",
		"check seo ranking", "keyword analyzer tool", "search ranking checker", "ranking checker",
		"website ranking checker", "seo ranking checker", "best seo software",
		"serp tracker", "best serp tracker", "free serp tracker", "serp tracker free", "website ranking tool free",
		"rank tracker free", "google keyword tracker free", "seo keyword check", "online serp tool", "serp track",
		"top seo software", "site ranking tool", "seo track", "position checker", "instant seo", "google ranking tool",
		"whitelabel seo", "page rank tracker", "php keyword tracking", "keywords spy", "google rank checker free",
		"keyword ranking tool google", "seo rank checker", "keyword rank checker free",
		"google keyword ranking tool", "keyword monitoring",
		"website seo software", "site rankings checker", "seo website software",
		"web page ranking tool", "free keyword ranking checker", "ranktracker", "google rank checker tool",
		"seo keyword checker",
	);
	
	$paused_countries = array(
		/*"LT", "EE", "PL",
		"RO",
		"UA",
		"GB", "FR",
		"ES", "PT",
		"SE", "FI",
		"DE", "AT", "CH", 
		"TW", "JP", 
		"VN", "PH", "TH", "KH", "LA",
		"PK", "BD", "NP", 
		"IQ",
		"DO",
		"NZ",*/
	);
	
	if ($pages->path[1] == "users_by_timezone") {
		$keywords_stats = array();
		$users_stats = array();

		$users_info = $coreSQL->queryData("SELECT * FROM `users` WHERE 1");
		foreach ($users_info as $user_data) {

			if ($user_data['timezone'] && $user_data['timezone_diff']) {
				$time_diff = explode(':', $user_data['timezone_diff']);
				if ($time_diff[0] == '+') {
					$hour = 8 - (int)$time_diff[1];
				}
				else {
					$hour = 8 + (int)$time_diff[1];
				}
			}
			else {
				$hour = 10;
			}


			if (!isset($keywords_stats[$hour]['users_count'])) {$keywords_stats[$hour]['users_count'] = 0;}
			if (!isset($keywords_stats[$hour]['keywords_count'])) {$keywords_stats[$hour]['keywords_count'] = 0;}
			if (!isset($keywords_stats[$hour]['domains_count'])) {$keywords_stats[$hour]['domains_count'] = 0;}

			if (!isset($keywords_stats[$hour]['users_free_count'])) {$keywords_stats[$hour]['users_free_count'] = 0;}
			if (!isset($keywords_stats[$hour]['keywords_free_count'])) {$keywords_stats[$hour]['keywords_free_count'] = 0;}
			if (!isset($keywords_stats[$hour]['domains_free_count'])) {$keywords_stats[$hour]['domains_free_count'] = 0;}

			if (!isset($keywords_stats[$hour]['users_trial_end_count'])) {$keywords_stats[$hour]['users_trial_end_count'] = 0;}
			if (!isset($keywords_stats[$hour]['keywords_trial_end_count'])) {$keywords_stats[$hour]['keywords_trial_end_count'] = 0;}
			if (!isset($keywords_stats[$hour]['domains_trial_end_count'])) {$keywords_stats[$hour]['domains_trial_end_count'] = 0;}

			$user_domains_count = $domains->countUser($user_data['id']);
			$user_keywords_count = $keywords->countUser($user_data['id']);

			$keywords_stats[$hour]['users_count'] += 1;
			$keywords_stats[$hour]['domains_count'] += $user_domains_count;
			$keywords_stats[$hour]['keywords_count'] += $user_keywords_count;
			//$keywords_stats[$hour]['users'][] = $user_data['email'];

			if ($user_data['plan'] == "") {
				$keywords_stats[$hour]['users_free_count'] += 1;
				$keywords_stats[$hour]['domains_free_count'] += $user_domains_count;
				$keywords_stats[$hour]['keywords_free_count'] += $user_keywords_count;

				if ($user_data['trial_expired']) {
					$keywords_stats[$hour]['users_trial_end_count'] += 1;
					$keywords_stats[$hour]['domains_trial_end_count'] += $user_domains_count;
					$keywords_stats[$hour]['keywords_trial_end_count'] += $user_keywords_count;
				}
				else {
					$keywords_stats[$hour]['free_users'][$user_data['id']] = $user_data['id'];
				}
			}
			else {
				$keywords_stats[$hour]['premium_users'][$user_data['id']] = $user_data['id'];
				if (!isset($users_stats['plan'][$user_data['plan']])) {
					$users_stats['plan'][$user_data['plan']] = 1;
				} else {
					$users_stats['plan'][$user_data['plan']]++;
				}
			}

		}

		ksort($keywords_stats);

		foreach ($keywords_stats as $hour => $stats) {
			if ($stats['premium_users']) {
				$keywords_stats[$hour]['premium_users_ids'] = implode(",", $stats['premium_users']);
			}
			if ($stats['free_users']) {
				$keywords_stats[$hour]['free_users_ids'] = implode(",", $stats['free_users']);
			}
		}

		$smarty->assign('keywords_stats', $keywords_stats);
		$smarty->assign('users_stats', $users_stats);
	}
	
	if ($pages->path[1] == "coming_payments") {
		
		$canceled = $coreSQL->queryColumn("SELECT DISTINCT `payer_email` FROM `paypal` WHERE `txn_type`='subscr_cancel' OR `txn_type`='subscr_eot'", "payer_email");
		$smarty->assign('canceled', $canceled);
		
		$stats = $coreSQL->queryData(
			"SELECT `accounting_payments`.`user_id`, `users`.`first_name`, `users`.`email`, `users`.`billing_email`, "
				. "`users`.`plan`, `users`.`plan_start`, `users`.`plan_end`, `users`.`payments`, `users`.`location`, "
			. "SUM(`accounting_payments`.`billing_amount`) AS `total`, `accounting_payments`.`product_id`, `accounting_payments`.`billing_processor` "
			. "FROM `users`, `accounting_payments` "
			. "WHERE `users`.`id`=`accounting_payments`.`user_id` AND `users`.`plan`!='' "
			. "GROUP BY `accounting_payments`.`user_id`");
		
		$smarty->assign('products', $coreSQL->queryData("SELECT * FROM `accounting_products` WHERE 1", "id"));
	}
	
	if ($pages->path[1] == "monthly_revenue") {
		$stats = $coreSQL->queryData(
			"SELECT MONTH(`billing_date`) AS `month`, SUM(`billing_total`) AS `amount`, COUNT(`billing_total`) AS `payments` "
			. "FROM `accounting_payments` "
			. "WHERE YEAR(`billing_date`)=2020 GROUP BY MONTH(`billing_date`) ORDER BY `billing_date` ASC");
		
		$stats2 = $coreSQL->queryData(
			"SELECT MONTH(`billing_date`) AS `month`, SUM(`billing_total`) AS `amount`, COUNT(`billing_total`) AS `payments` "
			. "FROM `accounting_payments` "
			. "WHERE YEAR(`billing_date`)=2019 GROUP BY MONTH(`billing_date`) ORDER BY `billing_date` ASC");
		
		$stats3 = $coreSQL->queryData(
			"SELECT MONTH(`billing_date`) AS `month`, SUM(`billing_total`) AS `amount`, COUNT(`billing_total`) AS `payments` "
			. "FROM `accounting_payments` "
			. "WHERE YEAR(`billing_date`)=2018 GROUP BY MONTH(`billing_date`) ORDER BY `billing_date` ASC");
	}
	
	if ($pages->path[1] == "monthly_users") {
		$stats = $coreSQL->queryData(
			"SELECT MONTH(`created`) AS `month`, COUNT(MONTH(`created`)) AS `count` FROM `users`"
			. "WHERE YEAR(`created`)=2020 GROUP BY MONTH(`created`) ORDER BY MONTH(`created`) ASC", "month");
		$subscriptions_stats = $coreSQL->queryData(
			"SELECT MONTH(`created`) AS `month`, COUNT(MONTH(`created`)) AS `count` FROM `users`"
			. "WHERE YEAR(`created`)=2020 AND `payments`!=0 GROUP BY MONTH(`created`) ORDER BY MONTH(`created`) ASC");
		$payments_stats = $coreSQL->queryData(
			"SELECT `users`.`id`, MONTH(`users`.`created`) AS `month`, `accounting_payments`.`billing_total` FROM `users`, `accounting_payments` "
			. "WHERE `users`.`id`=`accounting_payments`.`user_id` AND YEAR(`users`.`created`)=2020 AND `users`.`payments`!=0 GROUP BY `users`.`id` "
			. "ORDER BY `accounting_payments`.`billing_date` ASC");
		
		foreach ($subscriptions_stats as $stat) {
			$stats[$stat['month']]['subscriptions'] = $stat['count'];
		}
		foreach ($payments_stats as $stat) {
			if (isset($stats[$stat['month']]['payments'])) {
				$stats[$stat['month']]['payments'] += $stat['billing_total'];
			}
			else {
				$stats[$stat['month']]['payments'] = $stat['billing_total'];
			}
		}
		
		$stats2 = $coreSQL->queryData(
			"SELECT MONTH(`created`) AS `month`, COUNT(MONTH(`created`)) AS `count` FROM `users`"
			. "WHERE YEAR(`created`)=2019 GROUP BY MONTH(`created`) ORDER BY MONTH(`created`) ASC", "month");
		$subscriptions_stats2 = $coreSQL->queryData(
			"SELECT MONTH(`created`) AS `month`, COUNT(MONTH(`created`)) AS `count` FROM `users`"
			. "WHERE YEAR(`created`)=2019 AND `payments`!=0 GROUP BY MONTH(`created`) ORDER BY MONTH(`created`) ASC");
		$payments_stats2 = $coreSQL->queryData(
			"SELECT `users`.`id`, MONTH(`users`.`created`) AS `month`, `accounting_payments`.`billing_total` FROM `users`, `accounting_payments` "
			. "WHERE `users`.`id`=`accounting_payments`.`user_id` AND YEAR(`users`.`created`)=2019 AND `users`.`payments`!=0 GROUP BY `users`.`id` "
			. "ORDER BY `accounting_payments`.`billing_date` ASC");
		
		foreach ($subscriptions_stats2 as $stat) {
			$stats2[$stat['month']]['subscriptions'] = $stat['count'];
		}
		foreach ($payments_stats2 as $stat) {
			if (isset($stats2[$stat['month']]['payments'])) {
				$stats2[$stat['month']]['payments'] += $stat['billing_total'];
			}
			else {
				$stats2[$stat['month']]['payments'] = $stat['billing_total'];
			}
		}
	}
	
	if ($pages->path[1] == "clicks_by_keywords") {
		for ($i = 9; $i >= 3; $i--) {
			$stats[$i] = $coreSQL->queryData(
				"SELECT `keyword`, COUNT(`keyword`) AS `clicks` FROM `adclicks` "
				. "WHERE `location`!='' AND `http_referer`!='' AND YEAR(`created`)=2017 AND MONTH(`created`)=".(int)$i." GROUP BY `keyword` ORDER BY COUNT(`keyword`) DESC", "keyword");
			
			$signups_stats = $coreSQL->queryData(
				"SELECT `adwords_keyword`, COUNT(`adwords_keyword`) AS `count` FROM `users` "
				. "WHERE `adwords_keyword` != '' AND YEAR(`created`)=2017 AND MONTH(`created`)=".(int)$i." GROUP BY `adwords_keyword`");
			
			$subscriptions_stats = $coreSQL->queryData(
				"SELECT `adwords_keyword`, COUNT(`adwords_keyword`) AS `count` FROM `users` "
				. "WHERE `adwords_keyword` != '' AND `payments`!=0 AND YEAR(`created`)=2017 AND MONTH(`created`)=".(int)$i." GROUP BY `adwords_keyword`");
			
			foreach ($signups_stats as $stat) {
				$stats[$i][$stat['adwords_keyword']]['keyword'] = $stat['adwords_keyword'];
				$stats[$i][$stat['adwords_keyword']]['signups'] = $stat['count'];
			}
			foreach ($subscriptions_stats as $stat) {
				$stats[$i][$stat['adwords_keyword']]['keyword'] = $stat['adwords_keyword'];
				$stats[$i][$stat['adwords_keyword']]['subscriptions'] = $stat['count'];
			}
		}
	}
	
	if ($pages->path[1] == "clicks_by_keywords_campaign") {
		$creatives_data = $creatives->getCreativesByCampaign();
		foreach ($creatives_data as $campaign => $creative_array) {
			$stats[$campaign] = $coreSQL->queryData(
				"SELECT `keyword`, COUNT(`keyword`) AS `clicks` FROM `adclicks` "
				. "WHERE `location`!='' AND `http_referer`!='' AND `creative` IN ('".implode("','", $creative_array)."') AND YEAR(`created`)=2017 AND MONTH(`created`) IN (6) GROUP BY `keyword` ORDER BY COUNT(`keyword`) DESC", "keyword");

			$signups_stats = $coreSQL->queryData(
				"SELECT `adwords_keyword`, COUNT(`adwords_keyword`) AS `count` FROM `users` "
				. "WHERE `adwords_keyword` != '' AND `adwords_creative` IN ('".implode("','", $creative_array)."') AND YEAR(`created`)=2017 AND MONTH(`created`) IN (6) GROUP BY `adwords_keyword`");

			$subscriptions_stats = $coreSQL->queryData(
				"SELECT `adwords_keyword`, COUNT(`adwords_keyword`) AS `count` FROM `users` "
				. "WHERE `adwords_keyword` != '' AND `adwords_creative` IN ('".implode("','", $creative_array)."') AND `payments`!=0 AND YEAR(`created`)=2017 AND MONTH(`created`) IN (6) GROUP BY `adwords_keyword`");

			foreach ($signups_stats as $stat) {
				$stats[$campaign][$stat['adwords_keyword']]['keyword'] = $stat['adwords_keyword'];
				$stats[$campaign][$stat['adwords_keyword']]['signups'] = $stat['count'];
			}
			foreach ($subscriptions_stats as $stat) {
				$stats[$campaign][$stat['adwords_keyword']]['keyword'] = $stat['adwords_keyword'];
				$stats[$campaign][$stat['adwords_keyword']]['subscriptions'] = $stat['count'];
			}
		}
	}
	
	if ($pages->path[1] == "clicks_by_keywords_total") {
		$stats = $coreSQL->queryData(
			"SELECT `keyword`, COUNT(`keyword`) AS `clicks` FROM `adclicks` "
			. "WHERE `location`!='' AND `http_referer`!='' AND YEAR(`created`) IN (2015,2016,2017) GROUP BY `keyword` ORDER BY COUNT(`keyword`) DESC", "keyword");

		$signups_stats = $coreSQL->queryData(
			"SELECT `adwords_keyword`, COUNT(`adwords_keyword`) AS `count` FROM `users` "
			. "WHERE `adwords_keyword` != '' AND YEAR(`created`) IN (2015,2016,2017) GROUP BY `adwords_keyword`");

		$subscriptions_stats = $coreSQL->queryData(
			"SELECT `adwords_keyword`, COUNT(`adwords_keyword`) AS `count` FROM `users` "
			. "WHERE `adwords_keyword` != '' AND `payments`!=0 AND YEAR(`created`) IN (2015,2016,2017) GROUP BY `adwords_keyword`");

		foreach ($signups_stats as $stat) {
			$stats[$stat['adwords_keyword']]['keyword'] = $stat['adwords_keyword'];
			$stats[$stat['adwords_keyword']]['signups'] = $stat['count'];
		}
		foreach ($subscriptions_stats as $stat) {
			$stats[$stat['adwords_keyword']]['keyword'] = $stat['adwords_keyword'];
			$stats[$stat['adwords_keyword']]['subscriptions'] = $stat['count'];
		}
	}
	
	if ($pages->path[1] == "clicks_by_keywords_year") {
		$stats = $coreSQL->queryData(
			"SELECT `keyword`, COUNT(`keyword`) AS `clicks` FROM `adclicks` "
			. "WHERE `location`!='' AND `http_referer`!='' AND YEAR(`created`) IN (2017) GROUP BY `keyword` ORDER BY COUNT(`keyword`) DESC", "keyword");

		$signups_stats = $coreSQL->queryData(
			"SELECT `adwords_keyword`, COUNT(`adwords_keyword`) AS `count` FROM `users` "
			. "WHERE `adwords_keyword` != '' AND YEAR(`created`) IN (2017) GROUP BY `adwords_keyword`");

		$subscriptions_stats = $coreSQL->queryData(
			"SELECT `adwords_keyword`, COUNT(`adwords_keyword`) AS `count` FROM `users` "
			. "WHERE `adwords_keyword` != '' AND `payments`!=0 AND YEAR(`created`) IN (2017) GROUP BY `adwords_keyword`");

		foreach ($signups_stats as $stat) {
			$stats[$stat['adwords_keyword']]['keyword'] = $stat['adwords_keyword'];
			$stats[$stat['adwords_keyword']]['signups'] = $stat['count'];
		}
		foreach ($subscriptions_stats as $stat) {
			$stats[$stat['adwords_keyword']]['keyword'] = $stat['adwords_keyword'];
			$stats[$stat['adwords_keyword']]['subscriptions'] = $stat['count'];
		}
	}
	
	if ($pages->path[1] == "clicks_by_location") {
		for ($i = 9; $i >= 3; $i--) {
			$stats[$i] = $coreSQL->queryData(
				"SELECT SUBSTRING(`location`, -2) AS `location`, COUNT(*) AS `clicks` FROM `adclicks` "
				. "WHERE `location`!='' AND `http_referer`!='' AND YEAR(`created`)=2017 AND MONTH(`created`)=".(int)$i." GROUP BY SUBSTRING(`location`, -2) ORDER BY COUNT(*) DESC", "location");
			
			$signups_stats = $coreSQL->queryData(
				"SELECT SUBSTRING(`location`, -2) AS `loc`, COUNT(SUBSTRING(`location`, -2)) AS `count` FROM `users` "
				. "WHERE `adwords_keyword` != '' AND YEAR(`created`)=2017 AND MONTH(`created`)=".(int)$i." GROUP BY SUBSTRING(`location`, -2)");
			
			$subscriptions_stats = $coreSQL->queryData(
				"SELECT SUBSTRING(`location`, -2) AS `loc`, COUNT(SUBSTRING(`location`, -2)) AS `count` FROM `users` "
				. "WHERE `adwords_keyword` != '' AND `payments`!=0 AND YEAR(`created`)=2017 AND MONTH(`created`)=".(int)$i." GROUP BY SUBSTRING(`location`, -2)");
			
			foreach ($signups_stats as $stat) {
				$stats[$i][$stat['loc']]['signups'] = $stat['count'];
			}
			foreach ($subscriptions_stats as $stat) {
				$stats[$i][$stat['loc']]['subscriptions'] = $stat['count'];
			}
		}
	}
	
	if ($pages->path[1] == "clicks_by_location_total") {
		$stats = $coreSQL->queryData(
			"SELECT SUBSTRING(`location`, -2) AS `location`, COUNT(*) AS `clicks` FROM `adclicks` "
			. "WHERE `location`!='' AND `http_referer`!='' AND YEAR(`created`) IN (2015,2016,2017) GROUP BY SUBSTRING(`location`, -2) ORDER BY COUNT(*) DESC", "location");

		$signups_stats = $coreSQL->queryData(
			"SELECT SUBSTRING(`location`, -2) AS `loc`, COUNT(SUBSTRING(`location`, -2)) AS `count` FROM `users` "
			. "WHERE `adwords_keyword` != '' AND YEAR(`created`) IN (2015,2016,2017) GROUP BY SUBSTRING(`location`, -2)");

		$subscriptions_stats = $coreSQL->queryData(
			"SELECT SUBSTRING(`location`, -2) AS `loc`, COUNT(SUBSTRING(`location`, -2)) AS `count` FROM `users` "
			. "WHERE `adwords_keyword` != '' AND `payments`!=0 AND YEAR(`created`) IN (2015,2016,2017) GROUP BY SUBSTRING(`location`, -2)");

		foreach ($signups_stats as $stat) {
			$stats[$stat['loc']]['signups'] = $stat['count'];
		}
		foreach ($subscriptions_stats as $stat) {
			$stats[$stat['loc']]['subscriptions'] = $stat['count'];
		}
	}
	
	if ($pages->path[1] == "clicks_by_location_year") {
		$stats = $coreSQL->queryData(
			"SELECT SUBSTRING(`location`, -2) AS `location`, COUNT(*) AS `clicks` FROM `adclicks` "
			. "WHERE `location`!='' AND `http_referer`!='' AND YEAR(`created`) IN (2017) GROUP BY SUBSTRING(`location`, -2) ORDER BY COUNT(*) DESC", "location");

		$signups_stats = $coreSQL->queryData(
			"SELECT SUBSTRING(`location`, -2) AS `loc`, COUNT(SUBSTRING(`location`, -2)) AS `count` FROM `users` "
			. "WHERE `adwords_keyword` != '' AND YEAR(`created`) IN (2017) GROUP BY SUBSTRING(`location`, -2)");

		$subscriptions_stats = $coreSQL->queryData(
			"SELECT SUBSTRING(`location`, -2) AS `loc`, COUNT(SUBSTRING(`location`, -2)) AS `count` FROM `users` "
			. "WHERE `adwords_keyword` != '' AND `payments`!=0 AND YEAR(`created`) IN (2017) GROUP BY SUBSTRING(`location`, -2)");

		foreach ($signups_stats as $stat) {
			$stats[$stat['loc']]['signups'] = $stat['count'];
		}
		foreach ($subscriptions_stats as $stat) {
			$stats[$stat['loc']]['subscriptions'] = $stat['count'];
		}
	}
	
	if ($pages->path[1] == "keywords_with_errors") {
		$stats = $coreSQL->queryData("SELECT * FROM `keywords` WHERE `errors`!=0 ORDER BY `errors` DESC");
	}
	
	$smarty->assign('stats', $stats);
	$smarty->assign('stats2', $stats2);
	$smarty->assign('stats3', $stats3);
	$smarty->assign('paused_keywords', $paused_keywords);
	$smarty->assign('paused_countries', $paused_countries);
}
else {
	redirect('');
}

