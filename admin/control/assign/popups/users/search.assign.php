<?php

if ($users->isLogged) {
	
	$plans = array(
		'standard' => "Standard",
		'premium' => "Premium",
	);

	$smarty->assign('plans', $plans);
	
	$_POST = $coreSession->session['page_info'][$pages->url]['filter'];
}
else {
	$create_popup = false;
}

?>