<?php

if ($users->isLogged) {

	$users_info = $coreSQL->queryData("SELECT * FROM `users` WHERE `id` IN (".addslashes($form_data['ids']).")", "id");
	
	foreach ($users_info as $user_id => $user_info) {
		$users_info[$user_id]['keywords'] = $coreControllers['keywords']->getByUser($user_info['id']);
		$users_info[$user_id]['keywords_scanned'] = $coreControllers['keywords']->keywordsScanned($user_info['id']);
		$users_info[$user_id]['scan_servers'] = $coreControllers['keywords']->getScanServers($user_info['id']);
	}
	
	$smarty->assign('users', $users_info);
	
}
else {
	$create_popup = false;
}

?>