<?php

if ($users->isLogged) {
	$_POST = $users->getById($form_data);
	$_POST['password'] = $_POST['password2'];

	$notifications = array(
		'0' => "Do not send notifications",
		'1' => "Send daily notifications",
		'2' => "Send weekly notifications",
		'3' => "Send monthly notifications",
	);

	$smarty->assign('notifications', $notifications);

	// data for createPopupCallback
	$form_data = array();
	$form_data['timezone'] = $_POST['timezone'];
}
else {
	$create_popup = false;
}

?>