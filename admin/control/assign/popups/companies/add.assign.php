<?php

if ($users->isLogged) {

	$smarty->assign('company_types', $companies->types);
	$smarty->assign('sectors', $companies->sectors);
	$smarty->assign('products', $companies->products);
	$smarty->assign('ratings', $companies->ratings);
	
}
else {
	$create_popup = false;
}

?>