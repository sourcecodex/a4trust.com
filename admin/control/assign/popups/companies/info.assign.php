<?php

if ($users->isLogged) {

	$smarty->assign('company_types', $companies->types);
	
	$company = $companies->getById($form_data);
	
	$smarty->assign('company', $company);

}
else {
	$create_popup = false;
}

?>