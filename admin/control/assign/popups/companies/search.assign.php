<?php

if ($users->isLogged) {
	
	$smarty->assign('company_types', $companies->types);
	$smarty->assign('sectors', $companies->sectors);
	$smarty->assign('products', $companies->products);
	
	$_POST = $coreSession->session['page_info'][$pages->url]['filter'];
	
}
else {
	$create_popup = false;
}

?>