function drawChart(id) {
	var chart_data = JSON.parse($('#chart' + id + '_data').html());

	var labels = new Array();
	var values = new Array();

	for (var i in chart_data) {
		labels.push(chart_data[i].date);
		values.push(parseInt(chart_data[i].value));
	}

	var data = {
		labels: labels,
		datasets: [
			{
				label: "Chart",
				fill: true,
				lineTension: 0.1,
				backgroundColor: "rgba(75,192,192,0.4)",
				borderColor: "rgba(75,192,192,1)",
				borderCapStyle: 'butt',
				borderDash: [],
				borderDashOffset: 0.0,
				borderJoinStyle: 'miter',
				pointBorderColor: "rgba(75,192,192,1)",
				pointBackgroundColor: "#fff",
				pointBorderWidth: 1,
				pointHoverRadius: 5,
				pointHoverBackgroundColor: "rgba(75,192,192,1)",
				pointHoverBorderColor: "rgba(220,220,220,1)",
				pointHoverBorderWidth: 2,
				pointRadius: 1,
				pointHitRadius: 30,
				data: values,
				spanGaps: false
			}
		]
	};

	var options = {
		legend: {
			display: false
		},
		scales: {
			yAxes: [{
				ticks: {
					beginAtZero: true
				}
			}]
		}
	};

	var ctx = document.getElementById("chart" + id);
	var myChart = new Chart(ctx, {
		type: 'line',
		data: data,
		options: options
	});
}

function drawChart2(id) {
	var chart_data = JSON.parse($('#chart' + id + '_data').html());
	var chart_data2 = JSON.parse($('#chart' + id + '_data2').html());
	
	var labels = new Array();
	var values = new Array();

	for (var i in chart_data) {
		labels.push(chart_data[i].date);
		values.push(parseInt(chart_data[i].value));
	}
	
	var values2 = new Array();

	for (var i in chart_data2) {
		values2.push(parseInt(chart_data2[i].value));
	}

	var data = {
		labels: labels,
		datasets: [
			{
				label: "Chart",
				fill: true,
				lineTension: 0.1,
				backgroundColor: "rgba(75,192,192,0.4)",
				borderColor: "rgba(75,192,192,1)",
				borderCapStyle: 'butt',
				borderDash: [],
				borderDashOffset: 0.0,
				borderJoinStyle: 'miter',
				pointBorderColor: "rgba(75,192,192,1)",
				pointBackgroundColor: "#fff",
				pointBorderWidth: 1,
				pointHoverRadius: 5,
				pointHoverBackgroundColor: "rgba(75,192,192,1)",
				pointHoverBorderColor: "rgba(220,220,220,1)",
				pointHoverBorderWidth: 2,
				pointRadius: 1,
				pointHitRadius: 30,
				data: values,
				spanGaps: false
			},
			{
				label: "Chart2",
				fill: true,
				lineTension: 0.1,
				backgroundColor: "rgba(75,92,192,0.4)",
				borderColor: "rgba(75,92,192,1)",
				borderCapStyle: 'butt',
				borderDash: [],
				borderDashOffset: 0.0,
				borderJoinStyle: 'miter',
				pointBorderColor: "rgba(75,92,192,1)",
				pointBackgroundColor: "#fff",
				pointBorderWidth: 1,
				pointHoverRadius: 5,
				pointHoverBackgroundColor: "rgba(75,92,192,1)",
				pointHoverBorderColor: "rgba(220,120,220,1)",
				pointHoverBorderWidth: 2,
				pointRadius: 1,
				pointHitRadius: 30,
				data: values2,
				spanGaps: false
			}
		]
	};

	var options = {
		legend: {
			display: false
		},
		scales: {
			yAxes: [{
				ticks: {
					beginAtZero: true
				}
			}]
		}
	};

	var ctx = document.getElementById("chart" + id);
	var myChart = new Chart(ctx, {
		type: 'line',
		data: data,
		options: options
	});
}