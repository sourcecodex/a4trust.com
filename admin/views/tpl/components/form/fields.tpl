{if $item.type == "timezone"}
	{include file='includes/timezones.tpl'}
{elseif $item.type == "related_companies"}
	<label class="control-label" for="{$field}">{$item.title}</label><br/>
	
	<div id="{$field}_selected" class="mb-2">
		{assign_by_name var="related_companies" value=$item.selected}
		{foreach from=$related_companies item="company"}
			<button type="button" id="{$field}_selected_company{$company.id}" class="btn btn-light mr-2">
				<input name="{$field}[{$company.id}]" type="hidden" value="{$company.id}" />
				<b>{$company.name}</b> <span onclick="removeSelectedCompany('{$field}', '{$company.id}');">x</span>
			</button>
		{/foreach}
	</div>
	
	<input type="text" 
		   id="{$field}" name="{$field}" value=""
		   class="form-control" autocomplete="off" 
		   placeholder="Search company by name"
		   
		   onkeyup="xajax_searchCompanies2(this.id, this.value, getInputsValuesAsArray('#{$field}_selected input'));" 
		   onfocus="xajax_searchCompanies2(this.id, this.value, getInputsValuesAsArray('#{$field}_selected input'));" 
		   onblur="setTimeout('hideCompanySuggestions2(this.id);', 400);" />
	
	<div id="{$field}_suggestions" style="display: none;"></div>
	
{elseif $item.type == "addresses"}
	<label class="control-label" for="{$field}">{$item.title}</label><br/>
	
	<div id="{$field}">
	
		{if $addresses}

			{foreach from=$addresses key="key" item="address"}
				{include file='components/xajax/address_row.tpl' row_id=$key}
			{/foreach}
			
			<button id="{$field}_plus" type="button" class="btn btn-light" 
					onclick="xajax_addAddressRow({$addresses|@count});">
				Add address
			</button>
		{else}

			{include file='components/xajax/address_row.tpl' row_id='0'}
			
			<button id="{$field}_plus" type="button" class="btn btn-light" 
					onclick="xajax_addAddressRow(1);">
				Add address
			</button>
		{/if}

	</div>
		
{elseif $item.type == "projects"}
	<label class="control-label" for="{$field}">{$item.title}</label><br/>
	
	<div id="{$field}">
		{if $projects}

			{foreach from=$projects key="key" item="project"}
				{include file='components/xajax/project_row.tpl' row_id=$key}
			{/foreach}
			
			<button id="{$field}_plus" type="button" class="btn btn-light" 
					onclick="xajax_addProjectRow({$projects|@count});">
				Add project
			</button>
		{else}

			{include file='components/xajax/project_row.tpl' row_id='0'}
			
			<button id="{$field}_plus" type="button" class="btn btn-light" 
					onclick="xajax_addProjectRow(1);">
				Add project
			</button>
		{/if}
	</div>
	
{elseif $item.type == "rating"}
	
	<label class="control-label" for="{$field}">{$item.title}</label><br/>
	
	<input type="text" 
		   id="{$field}" name="{$field}" value="{$smarty_post[$field]|escape}"
		   class="bootstrap-rating" data-size="md" />
	
{else}
	Unknown field type `{$item.type}`
{/if}