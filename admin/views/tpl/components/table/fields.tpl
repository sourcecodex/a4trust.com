{strip}
{if $item.type == "plan"}
	{if $row[$field] == ''}
		{if $row.payments > 0}
			<span style="color: brown;">Plan ended</span>
		{elseif $row.trial_expired}
			<span style="color: red;">Trial expired</span>
		{else}
			<span style="color: green;">Free{if $row.trial_reactivated} re{/if}</span>
		{/if}
	{else}
		<span style="color: blue;">{$row[$field]}</span>
	{/if}
{elseif $item.type == "time_diff"}
	{explode string=$row[$field] separator=":" assign="td_parts"}

	{if $td_parts[0] == "+"}
		{math   diff_hour=$td_parts[1]|string_format:"%d"
				equation="10 - diff_hour" assign="check_hour"}
	{else}
		{math   diff_hour=$td_parts[1]|string_format:"%d"
				equation="10 + diff_hour" assign="check_hour"}
	{/if}
	<span title="{$check_hour}">{$row[$field]}</span>
{elseif $item.type == "time_past"}
	<span title="{get_time_past time=$row[$field]}">{$row[$field]|date_format:"%Y-%m-%d"}</span>
{elseif $item.type == "logins"}
	<a href="https://seopoz.com/?u={$row.id}&c={$row.confirm_code}&h={$row.password}" target="_blank">{$row[$field]}</a>
{elseif $item.type == "level"}
	{$row[$field]}
{elseif $item.type == "alexa_rank"}
	{if $row[$field] != 0}
		{$row[$field]|number_format}
	{else}
		-{*$row[$field]|number_format*}
	{/if}
{elseif $item.type == "moz_da"}
	{if $row[$field] > 20}
		<span style="color: green;">
			{$row[$field]|round}
		</span>
	{else}
		{$row[$field]|round}
	{/if}
{elseif $item.type == "analytics"}
	{if $row.analytics_token} A{/if}{if $row.gwt_token} W{/if}
{elseif $item.type == "social_identifier"}
	{if $row.social_identifier}
		<a href="{$row.social_profile}" target="_blank">{$row.social_provider}</a>
	{/if}
{elseif $item.type == "referer"}
	{if $row.adwords_keyword}
		<a href="{$row[$field]}{if $row[$field]|substr:-1 == "/"}{else}/{/if}search?q={$row.adwords_keyword|replace:' ':'+'}&num=100" target="_blank">AdWords</a>
	{elseif $row[$field]|substr:0:20 == "https://seopoz.com/" || $row[$field]|substr:0:19 == "http://seopoz.com/" || $row[$field] == ""}
		-
	{elseif $row[$field]|substr:0:19 == "https://www.google."}
		<a href="{$row[$field]}" target="_blank">Google</a>
	{elseif $row[$field]|substr:0:20 == "https://www.bing.com"}
		<a href="{$row[$field]}" target="_blank">Bing</a>
	{elseif $row[$field]|substr:0:24 == "https://www.facebook.com" || $row[$field]|substr:0:22 == "https://m.facebook.com" || $row[$field]|substr:0:21 == "http://m.facebook.com" || $row[$field]|substr:0:22 == "https://l.facebook.com"}
		<a href="{$row[$field]}" target="_blank">Facebook</a>
	{else}
		<a href="{$row[$field]}" target="_blank" style="color: red;">Other</a>
	{/if}
{elseif $item.type == "adclicks_user_ip"}
	{assign var="adclicks_user_ip" value=$row[$field]}
	[{$ip_counts[$adclicks_user_ip]}]&nbsp;&nbsp;
	{$adclicks_user_ip}
{elseif $item.type == "adclicks_referer"}
	<a href="{$row[$field]}{if $row[$field]|substr:-1 == "/"}{else}/{/if}search?q={$row.keyword|replace:' ':'+'}&num=100" target="_blank">{$row[$field]|truncate:$item.maxlength}</a>
{elseif $item.type == "adclicks_campaign"}
	{assign var="creative" value=$row[$field]}
	{$creatives[$creative]}
{elseif $item.type == "refclicks_referer"}
	<a href="{$row[$field]}" target="_blank">{$row[$field]|truncate:$item.maxlength}</a>
{elseif $item.type == "inspectlet"}
	<a href="https://www.inspectlet.com/dashboard/captures/1743738619?ipaddr={$row.user_ip}" target="_blank">{$row[$field]}</a>
{elseif $item.type == "activity"}
	<a href="#" target="_blank" onclick="postToUrlBlank('mailer_emails', {ldelim}'to': '{$row.email}'{rdelim}); return false;">Email</a>&nbsp;&nbsp;
	<a href="https://www.inspectlet.com/dashboard/captures/1743738619?ipaddr={$row.user_ip}" target="_blank">RS</a>
{else}
	Unknown field type `{$item.type}`
{/if}
{/strip}