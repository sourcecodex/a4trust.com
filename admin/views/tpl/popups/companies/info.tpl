{capture name="modal_body"}
<div>
	<table style="margin-bottom: 20px;">
		<tr>
			<td>Id:&nbsp;</td>
			<td>{$company.id}</td>
			<td width="100">&nbsp;</td>
			<td>Type:&nbsp;</td>
			<td>{$company.type}</td>
		</tr>
		<tr>
			<td>Name:&nbsp;</td>
			<td>{$company.name}</td>
			<td width="100">&nbsp;</td>
			<td>Rating:&nbsp;</td>
			<td>{$company.rating}</td>
		</tr>
		<tr>
			<td>Description:&nbsp;</td>
			<td>{$company.description}</td>
			<td width="100">&nbsp;</td>
			<td>Revenue:&nbsp;</td>
			<td>{$company.revenue}</td>
		</tr>
	</table>
	
</div>
{/capture}
{include file="file:`$core_com`modal.tpl" controller=$core_controllers.companies modal_name="info_modal"}