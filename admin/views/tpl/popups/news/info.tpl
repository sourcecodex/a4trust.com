{capture name="modal_body"}
<div>
	<table style="margin-bottom: 20px;">
		<tr>
			<td>Id:&nbsp;</td>
			<td>{$user_info.id}</td>
			<td width="100">&nbsp;</td>
			<td>Created:&nbsp;</td>
			<td>{$user_info.created}</td>
		</tr>
		<tr>
			<td>Name:&nbsp;</td>
			<td>{$user_info.first_name} {$user_info.last_name}</td>
			<td width="100">&nbsp;</td>
			<td>Logged:&nbsp;</td>
			<td>{$user_info.last_online}</td>
		</tr>
		<tr>
			<td>Email:&nbsp;</td>
			<td><a href="mailto:{$user_info.email}">{$user_info.email}</a></td>
			<td width="100">&nbsp;</td>
			<td>User Ip:&nbsp;</td>
			<td>{$user_info.user_ip}</td>
		</tr>
	</table>
	
</div>
{/capture}
{include file="file:`$core_com`modal.tpl" controller=$core_controllers.news modal_name="info_modal"}