{capture name="modal_body"}
<div>
	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title">
				Users
			</h3>
		</div>
		<div class="panel-body">
			<table class="table table-striped table-hover table-condensed">
			{foreach from=$users item="user"}
				<tr>
					<td>{$user.id}</td>
					<td>{$user.email}</td>
					<td>{$user.location}</td>
					<td>{$user.plan}</td>
					<td>
						{if $user.adwords_keyword}
							<a href="{$user.http_referer}{if $user.http_referer|substr:-1 == "/"}{else}/{/if}search?q={$user.adwords_keyword|replace:' ':'+'}&num=100" target="_blank">AdWords</a>
						{elseif $user.http_referer|substr:0:20 == "https://seopoz.com/" || $row[$field]|substr:0:19 == "http://seopoz.com/" || $user.http_referer == ""}
							-
						{elseif $user.http_referer|substr:0:19 == "https://www.google."}
							<a href="{$user.http_referer}" target="_blank">Google</a>
						{elseif $user.http_referer|substr:0:19 == "http://www.bing.com"}
							<a href="{$user.http_referer}" target="_blank">Bing</a>
						{elseif $user.http_referer|substr:0:24 == "https://www.facebook.com"}
							<a href="{$user.http_referer}" target="_blank">Facebook</a>
						{else}
							<a href="{$user.http_referer}" target="_blank" style="color: red;">Other</a>
						{/if}
					</td>
					<td>{$user.keywords|@count} (Scanned: {$user.keywords_scanned})</td>
					{*<td>
						{foreach from=$user.scan_servers item="item"}
							{$item.scan_server}: {$item.keywords_count}, 
						{/foreach}
					</td>*}
				</tr>
			{/foreach}
			</table>
		</div>
	</div>
</div>
{/capture}
{include file="file:`$core_com`modal.tpl" controller=$core_controllers.users modal_name="list_modal"}