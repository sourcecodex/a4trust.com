{include file="includes/submenu.tpl"}

{if $core_pages->path[1] == "emails"}
	
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>
	<script type="text/javascript" src="{$config.views_url}js/chart.js"></script>
	
	<div class="row">
		<div class="col-md-6">
			<div style="padding: 10px 0;">
				<a href="#" onclick="postToUrl('mailer_emails', {ldelim}'type': '{$chart1_type}'{rdelim}); return false;">{$chart1_title}</a> 
				per {$period_metric} <b>{$chart1_average}</b> - Opened <b>{$chart1_average2}</b> ({$chart1_average2/$chart1_average*100|round:1}%)
			</div>
			<div id="chart1_data" style="display: none;">{$chart1_data}</div>
			<div id="chart1_data2" style="display: none;">{$chart1_data2}</div>
			<canvas id="chart1" width="400" height="150"></canvas>
		</div>
		<div class="col-md-6">
			<div style="padding: 10px 0;">
				<a href="#" onclick="postToUrl('mailer_emails', {ldelim}'type': '{$chart2_type}'{rdelim}); return false;">{$chart2_title}</a> 
				per {$period_metric} <b>{$chart2_average}</b> - Monthly <b>{$chart2_monthly}</b>
			</div>
			<div id="chart2_data" style="display: none;">{$chart2_data}</div>
			<canvas id="chart2" width="400" height="150"></canvas>
		</div>
	</div>
			
	<div class="row">
		<div class="col-md-6">
			<div style="padding: 10px 0;">
				<a href="#" onclick="postToUrl('mailer_emails', {ldelim}'type': '{$chart3_type}'{rdelim}); return false;">{$chart3_title}</a> 
				per {$period_metric} <b>{$chart3_average}</b> - Opened <b>{$chart3_average2}</b> ({$chart3_average2/$chart3_average*100|round:1}%)
			</div>
			<div id="chart3_data" style="display: none;">{$chart3_data}</div>
			<div id="chart3_data2" style="display: none;">{$chart3_data2}</div>
			<canvas id="chart3" width="400" height="150"></canvas>
		</div>
		<div class="col-md-6">
			<div style="padding: 10px 0;">
				<a href="#" onclick="postToUrl('mailer_emails', {ldelim}'type': '{$chart4_type}'{rdelim}); return false;">{$chart4_title}</a> 
				per {$period_metric} <b>{$chart4_average}</b> - Opened <b>{$chart4_average2}</b> ({$chart4_average2/$chart4_average*100|round:1}%)
			</div>
			<div id="chart4_data" style="display: none;">{$chart4_data}</div>
			<div id="chart4_data2" style="display: none;">{$chart4_data2}</div>
			<canvas id="chart4" width="400" height="150"></canvas>
		</div>
	</div>
			
	<div class="row">
		<div class="col-md-6">
			<div style="padding: 10px 0;">
				<a href="#" onclick="postToUrl('mailer_emails', {ldelim}'type': '{$chart5_type}'{rdelim}); return false;">{$chart5_title}</a> 
				per {$period_metric} <b>{$chart5_average}</b> - Opened <b>{$chart5_average2}</b> ({$chart5_average2/$chart5_average*100|round:1}%)
			</div>
			<div id="chart5_data" style="display: none;">{$chart5_data}</div>
			<div id="chart5_data2" style="display: none;">{$chart5_data2}</div>
			<canvas id="chart5" width="400" height="150"></canvas>
		</div>
		<div class="col-md-6">
			<div style="padding: 10px 0;">
				<a href="#" onclick="postToUrl('mailer_emails', {ldelim}'type': '{$chart6_type}'{rdelim}); return false;">{$chart6_title}</a> 
				per {$period_metric} <b>{$chart6_average}</b> - Opened <b>{$chart6_average2}</b> ({$chart6_average2/$chart6_average*100|round:1}%)
			</div>
			<div id="chart6_data" style="display: none;">{$chart6_data}</div>
			<div id="chart6_data2" style="display: none;">{$chart6_data2}</div>
			<canvas id="chart6" width="400" height="150"></canvas>
		</div>
	</div>
			
	<div class="row">
		<div class="col-md-6">
			<div style="padding: 10px 0;">
				<a href="#" onclick="postToUrl('mailer_emails', {ldelim}'type': '{$chart7_type}'{rdelim}); return false;">{$chart7_title}</a> 
				per {$period_metric} <b>{$chart7_average}</b> - Opened <b>{$chart7_average2}</b> ({$chart7_average2/$chart7_average*100|round:1}%)
			</div>
			<div id="chart7_data" style="display: none;">{$chart7_data}</div>
			<div id="chart7_data2" style="display: none;">{$chart7_data2}</div>
			<canvas id="chart7" width="400" height="150"></canvas>
		</div>
		<div class="col-md-6">
			<div style="padding: 10px 0;">
				<a href="#" onclick="postToUrl('mailer_emails', {ldelim}'type': '{$chart8_type}'{rdelim}); return false;">{$chart8_title}</a> 
				per {$period_metric} <b>{$chart8_average}</b> - Opened <b>{$chart8_average2}</b> ({$chart8_average2/$chart8_average*100|round:1}%)
			</div>
			<div id="chart8_data" style="display: none;">{$chart8_data}</div>
			<div id="chart8_data2" style="display: none;">{$chart8_data2}</div>
			<canvas id="chart8" width="400" height="150"></canvas>
		</div>
	</div>
			
	<script type="text/javascript">
		drawChart2(1);
		drawChart(2);
		drawChart2(3);
		drawChart2(4);
		drawChart2(5);
		drawChart2(6);
		drawChart2(7);
		drawChart2(8);
	</script>
{/if}

{if $core_pages->path[1] == "users_by_timezone"}
<table class="table">
<thead>
<tr>
	<th>Check Hour</th>
	<th>Premium users</th>
	<th>Premium domains</th>
	<th>Premium keywords</th>

	<th>Free users</th>
	<th>Free domains</th>
	<th>Free keywords</th>

	<th>Trial end users</th>
	<th>Trial end domains</th>
	<th>Trial end keywords</th>
</tr>
</thead>
<tbody>
{assign var="all_premium_users" value=0}
{assign var="all_premium_domains" value=0}
{assign var="all_premium_keywords" value=0}
{assign var="all_free_users" value=0}
{assign var="all_free_domains" value=0}
{assign var="all_free_keywords" value=0}
{assign var="all_trial_end_users" value=0}
{assign var="all_trial_end_domains" value=0}
{assign var="all_trial_end_keywords" value=0}
{foreach from=$keywords_stats key="hour" item="row"}
<tr>
	<td>{$hour}</td>
	{assign var="premium_users" value=$row.users_count-$row.users_free_count}
	{assign var="all_premium_users" value=$all_premium_users+$premium_users}
	<td>
		<a href="#" onclick="xajax_createPopup('users/list', {ldelim}'ids': '{$row.premium_users_ids}'{rdelim}); return false;">{$premium_users}</a>
	</td>
	{assign var="premium_domains" value=$row.domains_count-$row.domains_free_count}
	{assign var="all_premium_domains" value=$all_premium_domains+$premium_domains}
	<td>{$premium_domains}</td>
	{assign var="premium_keywords" value=$row.keywords_count-$row.keywords_free_count}
	{assign var="all_premium_keywords" value=$all_premium_keywords+$premium_keywords}
	<td>{$premium_keywords}</td>

	{assign var="free_users" value=$row.users_free_count-$row.users_trial_end_count}
	{assign var="all_free_users" value=$all_free_users+$free_users}
	<td>
		<a href="#" onclick="xajax_createPopup('users/list', {ldelim}'ids': '{$row.free_users_ids}'{rdelim}); return false;">{$free_users}</a>
	</td>
	{assign var="free_domains" value=$row.domains_free_count-$row.domains_trial_end_count}
	{assign var="all_free_domains" value=$all_free_domains+$free_domains}
	<td>{$free_domains}</td>
	{assign var="free_keywords" value=$row.keywords_free_count-$row.keywords_trial_end_count}
	{assign var="all_free_keywords" value=$all_free_keywords+$free_keywords}
	<td>{$free_keywords}</td>

	{assign var="all_trial_end_users" value=$all_trial_end_users+$row.users_trial_end_count}
	<td>{$row.users_trial_end_count}</td>
	{assign var="all_trial_end_domains" value=$all_trial_end_domains+$row.domains_trial_end_count}
	<td>{$row.domains_trial_end_count}</td>
	{assign var="all_trial_end_keywords" value=$all_trial_end_keywords+$row.keywords_trial_end_count}
	<td>{$row.keywords_trial_end_count}</td>
</tr>
{/foreach}
</tbody>
<tfoot>
<tr>
	<th>All</th>
	<th>{$all_premium_users}</th>
	<th>{$all_premium_domains}</th>
	<th>{$all_premium_keywords}</th>

	<th>{$all_free_users}</th>
	<th>{$all_free_domains}</th>
	<th>{$all_free_keywords}</th>

	<th>{$all_trial_end_users}</th>
	<th>{$all_trial_end_domains}</th>
	<th>{$all_trial_end_keywords}</th>
</tr>
</tfoot>
</table>

<div>
	<div>Bronze: {$users_stats.plan.bronze}</div>
	<div>Silver: {$users_stats.plan.silver}</div>
	<div>Gold: {$users_stats.plan.gold}</div>
	<div>Platinum: {$users_stats.plan.platinum}</div>
	<div>Enterprise: {$users_stats.plan.enterprise}</div>
</div>
{/if}
	
{if $core_pages->path[1] == "coming_payments"}
<table class="table">
<thead>
<tr>
	<th>Info</th>
	<th>Plan End</th>
	<th>Plan Start</th>
	<th>Name</th>
	<th>Email</th>
	<th>Billing Email</th>
	<th>Location</th>
	<th>Processor</th>
	<th>Plan</th>
	<th>Period</th>
	<th>Payments</th>
	<th>Total</th>
</tr>
</thead>
<tbody>
	{assign var="next_month_projection1" value="0"}
	{assign var="next_month_projection2" value="0"}
	{assign var="next_month_projection3" value="0"}
	{assign var="next_month_projection4" value="0"}
	{assign var="total_projection" value="0"}
	{foreach from=$stats item="stat"}
		{if !$stat.billing_email|in_array:$canceled}
			{assign var="projection_amount" value=$products[$stat.product_id].amount}
			{assign var="period" value=$products[$stat.product_id].period}
			
			{if $stat.plan_end|date_format:"%m" == "10"}
				{assign var="next_month_projection1" value=$next_month_projection1+$projection_amount}
			{elseif $stat.plan_end|date_format:"%m" == "11"}
				{assign var="next_month_projection2" value=$next_month_projection2+$projection_amount}
			{elseif $stat.plan_end|date_format:"%m" == "12"}
				{assign var="next_month_projection3" value=$next_month_projection3+$projection_amount}
			{elseif $stat.plan_end|date_format:"%m" == "01"}
				{assign var="next_month_projection4" value=$next_month_projection4+$projection_amount}
			{/if}
			
			{if $period == "5 years"}
				{assign var="projection_amount" value=$projection_amount/60}
			{elseif $period == "3 years"}
				{assign var="projection_amount" value=$projection_amount/36}
			{elseif $period == "1 year" || $period == "12 months"}
				{assign var="projection_amount" value=$projection_amount/12}
			{elseif $period == "6 months"}
				{assign var="projection_amount" value=$projection_amount/6}
			{elseif $period == "3 months"}
				{assign var="projection_amount" value=$projection_amount/3}
			{/if}
			
			{assign var="total_projection" value=$total_projection+$projection_amount}
		{/if}
		<tr {if $stat.billing_email|in_array:$canceled}style="background-color: #ECC6C5;"{/if}>
			<td>
				<button type="button" class="btn btn-default btn-xs" onclick="xajax_createPopup('users/info', '{$stat.user_id}');" title="Info">
					<span class="glyphicon glyphicon-info-sign"></span>
				</button>
			</td>
			<td>{$stat.plan_end}</td>
			<td>{$stat.plan_start}</td>
			<td>{$stat.first_name}</td>
			<td>{$stat.email}</td>
			<td>{$stat.billing_email}</td>
			<td>{$stat.location}</td>
			<td>{$stat.billing_processor}</td>
			<td>{$stat.plan}</td>
			<td>{$period}</td>
			<td>{$stat.payments}</td>
			<td>${$stat.total}</td>
		</tr>
	{/foreach}
</tbody>
</table>
<div style="margin-top: 20px;">
	Next payments:<br/>
	10 month: ${$next_month_projection1}<br/>
	11 month: ${$next_month_projection2}<br/>
	12 month: ${$next_month_projection3}<br/>
	01 month: ${$next_month_projection4}<br/>
	<br/>
	Total 1M: ${$total_projection|round:2}<br/>
	Total 3M: ${$total_projection*3}<br/>
	Total 1Y: ${$total_projection*12}<br/>
	<br/>
</div>
	
{literal}
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.10/css/jquery.dataTables.min.css" />
<script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
$(document).ready( function () {

	var table = $('.table').DataTable( {
		responsive: false,
		searching: false,
		info: true,
		paging: false,
		dom: '<"dt-toolbar">Bfrtip',
		aoColumnDefs:[ //Disable ordering for individual columns
			{
				bSortable: false,
				aTargets: [ 0 ]
			}
		],
		order: [[ 1, 'asc' ]],
	});
	
});
{/literal}
</script>
{/if}

{if $core_pages->path[1] == "monthly_revenue"}
<table class="table">
<thead>
<tr>
	<th>Month</th>
	<th>Amount</th>
	<th>Growth Rate</th>
	<th>Payments</th>
</tr>
</thead>
<tbody>
	{assign var="total_amount" value="0"}
	{assign var="total_payments" value="0"}
	{assign var="last_month_revenue" value=""}
	{foreach from=$stats item="stat"}
		{assign var="total_amount" value=$total_amount+$stat.amount}
		{assign var="total_payments" value=$total_payments+$stat.payments}
		<tr>
			<td>{$stat.month}</td>
			<td>${$stat.amount}</td>
			<td>{if $last_month_revenue == ""}-{else}{$stat.amount/$last_month_revenue*100-100|round:1}%{/if}</td>
			<td>{$stat.payments}</td>
		</tr>
		{assign var="last_month_revenue" value=$stat.amount}
	{/foreach}
	<tr>
		<td>Total:</td>
		<td>${$total_amount}</td>
		<td></td>
		<td>{$total_payments}</td>
	</tr>
</tbody>
</table>
		
<table class="table">
<thead>
<tr>
	<th>Month</th>
	<th>Amount</th>
	<th>Growth Rate</th>
	<th>Payments</th>
</tr>
</thead>
<tbody>
	{assign var="total_amount" value="0"}
	{assign var="total_payments" value="0"}
	{assign var="last_month_revenue" value=""}
	{foreach from=$stats2 item="stat"}
		{assign var="total_amount" value=$total_amount+$stat.amount}
		{assign var="total_payments" value=$total_payments+$stat.payments}
		<tr>
			<td>{$stat.month}</td>
			<td>${$stat.amount}</td>
			<td>{if $last_month_revenue == ""}-{else}{$stat.amount/$last_month_revenue*100-100|round:1}%{/if}</td>
			<td>{$stat.payments}</td>
		</tr>
		{assign var="last_month_revenue" value=$stat.amount}
	{/foreach}
	<tr>
		<td>Total:</td>
		<td>${$total_amount}</td>
		<td></td>
		<td>{$total_payments}</td>
	</tr>
</tbody>
</table>
{/if}

{if $core_pages->path[1] == "monthly_users"}
<table class="table">
<thead>
<tr>
	<th>Month</th>
	<th>Users</th>
	<th>Change</th>
	<th>Subscriptions</th>
	<th>Change</th>
	<th>Subscription Rate</th>
	<th>FP Amount</th>
</tr>
</thead>
<tbody>
	{assign var="total_users" value="0"}
	{assign var="total_subscriptions" value="0"}
	{assign var="total_payments" value="0"}
	{assign var="last_month_users" value=""}
	{assign var="last_month_subscriptions" value=""}
	{foreach from=$stats item="stat"}
		{assign var="total_users" value=$total_users+$stat.count}
		{assign var="total_subscriptions" value=$total_subscriptions+$stat.subscriptions}
		{assign var="total_payments" value=$total_payments+$stat.payments}
		<tr>
			<td>{$stat.month}</td>
			<td>{$stat.count}</td>
			<td>{if $last_month_users == ""}-{else}{$stat.count-$last_month_users}{/if}</td>
			<td>{$stat.subscriptions}</td>
			<td>{if $last_month_subscriptions == ""}-{else}{$stat.subscriptions-$last_month_subscriptions}{/if}</td>
			<td>{$stat.subscriptions/$stat.count*100|round:2}%</td>
			<td>${$stat.payments}</td>
		</tr>
	{assign var="last_month_users" value=$stat.count}
	{assign var="last_month_subscriptions" value=$stat.subscriptions}
	{/foreach}
	<tr>
		<td>Total:</td>
		<td>{$total_users}</td>
		<td>&nbsp;</td>
		<td>{$total_subscriptions}</td>
		<td>&nbsp;</td>
		<td>{$total_subscriptions/$total_users*100|round:2}%</td>
		<td>${$total_payments}</td>
	</tr>
</tbody>
</table>
		
<table class="table">
<thead>
<tr>
	<th>Month</th>
	<th>Users</th>
	<th>Change</th>
	<th>Subscriptions</th>
	<th>Change</th>
	<th>Subscription Rate</th>
	<th>FP Amount</th>
</tr>
</thead>
<tbody>
	{assign var="total_users" value="0"}
	{assign var="total_subscriptions" value="0"}
	{assign var="total_payments" value="0"}
	{assign var="last_month_users" value=""}
	{assign var="last_month_subscriptions" value=""}
	{foreach from=$stats2 item="stat"}
		{assign var="total_users" value=$total_users+$stat.count}
		{assign var="total_subscriptions" value=$total_subscriptions+$stat.subscriptions}
		{assign var="total_payments" value=$total_payments+$stat.payments}
		<tr>
			<td>{$stat.month}</td>
			<td>{$stat.count}</td>
			<td>{if $last_month_users == ""}-{else}{$stat.count-$last_month_users}{/if}</td>
			<td>{$stat.subscriptions}</td>
			<td>{if $last_month_subscriptions == ""}-{else}{$stat.subscriptions-$last_month_subscriptions}{/if}</td>
			<td>{$stat.subscriptions/$stat.count*100|round:2}%</td>
			<td>${$stat.payments}</td>
		</tr>
	{assign var="last_month_users" value=$stat.count}
	{assign var="last_month_subscriptions" value=$stat.subscriptions}
	{/foreach}
	<tr>
		<td>Total:</td>
		<td>{$total_users}</td>
		<td>&nbsp;</td>
		<td>{$total_subscriptions}</td>
		<td>&nbsp;</td>
		<td>{$total_subscriptions/$total_users*100|round:2}%</td>
		<td>${$total_payments}</td>
	</tr>
</tbody>
</table>
{/if}

{if $core_pages->path[1] == "clicks_by_keywords"}
<table class="table">
<thead>
<tr>
	<th>Keyword</th>
	<th>Clicks</th>
	<th>Signups</th>
	<th>Conversion</th>
	<th>Subscriptions</th>
</tr>
</thead>
<tbody>
	{foreach from=$stats key="month" item="month_stats"}
		<tr>
			<td colspan="5"><b>{$month}</b></td>
		</tr>
		{assign var="month_clicks" value="0"}
		{assign var="month_signups" value="0"}
		{assign var="month_subscriptions" value="0"}
		{foreach from=$month_stats item="stat"}
			{assign var="month_clicks" value=$month_clicks+$stat.clicks}
			{assign var="month_signups" value=$month_signups+$stat.signups}
			{assign var="month_subscriptions" value=$month_subscriptions+$stat.subscriptions}
			<tr>
				<td>
					<a href="#" onclick="postToUrl('adclicks', {ldelim}'keyword': '{$stat.keyword}'{rdelim}); return false;">{$stat.keyword}</a>
					{if $stat.keyword|in_array:$paused_keywords} <span style="color: red;">paused</span>{/if}
				</td>
				<td>{$stat.clicks}</td>
				<td>{$stat.signups}</td>
				<td>
					{if $stat.clicks && $stat.signups}
						{assign var="conversion" value=$stat.signups/$stat.clicks*100}
						<span style="color: {if $conversion|round < 10}red{else}green{/if};">{$conversion|round:1}%</span>
					{/if}
				</td>
				<td>{$stat.subscriptions}</td>
			</tr>
		{/foreach}
		<tr>
			<td>Total:</td>
			<td>{$month_clicks}</td>
			<td>{$month_signups}</td>
			<td>{if $month_clicks && $month_signups}{assign var="conversion" value=$month_signups/$month_clicks*100}{$conversion|round:1}%{/if}</td>
			<td>{$month_subscriptions}</td>
		</tr>
	{/foreach}
</tbody>
</table>
{/if}

{if $core_pages->path[1] == "clicks_by_keywords_campaign"}
<table class="table">
<thead>
<tr>
	<th>Keyword</th>
	<th>Clicks</th>
	<th>Signups</th>
	<th>Conversion</th>
	<th>Subscriptions</th>
</tr>
</thead>
<tbody>
	{foreach from=$stats key="campaign" item="campaign_stats"}
		<tr>
			<td colspan="5"><b>{$campaign}</b></td>
		</tr>
		{assign var="campaign_clicks" value="0"}
		{assign var="campaign_signups" value="0"}
		{assign var="campaign_subscriptions" value="0"}
		{foreach from=$campaign_stats item="stat"}
			{assign var="campaign_clicks" value=$campaign_clicks+$stat.clicks}
			{assign var="campaign_signups" value=$campaign_signups+$stat.signups}
			{assign var="campaign_subscriptions" value=$campaign_subscriptions+$stat.subscriptions}
			<tr>
				<td>
					<a href="#" onclick="postToUrl('adclicks', {ldelim}'keyword': '{$stat.keyword}'{rdelim}); return false;">{$stat.keyword}</a>
					{if $stat.keyword|in_array:$paused_keywords} <span style="color: red;">paused</span>{/if}
				</td>
				<td>{$stat.clicks}</td>
				<td>{$stat.signups}</td>
				<td>
					{if $stat.clicks && $stat.signups}
						{assign var="conversion" value=$stat.signups/$stat.clicks*100}
						<span style="color: {if $conversion|round < 10}red{else}green{/if};">{$conversion|round:1}%</span>
					{/if}
				</td>
				<td>{$stat.subscriptions}</td>
			</tr>
		{/foreach}
		<tr>
			<td>Total:</td>
			<td>{$campaign_clicks}</td>
			<td>{$campaign_signups}</td>
			<td>{if $campaign_clicks && $campaign_signups}{assign var="conversion" value=$campaign_signups/$campaign_clicks*100}{$conversion|round:1}%{/if}</td>
			<td>{$campaign_subscriptions}</td>
		</tr>
	{/foreach}
</tbody>
</table>
{/if}

{if $core_pages->path[1] == "clicks_by_keywords_total" || $core_pages->path[1] == "clicks_by_keywords_year"}
<table class="table">
<thead>
<tr>
	<th>Keyword</th>
	<th>Clicks</th>
	<th>Signups</th>
	<th>Conversion</th>
	<th>Subscriptions</th>
	<th>Conversion</th>
</tr>
</thead>
<tbody>
	{assign var="month_clicks" value="0"}
	{assign var="month_signups" value="0"}
	{assign var="month_subscriptions" value="0"}
	{foreach from=$stats item="stat"}
		{assign var="month_clicks" value=$month_clicks+$stat.clicks}
		{assign var="month_signups" value=$month_signups+$stat.signups}
		{assign var="month_subscriptions" value=$month_subscriptions+$stat.subscriptions}
		{if $smarty.get.paused}
			{if $stat.keyword|in_array:$paused_keywords}
			<tr>
				<td>
					<a href="#" onclick="postToUrl('adclicks', {ldelim}'keyword': '{$stat.keyword}'{rdelim}); return false;">{$stat.keyword}</a>
					{*if $stat.keyword|in_array:$paused_keywords} <span style="color: red;">paused</span>{/if*}
				</td>
				<td>{$stat.clicks}</td>
				<td>{$stat.signups}</td>
				<td>
					{if $stat.clicks && $stat.signups}
						{assign var="conversion" value=$stat.signups/$stat.clicks*100}
						<span style="color: {if $conversion|round < 10}red{else}green{/if};">{$conversion|round:1}%</span>
					{/if}
				</td>
				<td>{$stat.subscriptions}</td>
				<td>
					{if $stat.signups && $stat.subscriptions}
						{assign var="conversion" value=$stat.subscriptions/$stat.signups*100}
						<span style="color: {if $conversion|round < 5}red{else}green{/if};">{$conversion|round:1}%</span>
					{/if}
				</td>
			</tr>
			{/if}
		{else}
			{if !$stat.keyword|in_array:$paused_keywords}
			<tr>
				<td>
					<a href="#" onclick="postToUrl('adclicks', {ldelim}'keyword': '{$stat.keyword}'{rdelim}); return false;">{$stat.keyword}</a>
					{*if $stat.keyword|in_array:$paused_keywords} <span style="color: red;">paused</span>{/if*}
				</td>
				<td>{$stat.clicks}</td>
				<td>{$stat.signups}</td>
				<td>
					{if $stat.clicks && $stat.signups}
						{assign var="conversion" value=$stat.signups/$stat.clicks*100}
						<span style="color: {if $conversion|round < 10}red{else}green{/if};">{$conversion|round:1}%</span>
					{/if}
				</td>
				<td>{$stat.subscriptions}</td>
				<td>
					{if $stat.signups && $stat.subscriptions}
						{assign var="conversion" value=$stat.subscriptions/$stat.signups*100}
						<span style="color: {if $conversion|round < 5}red{else}green{/if};">{$conversion|round:1}%</span>
					{/if}
				</td>
			</tr>
			{/if}
		{/if}
	{/foreach}
	<tr>
		<td>Total:</td>
		<td>{$month_clicks}</td>
		<td>{$month_signups}</td>
		<td>{if $month_clicks && $month_signups}{assign var="conversion" value=$month_signups/$month_clicks*100}{$conversion|round:1}%{/if}</td>
		<td>{$month_subscriptions}</td>
		<td>{if $month_subscriptions && $month_signups}{assign var="conversion" value=$month_subscriptions/$month_signups*100}{$conversion|round:1}%{/if}</td>
	</tr>
</tbody>
</table>
{/if}

{if $core_pages->path[1] == "clicks_by_location"}
<table class="table">
<thead>
<tr>
	<th>Location</th>
	<th>Clicks</th>
	<th>Signups</th>
	<th>Conversion</th>
	<th>Subscriptions</th>
</tr>
</thead>
<tbody>
	{foreach from=$stats key="month" item="month_stats"}
		<tr>
			<td colspan="5"><b>{$month}</b></td>
		</tr>
		{assign var="month_clicks" value="0"}
		{assign var="month_signups" value="0"}
		{assign var="month_subscriptions" value="0"}
		{foreach from=$month_stats item="stat"}
			{assign var="month_clicks" value=$month_clicks+$stat.clicks}
			{assign var="month_signups" value=$month_signups+$stat.signups}
			{assign var="month_subscriptions" value=$month_subscriptions+$stat.subscriptions}
			<tr>
				<td>
					<a href="#" onclick="postToUrl('adclicks', {ldelim}'location': ', {$stat.location}'{rdelim}); return false;">{$stat.location}</a>
					{if $stat.location|in_array:$paused_countries} <span style="color: red;">paused</span>{/if}
				</td>
				<td>{$stat.clicks}</td>
				<td>{$stat.signups}</td>
				<td>
					{if $stat.clicks && $stat.signups}
						{assign var="conversion" value=$stat.signups/$stat.clicks*100}
						<span style="color: {if $conversion|round < 10}red{else}green{/if};">{$conversion|round:1}%</span>
					{/if}
				</td>
				<td>{$stat.subscriptions}</td>
			</tr>
		{/foreach}
		<tr>
			<td>Total:</td>
			<td>{$month_clicks}</td>
			<td>{$month_signups}</td>
			<td>{if $month_clicks && $month_signups}{assign var="conversion" value=$month_signups/$month_clicks*100}{$conversion|round:1}%{/if}</td>
			<td>{$month_subscriptions}</td>
		</tr>
	{/foreach}
</tbody>
</table>
{/if}

{if $core_pages->path[1] == "clicks_by_location_total" || $core_pages->path[1] == "clicks_by_location_year"}
<table class="table">
<thead>
<tr>
	<th>Location</th>
	<th>Clicks</th>
	<th>Signups</th>
	<th>Conversion</th>
	<th>Subscriptions</th>
	<th>Conversion</th>
</tr>
</thead>
<tbody>
	{assign var="month_clicks" value="0"}
	{assign var="month_signups" value="0"}
	{assign var="month_subscriptions" value="0"}
	{foreach from=$stats item="stat"}
		{assign var="month_clicks" value=$month_clicks+$stat.clicks}
		{assign var="month_signups" value=$month_signups+$stat.signups}
		{assign var="month_subscriptions" value=$month_subscriptions+$stat.subscriptions}
		{if $smarty.get.paused}
			{if $stat.location|in_array:$paused_countries}
			<tr>
				<td>
					<a href="#" onclick="postToUrl('adclicks', {ldelim}'location': ', {$stat.location}'{rdelim}); return false;">{$stat.location}</a>
				</td>
				<td>{$stat.clicks}</td>
				<td>{$stat.signups}</td>
				<td>
					{if $stat.clicks && $stat.signups}
						{assign var="conversion" value=$stat.signups/$stat.clicks*100}
						<span style="color: {if $conversion|round < 10}red{else}green{/if};">{$conversion|round:1}%</span>
					{/if}
				</td>
				<td>{$stat.subscriptions}</td>
				<td>
					{if $stat.signups && $stat.subscriptions}
						{assign var="conversion" value=$stat.subscriptions/$stat.signups*100}
						<span style="color: {if $conversion|round < 5}red{else}green{/if};">{$conversion|round:1}%</span>
					{/if}
				</td>
			</tr>
			{/if}
		{else}
			{if !$stat.location|in_array:$paused_countries}
			<tr>
				<td>
					<a href="#" onclick="postToUrl('adclicks', {ldelim}'location': ', {$stat.location}'{rdelim}); return false;">{$stat.location}</a>
				</td>
				<td>{$stat.clicks}</td>
				<td>{$stat.signups}</td>
				<td>
					{if $stat.clicks && $stat.signups}
						{assign var="conversion" value=$stat.signups/$stat.clicks*100}
						<span style="color: {if $conversion|round < 10}red{else}green{/if};">{$conversion|round:1}%</span>
					{/if}
				</td>
				<td>{$stat.subscriptions}</td>
				<td>
					{if $stat.signups && $stat.subscriptions}
						{assign var="conversion" value=$stat.subscriptions/$stat.signups*100}
						<span style="color: {if $conversion|round < 5}red{else}green{/if};">{$conversion|round:1}%</span>
					{/if}
				</td>
			</tr>
			{/if}
		{/if}
	{/foreach}
	<tr>
		<td>Total:</td>
		<td>{$month_clicks}</td>
		<td>{$month_signups}</td>
		<td>{if $month_clicks && $month_signups}{assign var="conversion" value=$month_signups/$month_clicks*100}{$conversion|round:1}%{/if}</td>
		<td>{$month_subscriptions}</td>
		<td>{if $month_subscriptions && $month_signups}{assign var="conversion" value=$month_subscriptions/$month_signups*100}{$conversion|round:1}%{/if}</td>
	</tr>
</tbody>
</table>
{/if}

{if $core_pages->path[1] == "check_buffer"}
<table class="table">
<thead>
<tr>
	<th>Scan Server</th>
	<th>Count</th>
	<th>Scan Time</th>
	<th>Errors</th>
	<th>Timeout</th>
	<th></th>
</tr>
</thead>
<tbody>
	{foreach from=$stats item="stat"}
	<tr>
		<td>{$stat.scan_server}</td>
		<td>{$stat.count}</td>
		
		{math assign="hours" equation="floor(x / 60)" x=$stat.count}
		{math assign="minutes" equation="x % 60" x=$stat.count}
		<td>{$hours}h {$minutes}min</td>
		<td>{$servers[$stat.scan_server].error_count}</td>
		<td>{if $servers[$stat.scan_server].timeout}{$servers[$stat.scan_server].timeout_from}{/if}</td>
		<td>
			{if $servers[$stat.scan_server].timeout}
			<button class="btn btn-default btn-xs" onclick="postToUrl('', {ldelim}'id': '{$servers[$stat.scan_server].id}', 'controller': 'scan_servers', 'act': 'reset'{rdelim})">Reset</button>
			{/if}
		</td>
	</tr>
	{/foreach}
</tbody>
</table>
{/if}	

{if $core_pages->path[1] == "keywords_with_errors"}
<table class="table">
<thead>
<tr>
	<th>User</th>
	<th>Domain</th>
	<th>Keyword</th>
	<th>Scan Server</th>
	<th>Errors</th>
</tr>
</thead>
<tbody>
	{foreach from=$stats item="stat"}
	<tr>
		<td>{$stat.user_id}</td>
		<td>{$stat.domain}</td>
		<td>{$stat.keyword}</td>
		<td>{$stat.scan_server}</td>
		<td>{$stat.errors}</td>
	</tr>
	{/foreach}
</tbody>
</table>
{/if}	