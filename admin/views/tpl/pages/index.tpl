{if $core_user->isLogged}

	{include file="includes/submenu.tpl"}
	
	{include file="includes/dashboard.tpl"}
	
{else}
	<div class="login_box">
		{include file="file:`$core_com`form.tpl" controller=$core_controllers.users form_name="sign_in_form" act="signIn"}
	</div>
{/if}