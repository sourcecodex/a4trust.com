function createPopup() {

	$('body').append('<div id="popup_background"></div>');

	$('#popup_background').css({
		'display': 'none',
		'position': 'fixed',
		'z-index': 10001,
		'top': '0',
		'left': '0',
		'background-color': 'black',
		'opacity': '0.5',
		'height': $(window).height(),
		'width': $(window).width()
	});

	$('#popup_background').fadeIn('fast');

	$('#popup_background').click(function(){
		closePopup();
	});

	$('body').append('<div id="popup_content"></div>');

	$('#popup_content').css({
		'display': 'none',
		'position': 'fixed',
		'z-index': 10002,

		'min-height': '100px',
		'min-width': '100px',

		'cursor': 'auto'
		//'overflow': 'auto'
	});

}

function positionPopup() {
    var popup_top = ($(window).height()-$('#popup_content').height())/2;

	if (popup_top > 5) {
		$('#popup_content').css({'position': 'fixed', 'top': popup_top+'px'});
	}
	else {
		$('#popup_content').css({'position': 'absolute', 'top': ($(document).scrollTop()+5)+'px'});
	}

	$('#popup_content').css({
		'left': ($(window).width()-$('#popup_content').width())/2+'px'
	});

	$('#popup_background').css({
		'height': $(window).height(),
		'width': $(window).width()
	});
}

function showPopup() {

	positionPopup();

	$('#popup_content').fadeIn('fast');

	$(window).resize(function() {
		positionPopup();
	});

	$('#popup_content img').load(function() {
		positionPopup();
	});
}

function closePopup() {
	$('#popup_background').fadeOut('fast');
	$('#popup_background').remove();
	$('#popup_content').remove();
}

function createPopupCallback(popup_name, form_data) {

	var data = JSON.parse(form_data);

	// Admin

	if (popup_name == "companies/add" || popup_name == "companies/edit") {
		adjustCompanyType();
	
		$("#type").on('change', function() {
			adjustCompanyType();
		});
		
		tinymce.init({
			selector: '.tinymce',
			plugins: [
				'advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker',
				'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
				'save table directionality emoticons template paste'
			],
			toolbar: ' styleselect | bold italic underline | bullist numlist blockquote | link unlink',
			menubar: false,
			branding: false,
			setup: function (editor) {
			editor.on('change', function () {
					editor.save();
				});
			}
		});
		
		$(".bootstrap-rating").rating();
		
		initAutocomplete();
	}
	else if (popup_name == "news/add" || popup_name == "news/edit") {
		tinymce.init({
			selector: '.tinymce',
			plugins: [
				'advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker',
				'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
				'save table directionality emoticons template paste'
			],
			toolbar: ' styleselect | bold italic underline | bullist numlist blockquote | link unlink',
			menubar: false,
			branding: false,
			setup: function (editor) {
			editor.on('change', function () {
					editor.save();
				});
			}
		});
	}
	
}