FormDataField = function () {
	this.name = null;
	this.type = null;
	this.value = null;
	this.classes = null;
};

function getCheckFormData(form) {
	var form_data = new Array();

    var inputs = form.getElementsByTagName("input");
	var textareas = form.getElementsByTagName("textarea");
	var selects = form.getElementsByTagName("select");

	for (i=0; i < inputs.length; i++) {
		if (inputs[i].type == "checkbox" || inputs[i].type == "radio") {
			if (!inputs[i].checked) continue;
		}
		if (inputs[i].disabled) continue;
		
		form_field = new FormDataField();
		form_field.name = inputs[i].name;
		form_field.type = inputs[i].type;
		form_field.value = inputs[i].value;
		form_field.classes = inputs[i].className.split(" ");
		form_data.push(form_field);
	}

	for (i=0; i < textareas.length; i++) {
		if (textareas[i].disabled) continue;
		
		form_field = new FormDataField();
		form_field.name = textareas[i].name;
		form_field.type = textareas[i].type;
		form_field.value = textareas[i].value;
		form_field.classes = textareas[i].className.split(" ");
		form_data.push(form_field);
	}

	for (i=0; i < selects.length; i++) {
		if (selects[i].disabled) continue;
		
		form_field = new FormDataField();
		form_field.name = selects[i].name;
		form_field.type = selects[i].type;
		form_field.value = selects[i].value;
		form_field.classes = selects[i].className.split(" ");
		form_data.push(form_field);
	}

	return form_data;
}

function getFormDataIds() {
	var form_data = new Array();
	
	var inputs = document.getElementsByTagName("input");
	
	for (i=0; i < inputs.length; i++) {
		if (inputs[i].type == "checkbox") {
			if (!inputs[i].checked) continue;
		}
		form_field = new FormDataField();
		form_field.name = inputs[i].name;
		form_field.type = inputs[i].type;
		form_field.value = inputs[i].value;
		form_field.classes = inputs[i].className.split(" ");
		form_data.push(form_field);
	}
	
	return form_data;
}

var ladda_button = null;

function checkFormErrorHandling(errors) {
	
	$('#submit_button').prop('disabled', false);

	if (ladda_button) {
		ladda_button.stop();
	}
	
	for (var i=0; i < errors.length; i++) {
		$('#'+errors[i][0]+'-group .help-block').html('');
		$('#'+errors[i][0]+'-group .help-block').hide();
	}
	
	for (var i=0; i < errors.length; i++) {
		if (errors[i][1] === "ok") {
			$('#'+errors[i][0]+'-group').removeClass('has-error');
			$('#'+errors[i][0]+'-group').addClass('has-success');
			$('#'+errors[i][0]+'-group').addClass('has-feedback');
			$('#'+errors[i][0]+'-group .form-control-feedback').show();
			$('#'+errors[i][0]+'-group .form-control-feedback').removeClass('glyphicon-remove');
			$('#'+errors[i][0]+'-group .form-control-feedback').addClass('glyphicon-ok');
			$('#'+errors[i][0]+'-group .help-block').hide();
		}
		else {
			if ($('#'+errors[i][0]+'-group .help-block').is(':visible')) {
				$('#'+errors[i][0]+'-group .help-block').html($('#'+errors[i][0]+'-group .help-block').html()+'<br/>'+errors[i][1]);
			}
			else {
				$('#'+errors[i][0]+'-group').removeClass('has-success');
				$('#'+errors[i][0]+'-group').addClass('has-error');
				$('#'+errors[i][0]+'-group').addClass('has-feedback');
				$('#'+errors[i][0]+'-group .form-control-feedback').show();
				$('#'+errors[i][0]+'-group .form-control-feedback').removeClass('glyphicon-ok');
				$('#'+errors[i][0]+'-group .form-control-feedback').addClass('glyphicon-remove');
				$('#'+errors[i][0]+'-group .help-block').html(errors[i][2]);
				$('#'+errors[i][0]+'-group .help-block').show();
			}
		}
	}

	//console.log(errors);
}

function checkFormSuccessful(controller_name, function_name, form_data) {

	var data = JSON.parse(form_data);

	if (controller_name == "keywords" && (function_name == "add" || function_name == "edit")) {

		if (function_name == "add") {
			var item_id = data['function_result'];
		}
		else if (function_name == "edit") {
			var item_id = data['id'];
		}

		location.reload();
	}
}

function checkFormCallback(controller_name, function_name, form_data) {
	// TODO
}