// Default functions

function trim(stringToTrim) {
	return stringToTrim.replace(/^\s+|\s+$/g,"");
}

function submitEnter(form, e) {
	var keycode;
	if (window.event) keycode = window.event.keyCode;
	else if (e) keycode = e.which;
	else return true;

	if (keycode == 13) {
		checkForm(form);
		return false;
	}
	else return true;
}

function markAll(form, checked) {
	var inputs = form.getElementsByTagName("input");

	for (i=0; i < inputs.length; i++) {
		if (inputs[i].type == "checkbox") {
			inputs[i].checked = checked;
		}
	}
}

function disableSelection(target) {
	if (typeof target.onselectstart != "undefined") //IE route
		target.onselectstart = function(){return false}
	else if (typeof target.style.MozUserSelect != "undefined") //Firefox route
		target.style.MozUserSelect="none"
	else //All other route (ie: Opera)
		target.onmousedown = function(){return false}
	target.style.cursor = "default"
}

function getFormElements(form) {
	var result = new Array();
	if (form.elements) {
		for(var i in form.elements) {
			result[form.elements[i].name] = form.elements[i].value;
		}
	}
	return result;
}

function postToUrl(path, params, method) {
    method = method || "post";

    var form = document.createElement("form");
    form.setAttribute("method", method);
    form.setAttribute("action", path);

    for (var key in params) {
        var hiddenField = document.createElement("input");
        hiddenField.setAttribute("type", "hidden");
        hiddenField.setAttribute("name", key);
        hiddenField.setAttribute("value", params[key]);

        form.appendChild(hiddenField);
    }

    document.body.appendChild(form);
    form.submit();
}

// Custom functions

function signOut() {
	postToUrl('', {'controller': 'users', 'act': 'signOut'});
}

// Xajax functions

function resendConfirmation(email) {
	xajax_tableFunctionCall('users', 'resendConfirmation', {'email': email});
}

function tableDeleteItem(controller_name, item_id, confirmation) {
	if (confirmation != '') {
		if (confirm(confirmation)) {
			xajax_tableDeleteItem(controller_name, item_id);
		}
	}
	else {
		xajax_tableDeleteItem(controller_name, item_id);
	}
}

// Xajax callbacks

function tableDeleteItemCallback(controller_name, form_data) {
	location.reload();
}

function tableFunctionCallback(controller_name, function_name, form_data) {
	if (controller_name == "requests" && function_name == "approve") {
		location.reload();
	}
	else if (controller_name == "users" && function_name == "resendConfirmation") {
		var data = JSON.parse(form_data);

		if (data['function_result']) {
			alert('Email transfered successfully!');
		}
		else {
			alert('Email already confirmed!');
			window.location = '/login';
		}
	}
	else if (controller_name == "crm_contacts" && function_name == "export") {
		var data = JSON.parse(form_data);
		$('#crm_contacts_export').html(data['function_result']);
	}
}

// Document ready

$(document).ready(function(){
	var top_bar_height = $(".navbar").height();
	var footer_height = $(".footer").height();
	var content_height = $(window).height() - top_bar_height - footer_height - 21;

	$('.page_content').css("min-height", content_height+"px");
	
	tinymce.init({
		selector: '.tinymce',
		plugins: [
			'advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker',
			'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
			'save table directionality emoticons template paste'
		],
		toolbar: ' styleselect | bold italic underline | bullist numlist blockquote | link unlink',
		menubar: false,
		branding: false,
		setup: function (editor) {
        editor.on('change', function () {
				editor.save();
			});
		}
	});
	
	$('.slick-slider').slick({
		slidesToScroll: 1,
		slidesToShow: 1,
		adaptiveHeight: true,
		arrows: true,
		dots: true,
		autoplay: true,
		autoplaySpeed: 3000,
		centerMode: false,
		centerPadding: "50px",
		draggable: true,
		fade: false,
		focusOnSelect: false,
		infinite: false,
		pauseOnHover: false,
		responsive: [],
		rtl: false,
		speed: 300,
		vertical: false,
		prevArrow: '<div class="slick-prev" aria-label="Previous"><i class="fas fa-chevron-left"></i></div>',
		nextArrow: '<div class="slick-next" aria-label="Next"><i class="fas fa-chevron-right"></i></div>'
	});
	
	$(".bootstrap-rating").rating();
	
});

function adjustCompanyType() {
	if ($("#type").val() === '1') {
		$("#sector-group").show();
		$("#producers-group").show();
		$("#production-group").hide();
	}
	else {
		$("#sector-group").hide();
		$("#producers-group").hide();
		$("#production-group").show();
	}
}

// component search companies

function setCompanyId(company_id) {
	$('#company_id').val(company_id);
	xajax_getCompanyInfo(company_id);
	$('#company_selected').show();
}

function unsetCompanyId() { 
	$('#company_id').val('');
	$('#company_selected').hide();
}

function showCompanySuggestions() {
	$('#company_suggestions').css({
		top: 34
	});
	$('#company_suggestions').slideDown('fast');
}

function hideCompanySuggestions() {
	$("#company_suggestions").html("");
	$('#company_suggestions').hide();
}

function setSelectedCompany(company_id) {
	$("#company").val($('#company_name'+company_id).html());
	hideCompanySuggestions();
	setCompanyId(company_id);
}

// component related companies

function showCompanySuggestions2(field) {
	$('#'+field+'_suggestions').css({
		top: 34
	});
	$('#'+field+'_suggestions').slideDown('fast');
}

function hideCompanySuggestions2(field) {
	$('#'+field+'_suggestions').html('');
	$('#'+field+'_suggestions').hide();
}

function addSelectedCompany(field, company_id) {
	$('#'+field).val('');
	$('#'+field+'_selected').append(
			'<button type="button" id="'+field+'_selected_company'+company_id+'" class="btn btn-light mr-2">'+
			'<input name="'+field+'['+company_id+']" type="hidden" value="'+company_id+'" />'+
			'<b>'+$('#'+field+'_company_name'+company_id).html()+
			'</b> <span onclick="removeSelectedCompany(\''+field+'\', '+company_id+');">x</span></button>'
	);
	hideCompanySuggestions2(field);
}

function removeSelectedCompany(field, company_id) {
	$('#'+field+'_selected_company'+company_id).remove();
}

function getInputsValuesAsArray(selector) {

	var values = new Array();

	$(selector).each(function(index) {
		values.push($(this).attr('value'));
	});
	
	return values;
}