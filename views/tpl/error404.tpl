<div class="alert alert-danger">
	<b>Error 404:</b> Page not found
</div>
<div id="wrapper-content" class="wrapper-content text-center pb-13">
	<div class="image"><img src="{$config.views_url}images/other/under-contruction.png" alt="Under construction"></div>
	<div class="pt-9">
		<h3 class="mb-3">Sorry, we’re doing some work on site.</h3>
		<p class="font-size-md mb-0">Thank you for being patient. We will be back soon</p>
	</div>
</div>