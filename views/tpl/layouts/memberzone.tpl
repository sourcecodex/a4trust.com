<div id="site-wrapper" class="site-wrapper starter demo-header">
	
	{include file="includes/topbar.tpl"}
	
	{if $core_pages->current_page.type == "core"}
		{include file="file:`$config.core_tpl_dir``$core_pages->include_tpl`"}
	{else}
		{include file=$core_pages->include_tpl}
	{/if}
	
	{include file="includes/footer.tpl"}
	
</div>
