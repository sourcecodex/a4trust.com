<div id="page-title" class="page-title text-center mt-10">
	<div class="container">
		<div class="h-100 pt-12">
			<h1 class="mb-0 fadeInDown animated" data-animate="fadeInDown">
				Our Solutions
			</h1>
		</div>
	</div>
</div>
			
			
<div id="wrapper-content" class="wrapper-content pt-11 pb-13">
	<div class="container">
		<div class="page-description text-center text-dark font-size-h5 lh-1625">
			Aim 4 Trust provides you with smart solutions not only to connect with your partners and search the database 
			for new partners, but to keep up to date information on their latest rating, depending on current debts. 
			It's designed to help you tp evaluate your payment terms as well as recovering overdue debts.					
		</div>
		<div class="how-it-work">
			<div class="row align-items-center mb-7">
				<div class="col-md-6 mb-4 mb-md-0">
					<div class="p-12">
						<img src="{$config.views_url}images/how-it-works1.webp" alt="How it work 1" />
					</div>
				</div>
				<div class="col-md-6">
					<h5 class="mb-4">1. Database of Producers & Dealers</h5>
					<div class="mb-5">
						<b>Search the database</b><br/>
						Whether you are looking for new export partners or checking on the existing ones, use the database to make the 
						right decision for your sales strategy.
					</div>
					<div class="mb-5">
						<b>Follow companies to get updates</b><br/>
						Always be up to date - follow the companies you are interested in, get notified if company looses rating.
					</div>
					<div class="mb-5">
						<b>Corporate clients</b><br/>
						Have multinational client - find the right partner to complete project aborad by filtering location, same producers 
						as you work with or even the same clients.
					</div>
				</div>
			</div>
			<div class="item row align-items-center mb-7">
				<div class="col-md-6  order-1 order-md-0 pr-0  pr-md-5">
					<h5 class="mb-4">2. Ratings & More</h5>
					<div class="mb-5">
						<b>Check the ratings</b><br/>
						We hold a database of dealers & producers with a ranking system. Indentify unreliable partners with unsetteled debts.
					</div>
					<div class="mb-5">
						<b>Adapt your payment terms</b><br/>
						Check the company's rating (low rating means there are active debts) before making a decision on your payment terms.
					</div>
					<div class="mb-5">
						<b>Blacklist</b><br/>
						Filter the database by ratings, find which companies are not to be trusted. We recommend to use only advance 
						payment terms with companies who have unsettled debts.
					</div>
				</div>
				<div class="col-md-6 mb-4 mb-md-0 order-0 order-md-1">
					<div class="p-12">
						<img src="{$config.views_url}images/how-it-works2.webp" alt="How it work 2" />
					</div>
				</div>
			</div>
			<div class="item row align-items-center mb-7">
				<div class="col-md-6 mb-4 mb-md-0">
					<div class="p-12">
						<img src="{$config.views_url}images/how-it-works3.webp" alt="How it work 3" />
					</div>
				</div>
				<div class="col-md-6 ">
					<h5 class="mb-4">3. Debt recovery</h5>
					<div class="mb-5">
						<b>1 for All & All for 1</b><br/>
						Only when united there is a leverage to influence a company to settle the debt. By not paying 1 debt, all companies 
						can change their payment terms.
					</div>
					<div class="mb-5">
						<b>Submit a debt</b><br/>
						Have an unsettled debt? Submit to us and companies rating will be adjusted, all producers will be notified of 
						reduced rating and recommended to adjust the payment terms.
					</div>
					<div class="mb-5">
						<b>Recover</b><br/>
						Once a company receives a pressure from all producers they work with and information of low rating is visible, 
						it will be much easier to leverage the company to cover the debts.
					</div>
				</div>
			</div>
		</div>
		<div class="add-listing text-center pb-3">
			<h4 class="mb-7 px-6">
				Ready to reach all of the people who matter most to your
				business?
			</h4>
			<a href="/user/signup" class="btn btn-primary btn-block font-size-h4 btn-lg lh-16">Sign Up Now</a>
		</div>

	</div>
</div>