<div id="wrapper-content" class="panel wrapper-content pt-0 pb-0">
    <div class="page-wrapper d-flex flex-wrap flex-xl-nowrap">
		
        {include file="includes/sidebar.tpl"}
		
		<div class="page-container">
            <div class="container-fluid">
				<div class="page-content-wrapper d-flex flex-column">
					
					<h1 class="font-size-h4 mb-4 font-weight-normal">My Profile</h1>
					
					<div class="row">
						<div class="col-lg-6 mb-4 mb-lg-0">
							<div class="card rounded-0 border-0 bg-white px-4 pt-3 pb-6">
								<div class="card-header p-0 bg-transparent">
									<h5 class="card-title text-capitalize">Profile Details</h5>
								</div>
								<div class="card-body px-0 pt-4 pb-0">
									
									{if $smarty.get.updated}
										<div class="alert alert-success">
											User profile has been updated
										</div>
									{/if}
									
									{include file="file:`$core_com`form.tpl" controller=$core_controllers.users 
											 form_name="profile_edit_form" act="updateAccount"}

								</div>
							</div>
						</div>
						
						<div class="col-lg-6">
							<div class="card rounded-0 border-0 bg-white px-4 pt-3 pb-6">
								<div class="card-header p-0 bg-transparent">
									<h5 class="card-title text-capitalize">Change Password</h5>
								</div>
								<div class="card-body px-0 pt-4 pb-0">
									
									{include file="file:`$core_com`form.tpl" controller=$core_controllers.users 
											 form_name="change_pass_form" act="changePassword"}

								</div>
							</div>
						</div>
					</div>
					
				</div>
			</div>
		</div>
	</div>
</div>

