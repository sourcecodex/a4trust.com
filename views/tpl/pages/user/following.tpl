<div id="wrapper-content" class="panel wrapper-content pt-0 pb-0">
    <div class="page-wrapper d-flex flex-wrap flex-xl-nowrap">
		
        {include file="includes/sidebar.tpl"}
		
		<div class="page-container">
            <div class="container-fluid">
				<div class="page-content-wrapper d-flex flex-column">
					
					<h1 class="font-size-h4 mb-4 font-weight-normal">Watchlist</h1>
					
					{if $following}
					<div class="recent-view pt-2 pb-3">
						{assign var="rcompanies_title" value="Following"}
						{assign var="rcompanies" value=$following}

						{include file="includes/company/related_companies.tpl"}
					</div>
					{/if}
					
				</div>
			</div>
		</div>
	</div>
</div>