<div id="page-title" class="page-title pt-11 text-center">
	<div class="container">
		<div>
			<a href="{$config.site_url}" class="logo">
				<img src="{$config.views_url}images/logo.png" class="" />
			</a>

			<h3 class="mb-0 mt-3 letter-spacing-50">
				Sign in to your account
			</h3>
		</div>
	</div>
</div>
	
<div id="wrapper-content" class="wrapper-content pb-13 pt-8">
	<div class="container">

		<div >
			
			{if $auth_error}
			<div class="row">
				<div class="col-md-3 col-xs-1"></div>
				<div class="col-md-6 col-xs-10">
					<div class="alert alert-danger">
						<i class="fa-fw fa fa-warning"></i>
							Error: {$auth_error}
					</div>
				</div>
				<div class="col-md-3 col-xs-1"></div>
			</div>
			{/if}

			<div class="row">
			<div class="col-md-3 col-xs-1"></div>
			<div class="col-md-6 col-xs-10" style="padding: 15px 50px 40px 50px; border-radius: 4px; 
				 border-top: 2px solid #69C6C9; box-shadow: 0 2px 3px rgb(0 0 0 / 15%);">

				{include file="file:`$core_com`form.tpl" controller=$core_controllers.users 
						 form_name="sign_in_form" act="signIn"}

				<a href="/user/resetpass">Forgot your password?</a><br/>

				Don't have an account?<br/>
				<a href="/user/signup">Create one here</a>


			</div>
			<div class="col-md-3 col-xs-1"></div>
			</div>
				
		</div>
	</div>
</div>
