<div id="wrapper-content" class="panel wrapper-content pt-0 pb-0">
    <div class="page-wrapper d-flex flex-wrap flex-xl-nowrap">
		
        {include file="includes/sidebar.tpl"}
		
		<div class="page-container">
            <div class="container-fluid">
				<div class="page-content-wrapper d-flex flex-column">
					
					<h1 class="font-size-h4 mb-4 font-weight-normal">Company Profile</h1>
					
					{if $company}
					<div class="row">
						<div class="col-lg-8">
							
							{if $smarty.get.updated}
								<div class="alert alert-success">
									Company profile has been updated
								</div>
							{/if}
							
							{include file="file:`$core_com`form.tpl" controller=$core_controllers.companies 
									 form_name="company_logo_form" act="uploadLogo"}
							
							{include file="file:`$core_com`form.tpl" controller=$core_controllers.companies 
									 form_name="edit_form" act="edit"}
							
						</div>
					</div>
					{else}
					<div>
						You don't have any company assigned. Please <a href="/companies/submit">submit a request</a> 
						for edit or <a href="/companies/add">create new</a>.
					</div>
					{/if}
					
				</div>
			</div>
		</div>
	</div>
</div>
		
<script type="text/javascript" src="{$config.views_url}js/loc.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCHWl4_-yEVpSUePDMcYkc9KCRI1W-QE4g&libraries=places&callback=initAutocomplete" async defer></script>

