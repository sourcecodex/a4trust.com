<div id="page-title" class="page-title pt-11 text-center">
	<div class="container">
		<div>
			<a href="{$config.site_url}" class="logo">
				<img src="{$config.views_url}images/logo.png" class="" />
			</a>
			<h3 class="mb-0 mt-3 letter-spacing-50">
				Request a new password
			</h3>
		</div>
	</div>
</div>

<div id="wrapper-content" class="wrapper-content pb-13 pt-8">
	<div class="container">
		
		<div class="row">
		<div class="col-md-3 col-xs-1"></div>
		<div class="col-md-6 col-xs-10" style="padding: 15px 50px 40px 50px; border-radius: 4px; 
				 border-top: 2px solid #69C6C9; box-shadow: 0 2px 3px rgb(0 0 0 / 15%);">
		
			{if $smarty.get.confirm == 1}
				<h1>Please check your email inbox.</h1>
				We sent you an email with a link to reset your password.
				If you do not receive the email, please check your junk mail.
			{else}

			<span>A password reset message will be sent to your e-mail address.</span><br/>
			<br/>
			{include file="file:`$core_com`form.tpl" controller=$core_controllers.users 
					 form_name="reset_pass_form" act="resetPassword"}
			{/if}
			
		</div>
		<div class="col-md-3 col-xs-1"></div>
		</div>
			
	</div>
</div>

