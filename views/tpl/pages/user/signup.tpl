<div id="page-title" class="page-title pt-11 text-center">
	<div class="container">
		<div>
			<a href="{$config.site_url}" class="logo">
				<img src="{$config.views_url}images/logo.png" class="" />
			</a>
			<h3 class="mb-0 mt-3 letter-spacing-50">
				Set up your new account today.
			</h3>
		</div>
	</div>
</div>
			
<div id="wrapper-content" class="wrapper-content pb-13 pt-8">
	<div class="container">

		{if $auth_error}
		<div class="row">
			<div class="col-md-3 col-xs-1"></div>
			<div class="col-md-6 col-xs-10">
				<div class="alert alert-danger">
					<i class="fa-fw fa fa-warning"></i>
						Error: {$auth_error}
				</div>
			</div>
			<div class="col-md-3 col-xs-1"></div>
		</div>
		{/if}
		
		<div class="row">
		<div class="col-md-3 col-xs-1"></div>
		<div class="col-md-6 col-xs-10" style="padding: 15px 50px 40px 50px; border-radius: 4px; 
				 border-top: 2px solid #69C6C9; box-shadow: 0 2px 3px rgb(0 0 0 / 15%);">
		
			{if $smarty.get.confirm == 1}
				<h4>Please check your email inbox.</h4>
				We've sent your confirmation email to <b>{$smarty.get.email|escape}</b><br/>
				<br/>
				If you haven't received the email, check your junk/spam folder or
				<a href="#" onclick="resendConfirmation('{$smarty.get.email|escape}'); return false;">
					send the confirmation email again.
				</a>
				If it was sent to the wrong address, click back in your browser and enter a different one.<br/>
				<br/>
				Thank You!

				{include file="includes/conversion.tpl"}
			{else}

				{include file="file:`$core_com`form.tpl" controller=$core_controllers.users 
						 form_name="sign_up_form" act="signUp"}

			{/if}

			Already have an account? <br/>
			<a href="/user/login">Sign in here</a>
		
		</div>
		<div class="col-md-3 col-xs-1"></div>
		</div>
		
	</div>
</div>