<div id="wrapper-content" class="wrapper-content pb-13">
	<div class="container">
		<div class="page-container">
			<div class="page-content">
				<div class="mb-10">
					<div class="single-blog-top pt-9">
						<h2 class="text-center mb-3 letter-spacing-50">{$new.title}</h2>
						<ul class="list-inline d-flex align-items-center justify-content-center flex-wrap">
							<li class="list-inline-item mr-1">
								<span class="text-gray">{$new.created|date_format:"%B %e, %Y"} by</span>
							</li>
							<li class="list-inline-item">
								<a href="#" class="link-hover-dark-primary">Admin</a>
							</li>
						</ul>
					</div>
					<div class="mb-12">
						<img src="{$config.site_url}{$new.header_img}" alt="{$new.title}">
					</div>
					<div class="single-blog-content">
						{$new.body}
					</div>
				</div>
			</div>
		</div>
	</div>
</div>