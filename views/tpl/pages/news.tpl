<div id="page-title" class="page-title pt-11 text-center mt-5">
	<div class="container">
		<div>
			<h3 class="mb-0 letter-spacing-50">
				News
			</h3>
		</div>
	</div>
</div>

<div class="row mt-5">
	{foreach from=$news item="new"}
	<div class="col-md-6 col-lg-4 mb-6">
		<div class="card border-0">
			<a href="/news/info/{$new.id}" class="hover-scale">
				<img src="{$config.site_url}{$new.feature_img}" alt="{$new.title}" class="card-img-top image">
			</a>
			<div class="card-body px-0">
				<h5 class="card-title lh-13 letter-spacing-25">
					<a href="/news/info/{$new.id}" class="link-hover-dark-primary text-capitalize">
						{$new.title}
					</a>
				</h5>
				<ul class="list-inline">
					<li class="list-inline-item mr-0">
						<span class="text-gray">{$new.created|date_format:"%B %e, %Y"} by</span>
					</li>
					<li class="list-inline-item">
						<a href="#" class="link-hover-dark-primary">Admin</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	{/foreach}
</div>
