<div id="wrapper-content" class="wrapper-content pt-5 pb-13">
	<div class="container page-contact pt-12">
		
		<div class="form-contact">
			<div class="form-wrapper px-3 px-sm-12 pt-11 pb-12">
				<h3 class="mb-9 text-center">Get In Touch</h3>
				{if $smarty.get.sent == 1}
					Thank you!
				{else}
					{include file="file:`$core_com`form.tpl" controller=$core_controllers.mailer_emails form_name="contacts_form" act="contact"}
				{/if}
			</div>
		</div>
		
	</div>
</div>
