{if $core_user->isLogged}
	
<div id="wrapper-content" class="panel wrapper-content pt-0 pb-0">
    <div class="page-wrapper d-flex flex-wrap flex-xl-nowrap">
		
        {include file="includes/sidebar.tpl"}
		
		<div class="page-container">
            <div class="container-fluid">
				<div class="page-content-wrapper d-flex flex-column">
					
					<h1 class="font-size-h4 mb-4 font-weight-normal">Dashboard</h1>
					
					
				</div>
			</div>
		</div>
	</div>
</div>

{else}
	
	<div class="container">
	
		{*<div class="row" style="margin-top: 150px;">
			<div class="col-md-6">
				<span>Aim for Trust</span>
			</div>
			<div class="col-md-6">
			</div>
		</div>*}
		
		<div class="row mt-14 pl-8 pr-8">
			<div class="col-md-12">
				<div class="text-center pb-5">
					<h3 class="mb-0 font-weight-normal">
						Aim for Trust
					</h3>
				</div>
				<div class="slick-slider dots-inner">
				   <div class="box">
					   <img src="{$config.views_url}images/slide1.webp" alt="Slide1" />
				   </div>
				   <div class="box">
					   <img src="{$config.views_url}images/slide2.webp" alt="Slide2" />
				   </div>
				   <div class="box">
					   <img src="{$config.views_url}images/slide3.webp" alt="Slide3" />
				   </div>
				</div>
				<div class="text-center pb-10">
					<h3 class="mb-0 font-weight-normal">
						"Connecting exporting producers and dealers around the globe"<br/>
						Find reliable and financialy stable partners to increase export sales
					</h3>
				</div>
			</div>
		</div>
		<div class="row pb-10">
			<div class="col-md-4 pl-12 pr-12 text-center">
				<img src="{$config.views_url}images/counter1.webp" alt="Registered Dealers" /><br/>
				<br/>
				Registered Dealers<br/>
				1349
			</div>
			<div class="col-md-4 pl-12 pr-12 text-center">
				<img src="{$config.views_url}images/counter2.webp" alt="Registered Producers" /><br/>
				<br/>
				Registered Producers<br/>
				841
			</div>
			<div class="col-md-4 pl-12 pr-12 text-center">
				<img src="{$config.views_url}images/counter3.webp" alt="Debts recovered" /><br/>
				<br/>
				Debts recovered<br/>
				82
			</div>
		</div>
	</div>
	
	<section id="section-01" class="text-center pt-11 pb-13 bg-gray-02">
	<div class="container">
		<div class="mb-9">
			<h2 class="mb-0 font-weight-normal">
				How It Works
			</h2>
		</div>
		<div class="card-deck">
			<div class="card text-center image-box-center border-0 rounded-0 px-6 py-2 box-shadow-hover">
				<div class="image mb-3 p-8">
					<img src="{$config.views_url}images/how-it-works1.webp" alt="Database of Producers & Dealers">
				</div>
				<div class="card-body p-0">
					<div class="card-title mb-5 text-dark">
						Database of Producers & Dealers
					</div>
					<p class="card-text text-gray">
						Search the database, Analyse your sales strategy, Follow companies to get updates, Find partners for projects abroad.
					</p>
				</div>
			</div>
			<div class="card text-center image-box-center border-0 rounded-0 px-6 py-2 box-shadow-hover">
				<div class="image mb-3 p-8">
					<img src="{$config.views_url}images/how-it-works2.webp" alt="Ratings & More">
				</div>
				<div class="card-body p-0">
					<div class="card-title mb-5 text-dark">
						Ratings & More
					</div>
					<p class="card-text text-gray">
						Check the ratings of your existing or new partners, make decisions on payment terms depending on their reliability and rating score.
					</p>
				</div>
			</div>
			<div class="card text-center image-box-center border-0 rounded-0 px-6 py-2 box-shadow-hover">
				<div class="image mb-3 p-8">
					<img src="{$config.views_url}images/how-it-works3.webp" alt="Debt recovery">
				</div>
				<div class="card-body p-0">
					<div class="card-title mb-5 text-dark">
						Debt recovery
					</div>
					<p class="card-text text-gray">
						Use the leverage of our rating system to recover the debts. Submit the debt so other producers would be notified of company's low rating.
					</p>
				</div>
			</div>
		</div>
	</div>
	</section>
	{*			   
	<div class="home-main-how-it-work bg-white pt-11">
		<div class="container">
			<h2 class="mb-8">
				<span class="font-weight-light">Database of Producers & Dealers</span>
			</h2>
			<div class="row no-gutters pb-11">
				<div class="col-lg-4 mb-4 mb-lg-0 px-0 px-lg-4">
					<div class="media icon-box-style-02 fadeInDown animated" data-animate="fadeInDown">
						<div class="d-flex flex-column align-items-center mr-6">
							<svg class="icon icon-checkmark-circle">
								<use xlink:href="#icon-checkmark-circle"></use>
							</svg>
							<span class="number h1 font-weight-bold">1</span>
						</div>
						<div class="media-body lh-14">
							<h5 class="mb-3 lh-1">
								Search the database
							</h5>
							<p class="font-size-md text-gray mb-0 text-muted">
								Whether you are looking for new export partners or checking on the existing ones, 
								use the database to make the right decision for your sales strategy.
							</p>
						</div>
					</div>
				</div>
				<div class="col-lg-4 mb-4 mb-lg-0 px-0 px-lg-4">
					<div class="media icon-box-style-02 fadeInDown animated" data-animate="fadeInDown">
						<div class="d-flex flex-column align-items-center mr-6">
							<svg class="icon icon-checkmark-circle">
								<use xlink:href="#icon-checkmark-circle"></use>
							</svg>
							<span class="number h1 font-weight-bold">2</span>
						</div>
						<div class="media-body lh-14">
							<h5 class="mb-3 lh-1">
								Follow companies to get updates
							</h5>
							<p class="font-size-md text-gray mb-0 text-muted">
								Always be up to date - follow the companies you are interested in, 
								get notified if company looses rating.
							</p>
						</div>
					</div>
				</div>
				<div class="col-lg-4 mb-4 mb-lg-0 px-0 px-lg-4">
					<div class="media icon-box-style-02 fadeInDown animated" data-animate="fadeInDown">
						<div class="d-flex flex-column align-items-center mr-6">
							<svg class="icon icon-checkmark-circle">
								<use xlink:href="#icon-checkmark-circle"></use>
							</svg>
							<span class="number h1 font-weight-bold">3</span>
						</div>
						<div class="media-body lh-14">
							<h5 class="mb-3 lh-1">
								Corporate clients
							</h5>
							<p class="font-size-md text-gray mb-0 text-muted">
								Have multinational client - find the right partner to complete project abroad 
								by filtering location, products, sector or the same projects.
							</p>
						</div>
					</div>
				</div>
			</div>
			<div class="border-bottom"></div>
		</div>
	</div>
	*}
	{*<div class="home-main-how-it-work bg-white pt-11">
		<div class="container">
			<h2 class="mb-8">
				<span class="font-weight-light">Ratings & Debt recovery</span>
			</h2>
			<div class="row no-gutters pb-11">
				<div class="col-lg-4 mb-4 mb-lg-0 px-0 px-lg-4">
					<div class="media icon-box-style-02 fadeInDown animated" data-animate="fadeInDown">
						<div class="d-flex flex-column align-items-center mr-6">
							<svg class="icon icon-checkmark-circle">
								<use xlink:href="#icon-checkmark-circle"></use>
							</svg>
							<span class="number h1 font-weight-bold">1</span>
						</div>
						<div class="media-body lh-14">
							<h5 class="mb-3 lh-1">
								Check the ratings
							</h5>
							<p class="font-size-md text-gray mb-0 text-muted">
								We hold a database of unreliable partners with unsetteled debts and lower their rating, 
								so you could find reliable partners with good rating.
							</p>
						</div>
					</div>
				</div>
				<div class="col-lg-4 mb-4 mb-lg-0 px-0 px-lg-4">
					<div class="media icon-box-style-02 fadeInDown animated" data-animate="fadeInDown">
						<div class="d-flex flex-column align-items-center mr-6">
							<svg class="icon icon-checkmark-circle">
								<use xlink:href="#icon-checkmark-circle"></use>
							</svg>
							<span class="number h1 font-weight-bold">2</span>
						</div>
						<div class="media-body lh-14">
							<h5 class="mb-3 lh-1">
								Adapt your payment terms
							</h5>
							<p class="font-size-md text-gray mb-0 text-muted">
								Check the company's rating (active debts) before making a decision on the payment terms.
							</p>
						</div>
					</div>
				</div>
				<div class="col-lg-4 mb-4 mb-lg-0 px-0 px-lg-4">
					<div class="media icon-box-style-02 fadeInDown animated" data-animate="fadeInDown">
						<div class="d-flex flex-column align-items-center mr-6">
							<svg class="icon icon-checkmark-circle">
								<use xlink:href="#icon-checkmark-circle"></use>
							</svg>
							<span class="number h1 font-weight-bold">3</span>
						</div>
						<div class="media-body lh-14">
							<h5 class="mb-3 lh-1">
								Submit a debt
							</h5>
							<p class="font-size-md text-gray mb-0 text-muted">
								Have an unsettled debt? Submit to us and dealers rating will be adjusted 
								and all producers will be notified to adjust the payment terms.
							</p>
						</div>
					</div>
				</div>
			</div>
			<div class="border-bottom"></div>
		</div>
	</div>*}
	
	<section class="home-main-testimonial pt-12 pb-13" id="section-04">
		<div class="container">
			<h2 class="mb-8">
				<span class="font-weight-semibold">Clients </span>
				<span class="font-weight-light">Review</span>
			</h2>
			<div class="container">
				<div class="row">
					<div class="col col-md-12">
						<div class="slick-slider testimonials-slider arrow-top slick-initialized" data-slick-options="">
							<div class="slick-list draggable" style="height: 303.156px;"><div class="slick-track" style="opacity: 1; width: 2340px; transform: translate3d(0px, 0px, 0px);"><div class="slick-slide slick-current slick-active" data-slick-index="0" aria-hidden="false" style="width: 585px;"><div><div class="box" style="width: 100%; display: inline-block;">
								<div class="card testimonial h-100 border-0 bg-transparent">
									<a href="#" class="author-image" tabindex="0">
										<img src="{$config.views_url}images/listing/client-1.png" alt="Testimonial" class="rounded-circle">
									</a>
									<div class="card-body bg-white">
										<div class="testimonial-icon text-right">
											<svg class="icon icon-quote">
												<use xlink:href="#icon-quote"></use>
											</svg>
										</div>
										<ul class="list-inline mb-4 d-flex align-items-end flex-wrap">
											<li class="list-inline-item">
												<a href="#" class="font-size-lg text-dark font-weight-semibold d-inline-block" tabindex="0">Kanye
													Mark
												</a>
											</li>
											<li class="list-inline-item">
												<span class="h5 font-weight-light mb-0 d-inline-block ml-1 text-gray">/</span>
											</li>
											<li>
											<span class="text-gray">
												Office furniture producer
											</span>
											</li>
										</ul>
										<div class="card-text text-gray pr-4">
											We always check our partners rating and determine their payment terms based on if they have any active debts to other producers.
										</div>
									</div>
								</div>
							</div></div></div><div class="slick-slide slick-active" data-slick-index="1" aria-hidden="false" style="width: 585px;"><div><div class="box" style="width: 100%; display: inline-block;">
								<div class="card testimonial h-100 border-0 bg-transparent">
									<a href="#" class="author-image" tabindex="0">
										<img src="{$config.views_url}images/listing/client-2.png" alt="Testimonial" class="rounded-circle">
									</a>
									<div class="card-body bg-white">
										<div class="testimonial-icon text-right">
											<svg class="icon icon-quote">
												<use xlink:href="#icon-quote"></use>
											</svg>
										</div>
										<ul class="list-inline mb-4 d-flex align-items-end flex-wrap">
											<li class="list-inline-item">
												<a href="#" class="font-size-lg text-dark font-weight-semibold d-inline-block" tabindex="0">Anabella
													Olaf
												</a>
											</li>
											<li class="list-inline-item">
												<span class="h5 font-weight-light mb-0 d-inline-block ml-1 text-gray">/</span>
											</li>
											<li class="list-inline-item">
											<span class="text-gray">
												Office accessories producer
											</span>
											</li>

										</ul>
										<div class="card-text text-gray pr-4">
											The posibility to warn the dealer to submit their debt and lower the rating, has had a great impact on recovering the debt.
										</div>
									</div>
								</div>
							</div></div></div><div class="slick-slide" data-slick-index="2" aria-hidden="true" tabindex="-1" style="width: 585px;"><div><div class="box" style="width: 100%; display: inline-block;">
								<div class="card testimonial h-100 border-0 bg-transparent">
									<a href="#" class="author-image" tabindex="-1">
										<img src="{$config.views_url}images/listing/client-1.png" alt="Testimonial" class="rounded-circle">
									</a>
									<div class="card-body bg-white">
										<div class="testimonial-icon text-right">
											<svg class="icon icon-quote">
												<use xlink:href="#icon-quote"></use>
											</svg>
										</div>
										<ul class="list-inline mb-4 d-flex align-items-end flex-wrap">
											<li class="list-inline-item">
												<a href="#" class="font-size-lg text-dark font-weight-semibold d-inline-block" tabindex="-1">Kanye
													Gabriela
												</a>
											</li>
											<li class="list-inline-item">
												<span class="h5 font-weight-light mb-0 d-inline-block ml-1 text-gray">/</span>
											</li>
											<li>
											<span class="text-gray">
												Office furniture producer
											</span>
											</li>
										</ul>
										<div class="card-text text-gray pr-4">
											Of course we are all competitors in projects, but when it comes to detbs, we have to unite to have a greater impact. 
										</div>
									</div>
								</div>
							</div></div></div><div class="slick-slide" data-slick-index="3" aria-hidden="true" tabindex="-1" style="width: 585px;"><div><div class="box" style="width: 100%; display: inline-block;">
								<div class="card testimonial h-100 border-0 bg-transparent">
									<a href="#" class="author-image" tabindex="-1">
										<img src="{$config.views_url}images/listing/client-2.png" alt="Testimonial" class="rounded-circle">
									</a>
									<div class="card-body bg-white">
										<div class="testimonial-icon text-right">
											<svg class="icon icon-quote">
												<use xlink:href="#icon-quote"></use>
											</svg>
										</div>
										<ul class="list-inline mb-4 d-flex align-items-end flex-wrap">
											<li class="list-inline-item">
												<a href="#" class="font-size-lg text-dark font-weight-semibold d-inline-block" tabindex="-1">Anabella
													Johnatan
												</a>
											</li>
											<li class="list-inline-item">
												<span class="h5 font-weight-light mb-0 d-inline-block ml-1 text-gray">/</span>
											</li>
											<li class="list-inline-item">
											<span class="text-gray">
												Carpet producer
											</span>
											</li>

										</ul>
										<div class="card-text text-gray pr-4">
											Thanks to A4Trust, we found trusted partners in new rexport regions
										</div>
									</div>
								</div>
							</div></div></div></div></div><div class="slick-arrows"><div class="slick-prev slick-arrow slick-disabled" aria-label="Previous" aria-disabled="true" style=""><i class="fas fa-chevron-left"></i></div><div class="slick-next slick-arrow" aria-label="Next" style="" aria-disabled="false"><i class="fas fa-chevron-right"></i></div></div></div>
					</div>
				</div>
			</div>
		</div>
	</section>
	
{/if}