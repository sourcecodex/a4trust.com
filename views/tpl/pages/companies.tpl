<div id="wrapper-content" class="wrapper-content bg-gray-04 pb-0">
<div class="container">
	<div class="page-container row">
		<div class="sidebar col-12 col-lg-4 order-1 order-lg-0">

			{include file="includes/companies/filter.tpl"}

		</div>

		<div class="page-content col-12 col-lg-8">
			
			{include file="includes/companies/sort.tpl"}
			
			<div class="store-listing-style store-listing-style-02">

				{foreach from=$companies item='company'}
				<div class="mb-6">
					<div class="store media align-items-stretch bg-white job-store">
						<a href="/companies/info/{$company.id}" style="width: 150px;">
							{if $company.logo}
								<img src="{$config.site_url}{$company.logo}" alt="logo" 
									 style="max-width: 140px; max-height: 100px;" />
							{else}
								<img src="{$config.views_url}images/no-company.webp" alt="logo" 
									 style="max-width: 140px; max-height: 100px;" />
							{/if}
						</a>
						<div class="media-body px-0 px-md-4 pt-4 pt-lg-0">
							
							<div class="row">
								<div class="col-lg-6">
									<a href="/companies/info/{$company.id}" class="h5 text-dark d-inline-block store-name">
										<span class="letter-spacing-25">{$company.name}</span>
									</a>
								</div>
								<div class="col-lg-6 text-right">
									Rating: {$company.rating}
								</div>
							</div>
							<div class="row mb-2">
								<div class="col-lg-12">
									<i class="fal fa-map-marker-alt" title="Address"></i>
									<span class="d-inline-block ml-2">{$company.address}</span><br/>
									<i class="fal fa-sack-dollar" title="Revenue"></i>
									<span class="d-inline-block ml-2 text-danger">{$company.revenue|number_format} EUR</span>
								</div>
							</div>
							<div class="row mb-2">
								<div class="col-lg-12">
									{if $company.type == 1}
										<b>Sectors</b>:
										{foreach from=$company.sector item='sector'}
											{$sectors[$sector]},
										{/foreach}
									{else}
										<b>Products</b>:
										{foreach from=$company.production item='product'}
											{$products[$product]},
										{/foreach}
									{/if}
									<br/>
									
									{if $company.type == 1}
										<b>Producers</b>: {', '|implode:$company.producers}
									{/if}
									
								</div>
							</div>
							{*<div class="border-top pt-2 px-0 pb-0">
								{$company.description}
							</div>*}
						</div>
					</div>
				</div>
				{/foreach}

			</div>
			
			<div class="mb-6">
				{include file="components/pagination.tpl"}
			</div>
		</div>
	</div>
</div>      
</div>