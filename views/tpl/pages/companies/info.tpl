<div id="wrapper-content" class="wrapper-content pt-8 pb-0">
	<div class="container">
		
		<div class="page-title pb-5 mb-5 border-bottom">

			<div class="explore-details-top d-flex flex-wrap flex-lg-nowrap bg-white">
				<div class="d-flex align-items-stretch">
					<div class="mr-6">
						{if $company.logo}
							<img src="{$config.site_url}{$company.logo}" alt="logo" 
								 style="max-width: 200px; max-height: 150px;" />
						{else}
							<img src="{$config.views_url}images/no-company.webp" alt="logo" />
						{/if}
					</div>
					<div class="d-flex flex-column">
						<div class="d-flex flex-wrap d-inline-flex ">	
						
							<h2 class="text-dark mr-3 mb-2">
								{$company.name}
							</h2>
							<span class="check font-weight-semibold text-green mr-3 mb-2">
								Verified
							</span>
						
						</div>
						<ul class="list-inline store-meta d-flex flex-wrap align-items-center">
							<li class="list-inline-item">
								<span>{$company.rating} rating</span>
							</li>
							<li class="list-inline-item separate"></li>
							<li class="list-inline-item">
								<span>{if $company.type == 1}Dealer{else}Producer{/if}</span>
							</li>
							<li class="list-inline-item separate"></li>
							<li class="list-inline-item">
								<a href="{$company.website}" rel="nofollow" target="_blank"
								   class="text-link text-decoration-none d-flex align-items-center">
									{$company.website}
								</a>
							</li>
						</ul>
					</div>
					
				</div>
				<div class="ml-0 ml-lg-auto mt-4 mt-lg-0 d-flex flex-wrap flex-sm-nowrap">
					<ul class="list-inline mb-0">
						<li class="list-inline-item">
							<a href="#" onclick="xajax_createPopup('report'); return false;"
							   class="btn btn-white font-size-md mb-3 mb-sm-0 py-1 px-2 rounded-sm">
								<span class="d-inline-block mr-1">
									<i class="fal fa-exclamation-triangle"></i>
								</span>
								Report
							</a>
						</li>
						<li class="list-inline-item">
							<a href="#" onclick="xajax_followCompany('{$company.id}'); return false;"
							   class="btn btn-white font-size-md mb-3 mb-sm-0 py-1 px-4 rounded-sm">
								<span class="d-inline-block mr-1">
									<i id="follow_icon" class="{if $following}fas{else}fal{/if} fa-bookmark"></i>
								</span>
								Follow
							</a>
						</li>
					</ul>
				</div>
			</div>
		</div>

		{if $producers}
		<div class="recent-view pt-2 pb-3">
			{assign var="rcompanies_title" value="Producers"}
			{assign var="rcompanies" value=$producers}
			
			{include file="includes/company/related_companies.tpl"}
		</div>
		{/if}
								
		<div class="page-container row bg-white pt-3">
			<div class="page-content col-xl-8 mb-8 mb-xl-0 pt-1">
				<div class="explore-details-container">
					<div class="mb-9">
						
						<h6 class="font-size-md mb-4">Revenue</h6>
						<div class="mb-4">
							<p>
								<span class="text-danger">{$company.revenue|number_format} EUR</span>
							</p>
						</div>
						
						<h6 class="font-size-md mb-4">Description</h6>
						<div class="mb-4">
							<p>
								{$company.description_long}
							</p>
						</div>
						
						{include file="includes/company/projects.tpl"}
						
						{if $company.type == 1}
							{include file="includes/companies/sectors.tpl"}
						{else}
							{include file="includes/companies/products.tpl"}
						{/if}
						
					</div>

				</div>
			</div>

			<div class="sidebar col-12 col-xl-4 pt-1">
				
				{include file="includes/company/addresses.tpl"}

			</div>
		</div>

		{if $related_companies}
		<div class="recent-view pt-2 pb-3">
			{assign var="rcompanies_title" value="Related companies"}
			{assign var="rcompanies" value=$related_companies}
			
			{include file="includes/company/related_companies.tpl"}
		</div>
		{/if}
		
	</div>
</div>