<div id="wrapper-content" class="panel wrapper-content pt-0 pb-0">
    <div class="page-wrapper d-flex flex-wrap flex-xl-nowrap">
		
        {include file="includes/sidebar.tpl"}
		
		<div class="page-container">
            <div class="container-fluid">
				<div class="page-content-wrapper d-flex flex-column">
					
					<h1 class="font-size-h4 mb-4 font-weight-normal">Request for edit</h1>
					
					<div class="row">
						<div class="col-lg-8">
							
							{if $smarty.get.sent}
								<div class="alert alert-success">
									Request for edit has been sent
								</div>
							{else}
								{include file="file:`$core_com`form.tpl" controller=$core_controllers.companies 
										 form_name="request_for_edit_form" act="requestForEdit"}
							{/if}
						</div>
					</div>
					
				</div>
			</div>
		</div>
	</div>
</div>