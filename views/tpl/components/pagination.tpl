{strip}
{assign var="page_nr" value=$smarty.get.page}
{assign var="page" value=$page_info.pages[$page_nr]}

<div class="row">
	{if $page_info.result_count > 0}
	
	<div class="col-lg-6">
		<nav aria-label="Pagination">
		<ul class="pagination">

		{if $page_info.pages_count > 0}

			<li class="page-item {if $page_nr <= 1}disabled{/if}">
				{if $page_nr <= 1}<a class="page-link" href="#">&laquo;</a>
				{else}<a class="page-link" href="{$core_pages->url}?page={$page_nr-1}">&laquo;</a>{/if}
			</li>

			{if $page_nr < 10}
				{if $page_info.pages_count < 10}
					{section name=pages start=1 loop=$page_info.pages_count+1 step=1}
					{assign var="page_id" value=$smarty.section.pages.index}
					<li class="page-item {if $page_id == $page_nr}active{/if}">
						<a class="page-link" href="{$core_pages->url}?page={$page_id}">{$page_id}</a>
					</li>
					{/section}
				{else}
					{section name=pages start=1 loop=11 step=1}
					{assign var="page_id" value=$smarty.section.pages.index}
					<li class="page-item {if $page_id == $page_nr}active{/if}">
						<a class="page-link" href="{$core_pages->url}?page={$page_id}">{$page_id}</a>
					</li>
					{/section}
				{/if}
			{else}
				{if $page_nr+5 < $page_info.pages_count}
					{section name=pages start=$page_nr-5 loop=$page_nr+5 step=1}
					{assign var="page_id" value=$smarty.section.pages.index}
					<li class="page-item {if $page_id == $page_nr}active{/if}">
						<a class="page-link" href="{$core_pages->url}?page={$page_id}">{$page_id}</a>
					</li>
					{/section}
				{else}
					{section name=pages start=$page_info.pages_count-10 loop=$page_info.pages_count+1 step=1}
					{assign var="page_id" value=$smarty.section.pages.index}
					<li class="page-item {if $page_id == $page_nr}active{/if}">
						<a class="page-link" href="{$core_pages->url}?page={$page_id}">{$page_id}</a>
					</li>
					{/section}
				{/if}
			{/if}

			<li class="page-item {if $page_nr >= $page_info.pages_count}disabled{/if}">
				{if $page_nr >= $page_info.pages_count}<a class="page-link" href="#">&raquo;</a>
				{else}<a class="page-link" href="{$core_pages->url}?page={$page_nr+1}">&raquo;</a>{/if}
			</li>

		{/if}

		</ul>
		</nav>
	</div>
		
	<div class="col-lg-6" style="text-align: right;">
		<span>Showing <b>{$page.from}</b> to <b>{$page.to}</b> of <b>{$page_info.result_count}</b> entries</span>
	</div>

	{/if}
</div>
{/strip}