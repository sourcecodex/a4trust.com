<div id="addresses_row{$row_id}">
	<div class="input-group">
		<input type="text" 
			id="addresses_name{$row_id}" name="addresses_name{$row_id}" value="{$address.name}"
			class="form-control"
			placeholder="Address name {$row_id+1}" />

		<input type="text" 
			id="addresses_address{$row_id}" name="addresses_address{$row_id}" value="{$address.address}"
			class="form-control google-address" autocomplete="off" 
			placeholder="Address {$row_id+1}" />
	</div>
</div>