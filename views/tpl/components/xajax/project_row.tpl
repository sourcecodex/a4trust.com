<div id="projects_row{$row_id}">
	<div class="input-group">
		<input type="text" 
			id="projects_name{$row_id}" name="projects_name{$row_id}" value="{$project.name}"
			class="form-control"
			placeholder="Project name {$row_id+1}" />

		<input type="text" 
			id="projects_url{$row_id}" name="projects_url{$row_id}" value="{$project.url}"
			class="form-control" autocomplete="off" 
			placeholder="Project URL {$row_id+1}" />
	</div>
</div>