<div class="explore-filter d-lg-flex align-items-center d-block">
	<div class="text-dark font-weight-semibold font-size-md mb-4 mb-lg-0">
		{$companies|@count} Results found
	</div>
	<div class="form-filter d-flex align-items-center ml-auto">
		<div class="form-group row no-gutters align-items-center">
			<label for="sort-by" class="col-sm-3 text-dark font-size-md font-weight-semibold mb-0">
				Sort by
			</label>
			<div class="select-custom col-sm-9 bg-white">
				
				{foreach from=$page_info.order key="order_by" item="order_type"}
					{assign var="sort_by" value=$order_by}
					{assign var="sort_type" value=$order_type}
				{/foreach}
				
				<select id="sort-by" class="form-control bg-transparent" 
						onchange="window.location='{$core_pages->url}?order_by'+this.value;">
					
					<option value="[created]=desc" {if $sort_by == 'created'}selected{/if}>Latest</option>
					<option value="[name]=asc" {if $sort_by == 'name'}selected{/if}>A-Z</option>
					<option value="[revenue]=desc" {if $sort_by == 'revenue'}selected{/if}>Revenue</option>
					<option value="[rating]=desc" {if $sort_by == 'rating'}selected{/if}>Rating</option>
					{*<option value="4">Project count</option>*}
				</select>
			</div>
		</div>
	</div>
</div>