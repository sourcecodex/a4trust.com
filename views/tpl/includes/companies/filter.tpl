<div class="card widget-filter bg-white mb-6 border-0 rounded-0 px-6">
	<div class="card-header bg-transparent border-0 pt-4 pb-0 px-0">
		<h5 class="card-title mb-0">Filter</h5>
	</div>
	<div class="card-body px-0">
		<div class="form-filter">
			<form name="search_form" action="{$core_pages->url}" method="post">
				<div class="form-group category">
					<div class="select-custom">
						<select name="type" class="form-control" onchange="this.form.submit();">
							<option value="">- Company type -</option>
							<option value="1" {if $page_info.filter.type == 1}selected{/if}>Dealer</option>
							<option value="2" {if $page_info.filter.type == 2}selected{/if}>Producer</option>
						</select>
					</div>
				</div>
				
				<div class="form-group">
					<div class="input-group">
						<input type="text" name="address" placeholder="Location" class="form-control bg-transparent"
							   value="{$page_info.filter.address|escape}" />
						<span class="input-group-append">
							<i class="fal fa-map-marker-alt"></i>
						</span>
					</div>
				</div>
				
				<div class="form-group">
					<div class="input-group">
						<input type="text" name="search_query" placeholder="Keyword" class="form-control bg-transparent"
							   value="{$page_info.filter.search_query|escape}" />
						<span class="input-group-append">
							<i class="fal fa-key"></i>
						</span>
					</div>
				</div>
				
				{if $page_info.filter.type == 1}
				<div class="form-group category">
					<div class="select-custom">
						<select name="sector" class="form-control" onchange="this.form.submit();">
							<option value="">- Sector -</option>
							{foreach from=$sectors key='key' item='sector'}
								<option value="{$key}" {if $page_info.filter.sector == $key}selected{/if}>
									{$sector}
								</option>
							{/foreach}
						</select>
					</div>
				</div>
				{/if}
				
				{if $page_info.filter.type == 2}
				<div class="form-group category">
					<div class="select-custom">
						<select name="production" class="form-control" onchange="this.form.submit();">
							<option value="">- Products -</option>
							{foreach from=$products key='key' item='product'}
								<option value="{$key}" {if $page_info.filter.production == $key}selected{/if}>
									{$product}
								</option>
							{/foreach}
						</select>
					</div>
				</div>
				{/if}
				
				<div class="form-group category">
					<div class="input-group">
						<button class="btn btn-light" type="submit">
							Search <span class="fal fa-search"></span>
						</button>
					</div>
				</div>
				
				{*<div class="form-group filter-tags">
					<label class="form-label">Filter by tags</label>
					<div class="custom-control custom-checkbox lh-19">
						<input class="custom-control-input" type="checkbox" id="electronics">
						<label class="custom-control-label" for="electronics">
							Design
						</label>

					</div>
					<div class="custom-control custom-checkbox lh-19">
						<input class="custom-control-input" type="checkbox" id="books">
						<label class="custom-control-label" for="books">
							Developer
						</label>

					</div>
					<div class="custom-control custom-checkbox lh-19">
						<input class="custom-control-input" type="checkbox" id="fashion">
						<label class="custom-control-label" for="fashion">
							Programer
						</label>

					</div>
					<div class="custom-control custom-checkbox lh-19">
						<input class="custom-control-input" type="checkbox" id="vintage">
						<label class="custom-control-label" for="vintage">
							Consulting
						</label>

					</div>
					<div class="custom-control custom-checkbox lh-19">
						<input class="custom-control-input" type="checkbox" id="gift">
						<label class="custom-control-label" for="gift">
							Accounting
						</label>

					</div>
					<div class="custom-control custom-checkbox lh-19">
						<input class="custom-control-input" type="checkbox" id="furniture">
						<label class="custom-control-label" for="furniture">
							Transporter
						</label>

					</div>
					<div class="custom-control custom-checkbox lh-19">
						<input class="custom-control-input" type="checkbox" id="women-clothing">
						<label class="custom-control-label" for="women-clothing">
							Technology
						</label>

					</div>
					<div class="custom-control custom-checkbox lh-19">
						<input class="custom-control-input" type="checkbox" id="men-clothing">
						<label class="custom-control-label" for="men-clothing">
							Waiter
						</label>

					</div>
					<div class="custom-control custom-checkbox lh-19">
						<input class="custom-control-input" type="checkbox" id="shoes">
						<label class="custom-control-label" for="shoes">
							Intership
						</label>

					</div>
					<div class="custom-control custom-checkbox lh-19">
						<input class="custom-control-input" type="checkbox" id="jewelry">
						<label class="custom-control-label" for="jewelry">
							Controllers
						</label>

					</div>
				</div>*}
			</form>
		</div>
	</div>
</div>
