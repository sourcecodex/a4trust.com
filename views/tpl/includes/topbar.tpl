{if $core_user->isLogged}
	
<header id="header" class="main-header header-sticky header-sticky-smart header-style-10 text-uppercase bg-white">
	<div class="header-wrapper sticky-area border-bottom">
		<div class="container-fluid">
			<nav class="navbar navbar-expand-xl">
				<div class="header-mobile d-flex d-xl-none flex-fill justify-content-between align-items-center">
					<div class="navbar-toggler toggle-icon" data-toggle="collapse"
						 data-target="#navbar-main-menu">
						<span></span>
					</div>
					<a class="navbar-brand navbar-brand-mobile" href="/">
						<img src="{$config.views_url}images/logo.png" alt="{$config.project_name}">
					</a>
				</div>
					
				<div class="collapse navbar-collapse" id="navbar-main-menu">
					<a class="navbar-brand d-none d-xl-block" href="/">
						<img src="{$config.views_url}images/logo.png" alt="{$config.project_name}">
					</a>
					
					<ul class="navbar-nav">
						{if $company_profile.id}
						<li class="nav-item {if $core_pages->path[1] == 'company_profile'}active{/if}">
							<a href="/user/company_profile" class="nav-link">
								<i class="fa fa-edit"></i>&nbsp; Your company
							</a>
						</li>
						{else}
						<li class="nav-item {if $core_pages->path[1] == 'company_profile'}active{/if}">
							<a href="/user/company_profile" class="nav-link">
								<i class="fa fa-plus"></i>&nbsp; Add company
							</a>
						</li>
						{/if}
						<li class="nav-item {if $core_pages->path[0] == 'companies'}active{/if}">
							<a href="/companies" class="nav-link">
								<i class="fa fa-building"></i>&nbsp; Companies
							</a>
						</li>
						<li class="nav-item" style="width: 550px;">
							&nbsp;
						</li>
						<li class="nav-item">
							<a href="#" onclick="signOut(); return false;" class="nav-link">
								Log out &nbsp;<i class="fa fa-sign-out"></i>
							</a>
						</li>
					</ul>
				</div>
			</nav>
		</div>
	</div>
</header>

{else}
				
<header id="header" class="main-header header-sticky header-sticky-smart header-style-01 header-float text-uppercase">
    <div class="header-wrapper sticky-area">
        <div class="container">
            <nav class="navbar navbar-expand-xl">
                <div class="header-mobile d-flex d-xl-none flex-fill justify-content-between align-items-center">
                    <div class="navbar-toggler toggle-icon" data-toggle="collapse" data-target="#navbar-main-menu">
                        <span></span>
                    </div>
                    <a class="navbar-brand navbar-brand-mobile" href="/">
                        <img src="{$config.views_url}images/logo.png" alt="{$config.project_name}">
                    </a>
                </div>
                <div class="collapse navbar-collapse" id="navbar-main-menu">
                    <a class="navbar-brand d-none d-xl-block mr-auto" href="/">
                        <img src="{$config.views_url}images/logo.png" alt="{$config.project_name}">
                    </a>

                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a class="nav-link" href="/">Home</a>
                        </li>
						<li class="nav-item">
                            <a class="nav-link" href="/solutions">Solutions</a>
                        </li>
						<li class="nav-item">
                            <a class="nav-link" href="/news">News</a>
                        </li>
                    </ul>
                    <div class="header-customize justify-content-end align-items-center d-none d-xl-flex">
                        <div class="header-customize-item">
                            <a href="/user/login" class="link" data-gtf-mfp="true">Log in</a>
                        </div>
                        <div class="header-customize-item button">
                            <a href="/user/signup" class="btn btn-primary btn-sm btn-icon-right">
								Sign up
                                <i class="far fa-angle-right"></i>
							</a>
                        </div>
                    </div>
                </div>
            </nav>
        </div>
    </div>
</header>
{/if}