{if $core_user->isLogged}
{else}
<footer id="section-02" class="main-footer main-footer-style-01 bg-gray-04 pt-12 pb-8">
	<div class="footer-second">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-lg-4 mb-6 mb-lg-0">
					<div class="mb-8"><img src="{$config.views_url}images/logo.png" alt="{$config.project_name}"></div>
					<div class="mb-7">
						{*<div class="font-size-md font-weight-semibold text-dark mb-4">Global Headquaters
						</div>
						<p class="mb-0">
							90 Fifth Avenue, 3rd Floor<br>
							New York NY 10011<br>
							212.913.9058</p>*}
					</div>
				</div>
				<div class="col-md-6 col-lg mb-6 mb-lg-0">
					<div class="font-size-md font-weight-semibold text-dark mb-4">
						Company
					</div>
					<ul class="list-group list-group-flush list-group-borderless">
						<li class="list-group-item px-0 lh-1625 bg-transparent py-1">
							<a href="/contact_us" class="link-hover-secondary-primary">Contact us</a>
						</li>
						<li class="list-group-item px-0 lh-1625 bg-transparent py-1">
							<a href="/solutions" class="link-hover-secondary-primary">Solutions</a>
						</li>
						<li class="list-group-item px-0 lh-1625 bg-transparent py-1">
							<a href="/news" class="link-hover-secondary-primary">News</a>
						</li>
					</ul>
				</div>
				<div class="col-md-6 col-lg mb-6 mb-lg-0">
					<div class="font-size-md font-weight-semibold text-dark mb-4">
						Quick Links
					</div>
					<ul class="list-group list-group-flush list-group-borderless">
						<li class="list-group-item px-0 lh-1625 bg-transparent py-1">
							<a href="/user/login" class="link-hover-secondary-primary">Log in</a>
						</li>
						<li class="list-group-item px-0 lh-1625 bg-transparent py-1">
							<a href="/user/signup" class="link-hover-secondary-primary">Sign up</a>
						</li>
					</ul>
				</div>
				<div class="col-md-6 col-lg-4 mb-6 mb-lg-0">
					<div class="pl-0 pl-lg-9">
						<div class="font-size-md font-weight-semibold text-dark mb-4">Our Newsletter</div>
						<div class="mb-4">Subscribe to our newsletter and<br>
							we will inform you about newset directory and promotions
						</div>
						<div class="form-newsletter">
							<form>
								<div class="input-group bg-white">
									<input type="text" class="form-control border-0" placeholder="Email Address">
									<button type="button" class="input-group-append btn btn-white bg-transparent text-dark border-0">
										<i class="fas fa-arrow-right"></i>
									</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="footer-last mt-8 mt-md-11">
		<div class="container">
			<div class="footer-last-container position-relative">
				<div class="row align-items-center">
					<div class="col-lg-4 mb-3 mb-lg-0">
						<div class="social-icon text-dark">
							<ul class="list-inline">
								<li class="list-inline-item mr-5">
									<a target="_blank" title="Twitter" href="#">
										<i class="fab fa-twitter">
										</i>
										<span>Twitter</span>
									</a>
								</li>
								<li class="list-inline-item mr-5">
									<a target="_blank" title="Facebook" href="#">
										<i class="fab fa-facebook-f">
										</i>
										<span>Facebook</span>
									</a>
								</li>
								<li class="list-inline-item mr-5">
									<a target="_blank" title="Instagram" href="#">
										<svg class="icon icon-instagram">
											<use xlink:href="#icon-instagram"></use>
										</svg>
										<span>Instagram</span>
									</a>
								</li>
							</ul>
						</div>
					</div>
					<div class="col-lg-5 mb-3 mb-lg-0">
						<div>
							© 2021 <a href="/" class="link-hover-dark-primary font-weight-semibold">{$config.project_name}</a>
							All Rights Resevered.
						</div>
					</div>
					<div class="back-top text-left text-lg-right">
						<a href="#" class="gtf-back-to-top link-hover-secondary-primary in"><i class="fal fa-arrow-up"></i>
							<span>Back To Top</span>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</footer>
{/if}