<div class="sidebar">
	<ul class="nav nav-pills nav-stacked">
		{if $core_pages->path[0] == "settings"}
		<li {if $core_pages->path[1] == ""}class="active"{/if}>
			<a href="{$config.site_url}settings"><i class="fa fa-cog" style="width: 20px;"></i> Your account</a>
		</li>
		<li {if $core_pages->path[1] == "change_password"}class="active"{/if}>
			<a href="{$config.site_url}settings/change_password"><i class="fa fa-book" style="width: 20px;"></i> Change password</a>
		</li>
		<li {if $core_pages->path[1] == "cancel_account"}class="active"{/if}>
			<a href="{$config.site_url}settings/cancel_account"><i class="fa fa-times" style="width: 20px;"></i> Cancel account</a>
		</li>
		{/if}
	</ul>
</div>