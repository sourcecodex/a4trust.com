<div class="sidebar">
<div class="container-fluid">
	<div class="user-profile media align-items-center mb-6">
		<div class="image mr-3"><img src="{$config.views_url}images/other/account-campaign.png" alt="User image"
									 class="rounded-circle"></div>
		<div class="media-body lh-14">
			<span class="text-dark d-block font-size-md">Howdy,</span>
			<span class="mb-0 h5">{$core_user->userData.first_name}</span>
		</div>
	</div>
	<ul class="list-group list-group-flush list-group-borderless sidebar-menu">
		<li class="list-group-item p-0 mb-2 lh-15 {if $core_pages->path[0] == ''}active{/if}">
			<a href="/"
			   class="d-flex align-items-center link-hover-dark-primary font-size-md">
				<span class="d-inline-block mr-3"><i class="fal fa-cog"></i></span>
				<span>Dashboard</span>
			</a>
		</li>
		<li class="list-group-item p-0 mb-2 lh-15 {if $core_pages->path[1] == 'settings'}active{/if}">
			<a href="/user/settings"
			   class="d-flex align-items-center link-hover-dark-primary font-size-md">
				<span class="d-inline-block mr-3"><i class="fal fa-user"></i></span>
				<span>My Profile</span>
			</a>
		</li>
		<li class="list-group-item p-0 mb-2 lh-15 {if $core_pages->path[1] == 'company_profile'}active{/if}">
			<a href="/user/company_profile"
			   class="d-flex align-items-center link-hover-dark-primary font-size-md">
				<span class="d-inline-block mr-3"><i class="fal fa-building"></i></span>
				<span>Company Profile</span>
			</a>
		</li>
		<li class="list-group-item p-0 mb-2 lh-15 {if $core_pages->path[1] == 'following'}active{/if}">
			<a href="/user/following"
			   class="d-flex align-items-center link-hover-dark-primary font-size-md">
				<span class="d-inline-block mr-3"><i class="fal fa-bookmark"></i></span>
				<span>Following</span>
			</a>
		</li>
		<li class="list-group-item p-0 mb-2 lh-15">
			<a href="#" onclick="signOut(); return false;" 
			   class="d-flex align-items-center link-hover-dark-primary font-size-md">
				<span class="d-inline-block mr-3"><i class="fal fa-sign-out"></i></span>
				<span>Logout</span>
			</a>
		</li>
	</ul>
</div>
</div>