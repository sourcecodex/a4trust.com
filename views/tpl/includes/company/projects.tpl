<h6 class="font-size-md mb-4">
	Projects
</h6>
<div class="row mb-4">
	<div class="col-md-12">
		{foreach from=$projects item="project"}
			<a href="{$project.url}" target="_blank">{$project.name}</a>, 
		{/foreach}
	</div>
</div>