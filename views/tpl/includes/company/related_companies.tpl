<div class="mb-6">
	<h5 class="mb-0">
		{$rcompanies_title}
	</h5>
</div>
<div class="row">

	{foreach from=$rcompanies item="rc"}
	<div class="col-lg-4 mb-4 mb-lg-0">
		<div class="store media align-items-stretch bg-white">
			<div class="store-image position-relative">
				<a href="/companies/info/{$rc.id}">
					{if $rc.logo}
						<img src="{$config.site_url}{$rc.logo}" alt="logo" />
					{else}
						<img src="{$config.views_url}images/shop/jobs-shop-1.jpg" alt="logo" />
					{/if}
				</a>
			</div>
			<div class="media-body pl-0 pl-sm-3 pt-4 pt-sm-0">
				<a href="/companies/info/{$rc.id}" class="font-size-md font-weight-semibold text-dark d-inline-block mb-2 lh-11">
					<span class="letter-spacing-25">{$rc.name}</span> 
				</a>
				<ul class="list-inline store-meta mb-2 lh-1 font-size-sm d-flex align-items-center flex-wrap">
					<li class="list-inline-item">
						<span class="number">{$rc.rating} rating</span>
					</li>
					<li class="list-inline-item separate"></li>
					<li class="list-inline-item">
						<span>{if $rc.type == 1}Dealer{else}Producer{/if}</span>
					</li>
				</ul>
				<div>
					<span class="d-inline-block mr-1">
						<i class="fal fa-map-marker-alt"></i>
					</span>
					<span>{$rc.address}</span>
				</div>
			</div>
		</div>
	</div>
	{/foreach}
	
</div>
