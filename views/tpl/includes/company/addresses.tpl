<div class="card p-4 widget border-0 bg-gray-06 mb-6 rounded-0">
	<div class="card-title d-flex mb-0 font-size-md font-weight-semibold text-dark text-uppercase border-bottom pb-2 lh-1">
		<span>Headquarters</span>
	</div>

	<div class="card-body px-0 pb-0 py-2">
		<ul class="list-group list-group-flush">
			<li class="list-group-item bg-transparent d-flex text-dark px-0">
				<span class="item-icon mr-3"><i class="fal fa-map-marker-alt"></i></span>
				<span class="card-text">{$company.address}</span>
			</li>

		</ul>

	</div>
</div>

<div class="card p-4 widget border-0 bg-gray-06 mb-6 rounded-0">
	<div class="card-title d-flex mb-0 font-size-md font-weight-semibold text-dark text-uppercase border-bottom pb-2 lh-1">
		<span>Other addresses</span>
	</div>

	<div class="card-body px-0 pb-0 py-2">
		<ul class="list-group list-group-flush">
			{foreach from=$addresses item="address"}
			<li class="list-group-item bg-transparent d-flex text-dark px-0">
				<b>{$address.name}</b>&nbsp;&nbsp;&nbsp;
				<span class="item-icon mr-3"><i class="fal fa-map-marker-alt"></i></span>
				<span class="card-text">{$address.address}</span>
			</li>
			{/foreach}
		</ul>
	</div>
</div>