<div style="background-color: #ececec; padding: 15px 25px 100px 25px;">
<center>
	<table width="700" border="0" cellspacing="0" style="background-color: #fff;">
	<tr>
	<td style="text-align: left; font-family: Arial; color: #333; padding: 25px; border: 1px solid #dcdcdc; border-top: 2px solid #2985CC;">
		To start using your {$config.project_name} account, click the button to confirm your email address:<br/>
		<br/>
		<a href="{$config.site_url}?act=confirm_email&lang={$lang}&u={$user_id}&c={$code}&h={$hash}&id=[id_hash]" target="_blank"
		   style="display: block; width: 150px; height: 40px; line-height: 40px; text-align: center; background: #4D90FE; color: white; text-decoration: none; font-weight: bold; border: 1px solid grey; border-radius: 5px;">
		Confirm Email
		</a>
		<br/>
		Sincerely,<br/>
		{$config.project_name} Team<br/>
		<br/>
		<span style="font-size: 11px;">
		Trouble with the button above? Copy and paste this link into your browser:<br/>
		{$config.site_url}?act=confirm_email&lang={$lang}&u={$user_id}&c={$code}&h={$hash}&id=[id_hash]
		</span>
	</td>
	</tr>
	</table>
</center>
</div>