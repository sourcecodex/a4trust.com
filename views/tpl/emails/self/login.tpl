<div style="font-family: Arial;">
	
	User #{$user_id}: {$first_name} ({$email}) just logged {$login_count}th time.<br/>
	<br/>
	{if $login_count > 1}
	Login after: {$login_after}<br/>
	Last online: {$last_online}<br/>
	{/if}
	<br/>
	
	<a href="{$config.site_url}?lang={$lang}&u={$user_id}&c={$code}&h={$hash}&id=[id_hash]">Quick login</a>

</div>