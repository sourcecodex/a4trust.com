<div style="font-family: Arial;">
	User #{$user_id}: {$first_name} ({$email}) connected GWT.<br/>
	<br/>
	<a href="{$config.site_url}?lang={$lang}&u={$user_id}&c={$code}&h={$hash}&id=[id_hash]">Quick login</a><br/>
	<br/>
	GWT Sites:<br/>
	{foreach from=$sites item="site"}
		{$site->siteUrl}<br/>
	{/foreach}
</div>