<div style="background-color: #ececec; padding: 5px 25px 100px 25px;">
<center>
	<div style="text-align: center; margin: 10px 0;">
		<a href="{$config.site_url}?lang={$lang}&u={$user_id}&c={$code}&h={$hash}&id=[id_hash]" style="color: #7CB5EC; font-weight: bold; text-decoration: none;">
			<img src="cid:logo" alt="{$config.project_name}" />
		</a>
	</div>
	<table width="700" border="0" cellspacing="0" style="background-color: #fff;">
	<tr>
	<td style="text-align: left; font-family: Arial; color: #333; padding: 25px; font-size: 13px; border: 1px solid #dcdcdc; border-top: 2px solid #2985CC;">
		<b>Hi {$first_name|capitalize},</b><br/>
		<br/>
		Welcome to {$config.project_name}, we’re excited to have you joining us!<br/>
		<br/>
		To get into your account, just simply click on this link: <a href="{$config.site_url}?u={$user_id}&c={$code}&h={$hash}&id=[id_hash]" style="color: #7CB5EC; font-weight: bold; text-decoration: none;">{$config.project_name}</a><br/>
		<br/>
		To keep up to date with the latest additions to the site you can follow us on <a href="https://www.facebook.com/">Facebook</a> or <a href="https://twitter.com/">Twitter</a>.<br/>
		<br/>
		Sincerely,<br/>
		{$config.project_name} Team<br/>
	</td>
	</tr>
	</table>
</center>
</div>
<img src="{$config.site_url}mailer_image.php?id=[id_hash]" alt="" />