<div style="font-size: 16px; font-weight: bold;">{$config.project_name} account cancellation request</div><br/>

User: {$user_id} - {$email} {$first_name} {$last_name}<br/>

{if $cancel_reason}
	Cancelation reason:<br/>
	{$cancel_reason}
{else}
	No reason
{/if}