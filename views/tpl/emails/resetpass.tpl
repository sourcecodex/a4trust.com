<div style="background-color: #ececec; padding: 5px 25px 100px 25px;">
<center>
	<div style="text-align: center; margin: 10px 0;">
		<a href="{$config.site_url}?lang={$lang}&u={$user_id}&c={$code}&h={$hash}&id=[id_hash]" style="color: #7CB5EC; font-weight: bold; text-decoration: none;">
			<img src="cid:logo" alt="{$config.project_name}" />
		</a>
	</div>
	<table width="700" border="0" cellspacing="0" style="background-color: #fff;">
	<tr>
	<td style="text-align: left; font-family: Arial; color: #333; padding: 25px; font-size: 13px; border: 1px solid #dcdcdc; border-top: 2px solid #2985CC;">
		<b>Hi {$first_name|capitalize},</b><br/>
		<br/>
		A password reset has been requested for the {$config.project_name} account associated with your email address ({$email}).<br/>
		<br/>
		<a href="{$config.site_url}settings/change_password?lang={$lang}&u={$user_id}&c={$code}&h={$hash}&id=[id_hash]&reset_password=1" target="_blank"
		   style="display: block; width: 200px; height: 40px; line-height: 40px; text-align: center; background: #4D90FE; color: white; text-decoration: none; font-size: 14px; font-weight: bold; border: 1px solid grey; border-radius: 5px;">
			Create a new password
		</a>
		<br/>
		Sincerely,<br/>
		{$config.project_name} Team<br/>
	</td>
	</tr>
	</table>
</center>
</div>
<img src="{$config.site_url}mailer_image.php?id=[id_hash]" alt="" />